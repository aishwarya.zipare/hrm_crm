<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Session;
use Phpmailer;

class ProcessController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
         $this->middleware(function ($request, $next) {
            $this->role = json_decode(Auth::user()->role,true);
//            echo "<pre>";print_r(Auth::user()->dept_id);exit;
           return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getform()
    {
        $process_detail = \App\Process::select('*')->get();
        $client_detail = DB::table('users')
                              ->select(['users.*'])
                               ->leftjoin('tbl_role','tbl_role.id','=','users.role')
                               ->where('users.flag','=','0')
                                ->where('tbl_role.role_name','like','%Client%')
                               ->get();
        $qm_detail = DB::table('users')
                              ->select(['users.*'])
                               ->leftjoin('tbl_role','tbl_role.id','=','users.role')
                               ->where('users.flag','=','0')
                                ->where('tbl_role.role_name','like','%Quality Manager%')
                               ->get();
        $qa_detail = DB::table('users')
                              ->select(['users.*'])
                               ->leftjoin('tbl_role','tbl_role.id','=','users.role')
                               ->where('users.flag','=','0')
                                ->where('tbl_role.role_name','like','%Quality Auditor%')
                               ->get();
        $tl_detail = DB::table('users')
                              ->select(['users.*'])
                               ->leftjoin('tbl_role','tbl_role.id','=','users.role')
                               ->where('users.flag','=','0')
                                ->where('tbl_role.role_name','like','%Team Leader%')
                               ->get();
        return view('master_data.process_form',['process_detail'=>$process_detail,'client_detail'=>$client_detail,'qm_detail'=>$qm_detail,'qa_detail'=>$qa_detail,'tl_detail'=>$tl_detail]);
    }
    public function saveform(Request $request)
    {
        $requestData = $request->all();
        $requestData['user'] = $this->role;
         if(isset($requestData['client_id'])){
            $requestData['clients'] = implode(',', $requestData['client_id']);
        }
        
        if($requestData['process_id'] == "")
        {
//             echo "<pre>";print_r($requestData);exit;
        $process_data= \App\Process::create($requestData);
        $array=$requestData['parameter_detail'];
        foreach($array as $d){
            if (array_key_exists(2,$d)) 
        {
                $parameter_data['process_id']=$process_data->id;
                $parameter_data['parameter_name']=$d[0];
                //$parameter_data['rating']=$d[1];
                 
                $p_data= \App\Parameter::create($parameter_data);
                if (array_key_exists(2,$d)) 
                {
                $sub_parameter_data['process_id']=$process_data->id;
                $sub_parameter_data['parameter_id']=$p_data->id;
                $sub_parameter_data['subparameter_name']=$d[1];
                $sub_parameter_data['rating']=$d[2];
//              echo "<pre>";print_r($sub_parameter_data);exit;
                $s_data= \App\SubParameter::create($sub_parameter_data);
                }
        }
        else
        {
           
            
                $sub_parameter_data['process_id']=$process_data->id;
                $sub_parameter_data['parameter_id']=$p_data->id;
                $sub_parameter_data['subparameter_name']=$d[0];
                $sub_parameter_data['rating']=$d[1];
//                  echo "<pre>";print_r($sub_parameter_data);exit;
                $s_data= \App\SubParameter::create($sub_parameter_data);
        }
        }
        }
        else
        {
          //  echo "in else";
//            echo "<pre>";print_r($requestData);
            $process_data = \App\Process::select('*')->where(['id'=>$requestData['process_id']])->first();
            $process_data->update($requestData);
            $array=$requestData['parameter_detail'];
            $prev_parameter_data = \App\Parameter::select('*')->where(['process_id'=>$requestData['process_id']])->get();
            foreach($prev_parameter_data as $prev)
            {
//                  DB::table('tbl_parameters')
//        ->where('id',$prev->id)
//        ->delete();
//            }
//            $prev_subparameter_data = \App\SubParameter::select('*')->where(['process_id'=>$requestData['process_id']])->get();
//            foreach($prev_subparameter_data as $prev)
//            {
//                  DB::table('tbl_subparameter')
//        ->where('id',$prev->id)
//        ->delete();
            }
           foreach($array as $d){
              // print_r($d);exit;
                if (array_key_exists(2,$d)) 
        {
                $parameter_data['process_id']=$process_data->id;
                $parameter_data['parameter_name']=$d[0];
                //$parameter_data['rating']=$d[1];
                 
                $p_data= \App\Parameter::create($parameter_data);
                if (array_key_exists(2,$d)) 
                {
                $sub_parameter_data['process_id']=$process_data->id;
                $sub_parameter_data['parameter_id']=$p_data->id;
                $sub_parameter_data['subparameter_name']=$d[1];
                $sub_parameter_data['rating']=$d[2];
//              echo "<pre>";print_r($sub_parameter_data);exit;
                $s_data= \App\SubParameter::create($sub_parameter_data);
                }
        }
//        else if()
//        {
//           
//            
//                $sub_parameter_data['process_id']=$process_data->id;
//                $sub_parameter_data['parameter_id']=$p_data->id;
//                $sub_parameter_data['subparameter_name']=$d[0];
//                $sub_parameter_data['rating']=$d[1];
////                  echo "<pre>";print_r($sub_parameter_data);exit;
//                $s_data= \App\SubParameter::create($sub_parameter_data);
//        }
            if (array_key_exists(7,$d)) 
            {
                if (array_key_exists(8,$d)) 
                {
                    // echo "Hek 1";
                    $sub_parameter_data['process_id']=$process_data->id;
                    $sub_parameter_data['parameter_id']=$d[7];
                    $sub_parameter_data['subparameter_name']=$d[0];
                    $sub_parameter_data['rating']=$d[1];
//                    echo "<pre>";print_r($sub_parameter_data);exit;
                    if($d[8]==0 && $d[7] != 0)
                    {
                        $s_data=  \App\SubParameter::create($sub_parameter_data);
                    }
                    else if($d[8]==0 && $d[7] == 0)
                    {
                        $latest_data = \App\Parameter::select('*')->orderBy('id','desc')->first();
                        $sub_parameter_data['parameter_id']=$latest_data->id;
                         $s_data=  \App\SubParameter::create($sub_parameter_data);
                    }
                    else
                    {
                        \App\SubParameter::where(['id'=>$d[8]])->update($sub_parameter_data);
                    }
                    
                }
                else  if($d[7]!=0){
                    // echo "Hek 2";
                    $parameter_data['process_id']=$process_data->id;
                $parameter_data['parameter_name']=$d[0];
//                echo "<pre>";print_r($parameter_data);exit;
                \App\Parameter::where(['id'=>$d[7]])->update($parameter_data);
                }
                else
                {
                    //echo "Hek";
                $sub_parameter_data['process_id']=$process_data->id;
                $sub_parameter_data['parameter_id']=$p_data->id;
                $sub_parameter_data['subparameter_name']=$d[0];
                $sub_parameter_data['rating']=$d[1];
//                  echo "<pre>";print_r($sub_parameter_data);exit;
                $s_data= \App\SubParameter::create($sub_parameter_data);
                }
                
//                if($d[7]!="undefined")
//                {
//                    echo $d[7];
//                    $parameter_data['parameter_name']=$d[7];
//                    $check_parameter_data = \App\Parameter::select('*')->where(['parameter_name'=>$d[7]])->first();
//                    if(!empty($check_parameter_data))
//                    {
//                       $sub_parameter_data['parameter_id']=$check_parameter_data->id; 
//                    }
//                    else
//                    {
//                        $p_data= \App\Parameter::create($parameter_data);
//                        $sub_parameter_data['parameter_id']=$p_data->id;
//                    }
//                    $sub_parameter_data['subparameter_name']=$d[0];
//                    $sub_parameter_data['rating']=$d[1];
//                    $s_data= \App\SubParameter::create($sub_parameter_data);
//                }
                
                
            }
//             if (array_key_exists(2,$d)) 
//        {
//                $parameter_data['process_id']=$process_data->id;
//                $parameter_data['parameter_name']=$d[0];
//                //$parameter_data['rating']=$d[1];
//                 
//                $p_data= \App\Parameter::create($parameter_data);
//                if (array_key_exists(2,$d)) 
//                {
//                $sub_parameter_data['process_id']=$process_data->id;
//                $sub_parameter_data['parameter_id']=$p_data->id;
//                $sub_parameter_data['subparameter_name']=$d[1];
//                $sub_parameter_data['rating']=$d[2];
////              echo "<pre>";print_r($sub_parameter_data);exit;
//                $s_data= \App\SubParameter::create($sub_parameter_data);
//                }
//        }
            
           }
           
        
//            foreach($array as $d){
//            if (array_key_exists(1,$d)) 
//        {
//                $parameter_data['process_id']=$process_data->id;
//                $parameter_data['parameter_name']=$d[0];
//                $parameter_data['rating']=$d[1];
//                $parameter_data['parameter_id']=$d[10];
//                // echo "<pre>";print_r($parameter_data);exit;
//                $p_data = \App\Parameter::select('*')->where(['id'=>$parameter_data['parameter_id']])->first();
//               $p_data->update($parameter_data);
//               //exit;
//              
////                $sub_parameter_data['process_id']=$process_data->id;
////                $sub_parameter_data['parameter_id']=$p_data->id;
////                $sub_parameter_data['subparameter_name']=$d[2];
//                //$s_data= \App\SubParameter::create($sub_parameter_data);
//        }
//        else
//        {
////                $sub_parameter_data['process_id']=$process_data->id;
////                $sub_parameter_data['parameter_id']=$p_data->id;
////                $sub_parameter_data['subparameter_name']=$d[0];
//                //$s_data= \App\SubParameter::create($sub_parameter_data);
//        }
//        }
        }
        if($requestData["prod_flag"]==1)
        {
            $approval_data = \App\Process::where(['id'=>$process_data->id,'approval_flag'=>0])->first();
            if(!empty($approval_data))
            {
            $client_data = \App\User::select('*')->where(['id'=>$approval_data->clients])->first();
            require_once('phpmailer/PHPMailerAutoload.php');
            $flag1=false;
            $mail = new PHPMailer;
            $mail->isSMTP();   
			//$mail->isMail();
			// Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com'; //'smtp.gmail.com';             // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                     // Enable SMTP authentication
            $mail->Username = 'ha.hindsil@gmail.com';          // SMTP username
            $mail->Password = 'hind@1234'; // SMTP password
            $mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                          // TCP port to connect to            
//            $mail->Port = 465;                          // TCP port to connect to            
            $mail->From = "ha.hindsil@gmail.com";
            $mail->FromName = "MIBS Quality";
           // $mail->addAddress('aishwarya.zipare@iping.in');
            $mail->addAddress($client_data->email);
            $mail->isHTML(true); 
            $mail->Body = "Hello";
            $mail->Subject = "Process Approval";
            $mail->AltBody = "This is the plain text version of the email content";
            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
               // return redirect('error');
            } else {
           echo "Sent";
                
            }
            }
        }
    }
    public function getProcess($id)
    {
        $report='';
        $k=0;
        $i=0;
        $process_detail = \App\Process::select('*')->where('process_name','=',$id)->first();
        $parameter_detail = \App\Parameter::select('*')->where('process_id','=',$process_detail->id)->get();
        foreach($parameter_detail as $parameter)
        {
            $k++;;
            $i++;
              $report .= "<tr class='input_fields_wrap delparam_".$k."'>
                                <td style='width:0.5%;'><i class='fas fa-plus-circle add_field_button' style='color: #0c8a54;'></i><i class='fas fa-minus-circle remove_field' style='color: red;'></td>
                                <td colspan='2'><textarea name='parameter_detail[".$i."][]' id='remark' class='form-control checkblank parameter' rows='1'  aria-required='true' >".$parameter->parameter_name."</textarea><input type='hidden' name='parameter_detail[".$i."][7]' value='".$parameter->id."' class='param_id'/></td>   
                                <td style='width:5%;'></td>   
                              </tr>";
              $sub_parameter_detail = \App\SubParameter::select('*')->where('parameter_id','=',$parameter->id)->get();
              foreach($sub_parameter_detail as $sub)
              {
                  $i++;
                  $report .= "<tr class='input_fields_wrap delparam_".$k."'>
                                <td style='width:2%;'></td>
                                <td><i class='fas fa-plus-circle add_field_button1' style='color: blue;'></i><i class='fas fa-minus-circle remove_field1' style='color: red;'></i></td>
                                <td><textarea name='parameter_detail[".$i."][]' id='remark' class='form-control checkblank' rows='1'  aria-required='true'>".$sub->subparameter_name."</textarea></td>   
                                <td><input type='text' name='parameter_detail[".$i."][]' class='form-control number checkblank' value='".$sub->rating."' style='width: 50px;' /><input type='hidden' name='parameter_detail[".$i."][7]' value='".$parameter->id."' class='contact_name'/><input type='hidden' name='parameter_detail[".$i."][8]' value='".$sub->id."' class='sub_id'/></td>
                                </tr>";
              }
              
        }
        $data['result'] = $report;
        $data['index'] = $i;
        $data['process_id'] = $process_detail->id;
        $data['flag'] = $process_detail->prod_flag;
        $data['version'] = $process_detail->version;
        $data['process'] = $process_detail;
        $data['del_flag'] = $k;
        echo json_encode($data);
    }
    public function report()
    {
        $process_detail = \App\Process::select('*')->orderBy('process_name','asc')->get();
        return view('process_report',['process_detail'=>$process_detail]);
    }
    public function download()
    {
        $process_id = $_POST["process_name"];
        return view('report.download_process_report',['process_id'=>$process_id]);
    }

}
