@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')

<?php 
$dept_id = Auth::user()->dept_id;
//echo $dept_id;
    $status_arr=array();
    $status_arr=['0'=>'New','1'=>'In Progress','2'=>'Completed','3'=>'Closed'];
?>
<style>
.row [class^='col-xs-'], .row [class^='col-sm-'], .row [class^='col-md-'], .row [class^='col-lg-'] {
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 0px;
}
</style>
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="#">Department</a></li>
    <li><a href="#">Add Status</a></li>
    <!--<li><a href="#">Form Layout</a></li>-->
    <!--<li class="active">Two Column</li>-->
</ul>
<!-- END BREADCRUMB -->
<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
    <div class="row">
        <div class="col-md-12">
            <form class="form-horizontal" method="post" action="{{ url('update-status') }}">
                {{ csrf_field() }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><strong>Order Detail</strong></h3>
                    </div>
                    <div class="panel-body"> 
                        <?php if(!isset($msg_flag)) { ?>
                            <input type="hidden" name="task_id" value="{{$task_detail->task_id}}" />
                        <?php } ?>
                         <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Time Plan as on Date : </label>
                                    <div class="col-md-6">
                                       {{$order_detail->time_plan}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Fixture Number : </label>
                                    <div class="col-md-6">                                            
                                        <div class="input-group">
                                            {{$order_detail->fixture_number}}
                                        </div>                                            
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Quantity (No's) : </label>
                                    <div class="col-md-6">                                            
                                        <div class="input-group">
                                            {{$order_detail->quantity_no}}
                                        </div>                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-2 control-label" >Project Description : </label>
                                    <div class="col-md-10 col-xs-12">                                            
                                        {{$order_detail->project_desc}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Priority : </label>
                                    <div class="col-md-2">                                                                                            
                                        {{$order_detail->priority}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Packing & Dispatch :</label>
                                    <div class="col-md-6">                                            
                                        <div class="input-group">
                                            {{$order_detail->packing_dispatch}}
                                        </div>                                            
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">DAP Send To Customer :</label>
                                    <div class="col-md-6">                                            
                                        <div class="input-group">
                                            {{$order_detail->dap_send_to_customer}}
                                        </div>                                            
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Confirmation Against DAP From Customer : </label>
                                    <div class="col-md-6">                                            
                                        <div class="input-group">
                                            {{$order_detail->conf_dap}}
                                        </div>                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                            <div class="form-group">
                                    <label class="col-md-6 control-label">Design Release :</label>
                                    <div class="col-md-6">                                            
                                        <div class="input-group">
                                           {{$order_detail->design_release}}
                                        </div>                                            
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Raw Material Procurement :</label>
                                    <div class="col-md-6">                                            
                                        <div class="input-group">
                                            {{$order_detail->raw_material}}
                                        </div>                                            
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-6 control-label">Imported Boughtout Material :</label>
                                <div class="col-md-6">                                            
                                    <div class="input-group">
                                        {{$order_detail->imported_boughtout}}
                                    </div>                                            
                                </div>
                            </div>
                            </div>
                        </div>
                        <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Manufacturing :</label>
                                        <div class="col-md-6">                                            
                                            <div class="input-group">
                                                {{$order_detail->manufacturing}}
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Quality Inspection :</label>
                                        <div class="col-md-6">                                            
                                            <div class="input-group">
                                                {{$order_detail->quality_inspection}}
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Fixture Assembly :</label>
                                        <div class="col-md-6">                                            
                                            <div class="input-group">
                                                {{$order_detail->fixture_assembly}}
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Receipt Of Component : </label>
                                        <div class="col-md-6">                                            
                                            <div class="input-group">
                                                {{$order_detail->receipt_of_component}}
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Assembly CMM Inspection :</label>
                                        <div class="col-md-6">                                            
                                            <div class="input-group">
                                                {{$order_detail->assembly_cmm}}
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Machining Trial : </label>
                                        <div class="col-md-6">                                            
                                            <div class="input-group">
                                                {{$order_detail->machining_trial}}
                                            </div>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Remarks If Any :</label>
                                        <div class="col-md-10 col-xs-12">                                            
                                            {{$order_detail->remark}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <?php $role = Auth::user()->role;
                            if(!isset($msg_flag)) { 
                            if($role != 1){
                               if($dept_id == 2){
                            ?>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">DAP Send To Customer Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if($task_detail->comp_task_status == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_detail->comp_actual_date}}  
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if($task_detail->comp_task_status == 2) { 
                                                $status = $status_arr[$task_detail->comp_task_status];
                                                if($task_detail->comp_task_status == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Confirmation Against DAP From Customer Actual Date :</label>
                                    <div class="col-md-6">
                                        <?php if($task_detail->comp_task_status == 2) { ?>
                                        <div class="input-group">
                                              {{$task_detail->comp_actual_date}}  
                                        </div>
                                        <?php } else { ?>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Status :</label>
                                    <div class="col-md-6">   
                                        <?php if($task_detail->comp_task_status == 2) { 
                                            $status = $status_arr[$task_detail->comp_task_status];
                                            if($task_detail->comp_task_status == 2){
                                                $lab = "label-success";
                                            }else{
                                                $lab = "label-info";
                                            }
                                        ?>
                                        <div class="input-group">
                                            <span class="label {{$lab}}">{{$status}}</span>
                                        </div>
                                        <?php } else { ?>
                                        <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                            <option value="1">Inprogress</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <br/>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Design Release Actual Date :</label>
                                    <div class="col-md-6">
                                        <?php if($task_detail->comp_task_status == 2) { ?>
                                        <div class="input-group">
                                              {{$task_detail->comp_actual_date}}  
                                        </div>
                                        <?php } else { ?>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Status :</label>
                                    <div class="col-md-6">   
                                        <?php if($task_detail->comp_task_status == 2) { 
                                            $status = $status_arr[$task_detail->comp_task_status];
                                            if($task_detail->comp_task_status == 2){
                                                $lab = "label-success";
                                            }else{
                                                $lab = "label-info";
                                            }
                                        ?>
                                        <div class="input-group">
                                            <span class="label {{$lab}}">{{$status}}</span>
                                        </div>
                                        <?php } else { ?>
                                        <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                            <option value="1">Inprogress</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php 
                               }
                            if($dept_id == 3){
                            ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Raw Material Procurement Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if($task_detail->comp_task_status == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_detail->comp_actual_date}}  
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if($task_detail->comp_task_status == 2) { 
                                                $status = $status_arr[$task_detail->comp_task_status];
                                                if($task_detail->comp_task_status == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Imported Boughtout Material Actual Date :</label>
                                    <div class="col-md-6">
                                        <?php if($task_detail->comp_task_status == 2) { ?>
                                        <div class="input-group">
                                              {{$task_detail->comp_actual_date}}  
                                        </div>
                                        <?php } else { ?>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Status :</label>
                                    <div class="col-md-6">   
                                        <?php if($task_detail->comp_task_status == 2) { 
                                            $status = $status_arr[$task_detail->comp_task_status];
                                            if($task_detail->comp_task_status == 2){
                                                $lab = "label-success";
                                            }else{
                                                $lab = "label-info";
                                            }
                                        ?>
                                        <div class="input-group">
                                            <span class="label {{$lab}}">{{$status}}</span>
                                        </div>
                                        <?php } else { ?>
                                        <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                            <option value="1">Inprogress</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <br/>
                            <?php 
                               }
                               if($dept_id == 4){
                            ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Manufacturing Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if($task_detail->comp_task_status == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_detail->comp_actual_date}}  
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if($task_detail->comp_task_status == 2) { 
                                                $status = $status_arr[$task_detail->comp_task_status];
                                                if($task_detail->comp_task_status == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Machining Trial Actual Date :</label>
                                    <div class="col-md-6">
                                        <?php if($task_detail->comp_task_status == 2) { ?>
                                        <div class="input-group">
                                              {{$task_detail->comp_actual_date}}  
                                        </div>
                                        <?php } else { ?>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Status :</label>
                                    <div class="col-md-6">   
                                        <?php if($task_detail->comp_task_status == 2) { 
                                            $status = $status_arr[$task_detail->comp_task_status];
                                            if($task_detail->comp_task_status == 2){
                                                $lab = "label-success";
                                            }else{
                                                $lab = "label-info";
                                            }
                                        ?>
                                        <div class="input-group">
                                            <span class="label {{$lab}}">{{$status}}</span>
                                        </div>
                                        <?php } else { ?>
                                        <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                            <option value="1">Inprogress</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php 
                               }
                               if($dept_id == 5){
                            ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Quality Inspection Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if($task_detail->comp_task_status == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_detail->comp_actual_date}}  
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if($task_detail->comp_task_status == 2) { 
                                                $status = $status_arr[$task_detail->comp_task_status];
                                                if($task_detail->comp_task_status == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Assembly CMM Inspection Actual Date :</label>
                                    <div class="col-md-6">
                                        <?php if($task_detail->comp_task_status == 2) { ?>
                                        <div class="input-group">
                                              {{$task_detail->comp_actual_date}}  
                                        </div>
                                        <?php } else { ?>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Status :</label>
                                    <div class="col-md-6">   
                                        <?php if($task_detail->comp_task_status == 2) { 
                                            $status = $status_arr[$task_detail->comp_task_status];
                                            if($task_detail->comp_task_status == 2){
                                                $lab = "label-success";
                                            }else{
                                                $lab = "label-info";
                                            }
                                        ?>
                                        <div class="input-group">
                                            <span class="label {{$lab}}">{{$status}}</span>
                                        </div>
                                        <?php } else { ?>
                                        <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                            <option value="1">Inprogress</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php 
                               }
                               if($dept_id == 6){
                            ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Fixture Assembly Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if($task_detail->comp_task_status == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_detail->comp_actual_date}}  
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if($task_detail->comp_task_status == 2) { 
                                                $status = $status_arr[$task_detail->comp_task_status];
                                                if($task_detail->comp_task_status == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Receipt Of Component For Assembly Trial Actual Date :</label>
                                    <div class="col-md-6">
                                        <?php if($task_detail->comp_task_status == 2) { ?>
                                        <div class="input-group">
                                              {{$task_detail->comp_actual_date}}  
                                        </div>
                                        <?php } else { ?>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php print strftime('%Y-%m-%d'); ?>">                                            
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Status :</label>
                                    <div class="col-md-6">   
                                        <?php if($task_detail->comp_task_status == 2) { 
                                            $status = $status_arr[$task_detail->comp_task_status];
                                            if($task_detail->comp_task_status == 2){
                                                $lab = "label-success";
                                            }else{
                                                $lab = "label-info";
                                            }
                                        ?>
                                        <div class="input-group">
                                            <span class="label {{$lab}}">{{$status}}</span>
                                        </div>
                                        <?php } else { ?>
                                        <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                            <option value="1">Inprogress</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php 
                               }
                            }
                            }
                            $x = 1;
                            $arr = ['','dap_send_to_customer','raw_material','conf_dap','design_release','packing_dispatch','imported_boughtout',
          'manufacturing','quality_inspection','fixture_assembly','receipt_of_component','assembly_cmm','machining_trial'];

                            $order_status_det = DB::table('tbl_task')
                                                ->select('*')
                                                ->where(['tbl_task.order_id'=>$order_detail->order_id])
                                                ->leftjoin('tbl_department','tbl_department.dept_id','tbl_task.dept_id')
                                                ->leftjoin('tbl_order','tbl_order.order_id','tbl_task.order_id')
                                                ->get();
//                            echo "<pre>";print_r($order_status_det);exit;
                        ?>
                            <br/>
                            <?php if($role == 1) { ?>
                        <div class="row">
                            <div class="col-md-12">                                
                                <table class="table">
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Department</th>
                                        <th>Plan Date</th>
                                        <td></td>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                    <?php if(count($order_status_det) > 0){ ?>
                                    @foreach($order_status_det as $order)
                                    <?php 
                                    $com_status = explode(',', $order->comp_task_status);
                                    $com_date = explode(',', $order->comp_actual_date);
                                    $count = count($com_status);
                                    
                                    $sub_dept = \App\SubDepartment::where(['dept_id'=>$order->dept_id])->get();
//                                    echo "<pre>";print_r($sub_dept[0]->sub_dept_name);exit;
                                    $a = $arr[$order->dept_id]; ?>
                                     <tr>
                                         <td rowspan="{{$count}}">{{$x++}}</td>
                                         <td rowspan="{{$count}}">{{$order->dept_name}}</td>
                                         <td rowspan="{{$count}}">{{$order->$a}}</td>
                                         <td rowspan="{{$count}}">
                                            <table class="table" width="100%">
                                            <?php 
                                            foreach($sub_dept as $sub){
                                                echo "<tr><td>".$sub->sub_dept_name."</td></tr>";
                                            }?>
                                            </table>
                                         </td>
                                         <?php
                                         if($order->comp_task_status == "" && $order->view_task_status == ""){
                                            echo "<td>".$status_arr[$order->new_task_status]."</td>";
                                            echo "<td>".$order->new_actual_date."</td>";
                                         }
                                         else if($order->comp_task_status == ""){
                                            echo "<td>".$status_arr[$order->view_task_status]."</td>";
                                            echo "<td>".$order->view_actual_date."</td>";
                                         }else{
                                             echo "<td rowspan=".$count." ><table class='table'>";
                                             for($i=0;$i<count($com_status);$i++){
                                                echo "<tr><td>".$status_arr[$com_status[$i]]."</td></tr>";
                                             }
                                             echo "</table></td>";
                                             echo "<td rowspan=".$count." ><table class='table'>";
                                             for($i=0;$i<count($com_date);$i++){
                                                echo "<tr><td>".$com_date[$i]."</td></tr>";
                                             }
                                             echo "</table></td>";
                                         }
                                         //exit;
                                    ?>
                                     </tr>
                                    @endforeach
                                    <?php } ?>
                                </table>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="panel-footer">
                        <?php if($role == 1){ ?>
                            <a href="{{url('home')}}" class="btn btn-danger pull-right" >Back</a>
                        <?php } else { ?>
                            <a href="{{url('home')}}" class="btn btn-danger pull-right" >Back</a>
                            <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info pull-right" value="Submit" style="margin-right: 5px;"  />
                        <?php } ?>
                    </div>
                </div>
            </form>
        </div>
    </div>                    
</div>
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $("#comp_task_status").on("change",function(){
        var a = $(this).val();
//        alert(a);
        if(a == "2")
        {
            $("#btnsubmit").prop('disabled', false);
        }else{
            $("#btnsubmit").prop('disabled', true);
        }
    })
})
</script>
@endsection