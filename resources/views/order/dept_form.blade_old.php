@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')

<?php 
$dept_id1 = Auth::user()->dept_id;
$dept_id = explode(",", $dept_id1);
//echo $dept_id;
    $status_arr=array();
    $status_arr=['0'=>'New','1'=>'In Progress','2'=>'Completed','3'=>'Closed'];
//    echo "<pre>";print_r($task_detail);exit;
?>
<style>
.row [class^='col-xs-'], .row [class^='col-sm-'], .row [class^='col-md-'], .row [class^='col-lg-'] {
    min-height: 1px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 0px;
}
label {
    font-weight: bold;
}
</style>
<div class="page-wrapper">
    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Order Details</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Department</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Status</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
    
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                {{ csrf_field() }}
                <div class="card-body"> 
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Time Plan as on Date : </label>
                        <div class="input-group col-sm-2">
                            {{$order_detail->time_plan}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Fixture Number :</label>
                        <div class="col-sm-2">
                            {{$order_detail->fixture_number}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Quantity (No's) :</label>
                        <div class="col-sm-2">
                            {{$order_detail->quantity_no}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Project Description : </label>
                        <div class="input-group col-sm-10">
                            {{$order_detail->project_desc}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Priority : </label>
                        <div class="input-group col-sm-10">
                            {{$order_detail->priority}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Packing & Dispatch :</label>
                        <div class="input-group col-sm-2">
                            {{$order_detail->packing_dispatch}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label" style="padding-right: 3px;">DAP Send To Customer :</label>
                        <div class="col-sm-2">
                            {{$order_detail->dap_send_to_customer}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Confirmation Against DAP From Customer : </label>
                        <div class="col-sm-2">
                            {{$order_detail->conf_dap}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Design Release :</label>
                        <div class="input-group col-sm-2">
                            {{$order_detail->design_release}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Raw Material Procurement :</label>
                        <div class="col-sm-2">
                            {{$order_detail->raw_material}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Imported Boughtout Material : </label>
                        <div class="col-sm-2">
                            {{$order_detail->imported_boughtout}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Manufacturing :</label>
                        <div class="input-group col-sm-2">
                            {{$order_detail->manufacturing}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Quality Inspection :</label>
                        <div class="col-sm-2">
                            {{$order_detail->quality_inspection}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Fixture Assembly : </label>
                        <div class="col-sm-2">
                            {{$order_detail->fixture_assembly}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label" style="padding-right: 5px;">Receipt Of Component :</label>
                        <div class="input-group col-sm-2">
                            {{$order_detail->receipt_of_component}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Assembly CMM Inspection :</label>
                        <div class="col-sm-2">
                            {{$order_detail->assembly_cmm}}
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Machining Trial : </label>
                        <div class="col-sm-2">
                            {{$order_detail->machining_trial}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Remarks If Any : </label>
                        <div class="input-group col-sm-10">
                            {{$order_detail->remark}}
                        </div>
                    </div>
                            
                    <?php 
                    $role = Auth::user()->role;
                    if(!isset($msg_flag)) { 
                    if($role != 1){
                       if(in_array(1,$dept_id)){
                           $check = \App\Task::where(['order_id'=>$task_detail->order_id])
                                        ->where(['dept_id'=>1])
                                        ->first();
                           if(count($check)>0){
                            $task_st = explode(",", $check->comp_task_status);
                            $task_dt = explode(",", $check->comp_actual_date);
                    ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Packing & Dispatch :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[0]}}  
                                                  <?php if($task_detail->dept_id == 1){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[0]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[0])) echo $task_dt[0]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { 
                                                $status = $status_arr[$task_st[0]];
                                                if($task_st[0] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 1){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[0]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                               }}
//                               if($dept_id == 2){
                               if(in_array(2,$dept_id)){
                                   $check = \App\Task::where(['order_id'=>$task_detail->order_id])
                                                ->where(['dept_id'=>2])
                                                ->first();
                                   if(count($check)>0){
                                       $task_st = explode(",", $check->comp_task_status);
                                       $task_dt = explode(",", $check->comp_actual_date);
                            ?>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">DAP Send To Customer Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[0]}}  
                                                  <?php if($task_detail->dept_id == 2){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[0]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[0])) echo $task_dt[0]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { 
                                                $status = $status_arr[$task_st[0]];
                                                if($task_st[0] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 2){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[0]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Confirmation Against DAP From Customer Actual Date :</label>
                                    <div class="col-md-6">
                                        <?php if(isset($task_st[1]) && $task_st[1] == 2) { ?>
                                        <div class="input-group">
                                              {{$task_dt[1]}}  
                                              <?php if($task_detail->dept_id == 2){ ?>
                                              <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[1]}}">
                                              <?php } ?>
                                        </div>
                                        <?php } else { ?>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[1])) echo $task_dt[1]; else print strftime('%Y-%m-%d'); ?>">                                            
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Status :</label>
                                    <div class="col-md-6">   
                                        <?php if(isset($task_st[1]) && $task_st[1] == 2) { 
                                            $status = $status_arr[$task_st[1]];
                                            if($task_st[1] == 2){
                                                $lab = "label-success";
                                            }else{
                                                $lab = "label-info";
                                            }
                                        ?>
                                        <div class="input-group">
                                            <span class="label {{$lab}}">{{$status}}</span>
                                            <?php if($task_detail->dept_id == 2){ ?>
                                            <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[1]}}">
                                            <?php } ?>
                                        </div>
                                        <?php } else { ?>
                                        <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                            <option value="1">Inprogress</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <br/>
                            <div class="row">
                            <div class="col-md-5">
                                <div class="form-group">                                        
                                    <label class="col-md-6 control-label">Design Release Actual Date :</label>
                                    <div class="col-md-6">
                                        <?php if(isset($task_st[2]) && $task_st[2] == 2) { ?>
                                        <div class="input-group">
                                              {{$task_dt[2]}}  
                                              <?php if($task_detail->dept_id == 2){ ?>
                                              <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[2]}}">
                                              <?php } ?>
                                        </div>
                                        <?php } else { ?>
                                        <div class="input-group">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[2])) echo $task_dt[2]; else print strftime('%Y-%m-%d'); ?>">                                            
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-md-6 control-label">Status :</label>
                                    <div class="col-md-6">   
                                        <?php if(isset($task_st[2]) && $task_st[2] == 2) { 
                                            $status = $status_arr[$task_st[2]];
                                            if($task_st[2] == 2){
                                                $lab = "label-success";
                                            }else{
                                                $lab = "label-info";
                                            }
                                        ?>
                                        <div class="input-group">
                                            <span class="label {{$lab}}">{{$status}}</span>
                                            <?php if($task_detail->dept_id == 2){ ?>
                                            <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[2]}}">
                                            <?php } ?>
                                        </div>
                                        <?php } else { ?>
                                        <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                            <option value="1">Inprogress</option>
                                            <option value="2">Completed</option>
                                        </select>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php 
                               }}
//                            if($dept_id == 3){
                               if(in_array(3,$dept_id)){
                                   $check = \App\Task::where(['order_id'=>$task_detail->order_id])
                                                ->where(['dept_id'=>3])
                                                ->first();
                                   if(count($check)>0){
                                       $task_st = explode(",", $check->comp_task_status);
                                       $task_dt = explode(",", $check->comp_actual_date);
                            ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Raw Material Procurement Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[0]}} 
                                                  <?php if($task_detail->dept_id == 3){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[0]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[0])) echo $task_dt[0]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { 
                                                $status = $status_arr[$task_st[0]];
                                                if($task_st[0] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 3){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[0]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Imported Boughtout Material Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php 
                                            if(isset($task_st[1]) && $task_st[1] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[1]}}  
                                                  <?php if($task_detail->dept_id == 3){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[1]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php }   else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[1])) echo $task_dt[1]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php 
                                            if(isset($task_st[1]) && $task_st[1] == 2) { 
                                                $status = $status_arr[$task_st[1]];
                                                if($task_st[1] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 3){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[1]}}">
                                                <?php } ?>
                                            </div>
                                            <?php }  else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <?php 
                               } }
//                               if($dept_id == 4){
                               if(in_array(4,$dept_id)){
                                   $check = \App\Task::where(['order_id'=>$task_detail->order_id])
                                                ->where(['dept_id'=>4])
                                                ->first();
                                   if(count($check)>0){
                                       $task_st = explode(",", $check->comp_task_status);
                                       $task_dt = explode(",", $check->comp_actual_date);
                            ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Manufacturing Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[0]}}  
                                                  <?php if($task_detail->dept_id == 4){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[0]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[0])) echo $task_dt[0]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { 
                                                $status = $status_arr[$task_st[0]];
                                                if($task_st[0] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 4){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[0]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Machining Trial Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[1]) && $task_st[1] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[1]}}  
                                                  <?php if($task_detail->dept_id == 4){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[1]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[1])) echo $task_dt[1]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[1]) && $task_st[1] == 2) { 
                                                $status = $status_arr[$task_st[1]];
                                                if($task_st[1] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 4){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[1]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                               } }
//                               if($dept_id == 5){
                               if(in_array(5,$dept_id)){
                                   $check = \App\Task::where(['order_id'=>$task_detail->order_id])
                                                ->where(['dept_id'=>5])
                                                ->first();
                                   if(count($check)>0){
                                       $task_st = explode(",", $check->comp_task_status);
                                       $task_dt = explode(",", $check->comp_actual_date);
                            ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Quality Inspection Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[0]}}  
                                                  <?php if($task_detail->dept_id == 5){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[0]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[0])) echo $task_dt[0]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { 
                                                $status = $status_arr[$task_st[0]];
                                                if($task_st[0] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 5){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[0]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Assembly CMM Inspection Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[1]) && $task_st[1] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[1]}}  
                                                  <?php if($task_detail->dept_id == 5){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[1]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[1])) echo $task_dt[1]; else if(isset($task_dt[1])) echo $task_dt[1]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[1]) && $task_st[1] == 2) { 
                                                $status = $status_arr[$task_st[1]];
                                                if($task_st[1] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 5){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[1]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                               } }
//                               if($dept_id == 6){
                               if(in_array(6,$dept_id)){
                                   $check = \App\Task::where(['order_id'=>$task_detail->order_id])
                                                ->where(['dept_id'=>6])
                                                ->first();
                                   if(count($check)>0){
                                       $task_st = explode(",", $check->comp_task_status);
                                       $task_dt = explode(",", $check->comp_actual_date);
                            ?>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Fixture Assembly Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[0]}}  
                                                  <?php if($task_detail->dept_id == 6){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[0]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[0])) echo $task_dt[0]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[0]) && $task_st[0] == 2) { 
                                                $status = $status_arr[$task_st[0]];
                                                if($task_st[0] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 6){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[0]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                            </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">                                        
                                        <label class="col-md-6 control-label">Receipt Of Component Actual Date :</label>
                                        <div class="col-md-6">
                                            <?php if(isset($task_st[1]) && $task_st[1] == 2) { ?>
                                            <div class="input-group">
                                                  {{$task_dt[1]}}  
                                                  <?php if($task_detail->dept_id == 6){ ?>
                                                  <input type="hidden" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="{{$task_dt[1]}}">
                                                  <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                <input type="text" name="comp_actual_date[]" id="comp_actual_date[]" class="form-control datepicker" value="<?php if(isset($task_dt[1])) echo $task_dt[1]; else print strftime('%Y-%m-%d'); ?>">                                            
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-md-6 control-label">Status :</label>
                                        <div class="col-md-6">   
                                            <?php if(isset($task_st[1]) && $task_st[1] == 2) { 
                                                $status = $status_arr[$task_st[1]];
                                                if($task_st[1] == 2){
                                                    $lab = "label-success";
                                                }else{
                                                    $lab = "label-info";
                                                }
                                            ?>
                                            <div class="input-group">
                                                <span class="label {{$lab}}">{{$status}}</span>
                                                <?php if($task_detail->dept_id == 6){ ?>
                                                <input type="hidden" name="comp_task_status[]" id="comp_task_status[]" class="form-control datepicker" value="{{$task_st[1]}}">
                                                <?php } ?>
                                            </div>
                                            <?php } else { ?>
                                            <select class="form-control select" name="comp_task_status[]" id="comp_task_status[]">
                                                <option value="1">Inprogress</option>
                                                <option value="2">Completed</option>
                                                </select>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                               } }
                            }
                            }
                            $x = 1;
                            $arr = ['','dap_send_to_customer','raw_material','conf_dap','design_release','packing_dispatch','imported_boughtout',
          'manufacturing','quality_inspection','fixture_assembly','receipt_of_component','assembly_cmm','machining_trial'];

                            $order_status_det = DB::table('tbl_task')
                                                ->select('*')
                                                ->where(['tbl_task.order_id'=>$order_detail->order_id])
                                                ->leftjoin('tbl_department','tbl_department.dept_id','tbl_task.dept_id')
                                                ->leftjoin('tbl_order','tbl_order.order_id','tbl_task.order_id')
                                                ->get();
//                            echo "<pre>";print_r($order_status_det);exit;
                        ?>
                            <br/>
                            <?php if($role == 1) { ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">  
                                    <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Department</th>
                                        <th>Plan Date</th>
                                        <td></td>
                                        <th>Status</th>
                                        <th>Date</th>
                                    </tr>
                                    <?php if(count($order_status_det) > 0){ ?>
                                    @foreach($order_status_det as $order)
                                    <?php 
                                    $com_status = explode(',', $order->comp_task_status);
                                    $com_date = explode(',', $order->comp_actual_date);
                                    $count = count($com_status) + 1;
                                    
                                    $sub_dept = \App\SubDepartment::where(['dept_id'=>$order->dept_id])->get();
//                                    echo "<pre>";print_r($sub_dept[0]->sub_dept_name);exit;
                                    $a = $arr[$order->dept_id]; ?>
                                     <tr>
                                         <td rowspan="{{$count}}">{{$x++}}</td>
                                         <td rowspan="{{$count}}">{{$order->dept_name}}</td>
                                         <td rowspan="{{$count}}">{{$order->$a}}</td>
                                         <?php if($order->comp_task_status == "" && $order->view_task_status == ""){
                                            echo "<td></td>";
                                            echo "<td>".$status_arr[$order->new_task_status]."</td>";
                                            echo "<td>".$order->new_actual_date."</td>";
                                         }
                                         else if($order->comp_task_status == ""){
                                            echo "<td></td>";
                                            echo "<td>".$status_arr[$order->view_task_status]."</td>";
                                            echo "<td>".$order->view_actual_date."</td>";
                                         }else{
                                            for($i=0;$i<count($com_status);$i++){
                                                echo "<tr><td>".$sub_dept[$i]['sub_dept_name']."</td>";
                                                echo "<td>".$status_arr[$com_status[$i]]."</td>";
                                                echo "<td>".$com_date[$i]."</td></tr>";
                                             }
                                         }
                                    ?>
                                     </tr>
                                    @endforeach
                                    <?php } ?>
                                </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                        <?php if($role == 1){ ?>
                            <a href="{{url('list-order')}}" class="btn btn-danger pull-right" >Back</a>
                        <?php } else { ?>
                            <a href="{{url('list-order')}}" class="btn btn-danger pull-right" >Back</a>
                            <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info pull-right" value="Submit" style="margin-right: 5px;" disabled />
                        <?php } ?>
                    </div>
                    </div>
                </div>
            </form>
        </div>
    </div>                    
</div>
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    $(".select").on("change",function(){
        var a = $(this).val();
//        alert(a);
        if(a == "2")
        {
            $("#btnsubmit").prop('disabled', false);
        }else{
            $("#btnsubmit").prop('disabled', true);
        }
    })
})
</script>
@endsection