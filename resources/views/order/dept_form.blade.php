@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')

<?php
$dept_id1 = Auth::user()->dept_id;
$dept_id = explode(",", $dept_id1);
//echo $dept_id;
$status_arr = $dept_arr = $dept_table_arr = array();
$status_arr = ['0' => 'design_upload_status','1' => 'ppc_status', '2' => 'material_finish_status', '3' => 'operator_finish_status', '4' => 'quality_finish_status','5' => 'assembly_finish_status'];
$dept_arr = ['0' => 'Design Dept', '1' => 'PPC Dept', '2' => 'Material Dept', '3' => 'Operator Dept', '4' => 'Quality Dept', '5' => 'Assembly Dept'];
$dept_table_arr = ['0' => 'tbl_design', '1' => 'tbl_production_planning', '2' => 'tbl_material', '3' => 'tbl_operator', '4' => 'tbl_quality_inspection', '5' => 'tbl_assembly_finish'];
$dept_table_pdate = ['0' => 'design_release', '1' => '', '2' => 'raw_material', '3' => 'manufacturing', '4' => 'quality_inspection', '5' => 'assembly_cmm'];
$dept_table_adate = ['0' => 'design_date', '1' => 'ppc_date', '2' => 'material_date', '3' => 'operator_date', '4' => 'quality_inspection_date', '5' => 'assembly_receive_date'];
//    echo "<pre>";print_r($task_detail);exit;
?>
<style>
    .row [class^='col-xs-'], .row [class^='col-sm-'], .row [class^='col-md-'], .row [class^='col-lg-'] {
        min-height: 1px;
        padding-left: 10px;
        padding-right: 10px;
        padding-top: 0px;
    }
    label {
        font-weight: bold;
    }
</style>
<div class="page-wrapper">

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add Status</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Department</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Add Status</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    {{ csrf_field() }}
                    <div class="card-body"> 
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Time Plan as on Date : </label>
                            <div class="input-group col-sm-2">
                                {{$order_detail->time_plan}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Fixture Number :</label>
                            <div class="col-sm-2">
                                {{$order_detail->fixture_number}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Quantity (No's) :</label>
                            <div class="col-sm-2">
                                {{$order_detail->quantity_no}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Project Description : </label>
                            <div class="input-group col-sm-10">
                                {{$order_detail->project_desc}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Priority : </label>
                            <div class="input-group col-sm-10">
                                {{$order_detail->priority}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Packing & Dispatch :</label>
                            <div class="input-group col-sm-2">
                                {{$order_detail->packing_dispatch}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label" style="padding-right: 3px;">DAP Send To Customer :</label>
                            <div class="col-sm-2">
                                {{$order_detail->dap_send_to_customer}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Confirmation Against DAP From Customer : </label>
                            <div class="col-sm-2">
                                {{$order_detail->conf_dap}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Design Release :</label>
                            <div class="input-group col-sm-2">
                                {{$order_detail->design_release}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Raw Material Procurement :</label>
                            <div class="col-sm-2">
                                {{$order_detail->raw_material}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Imported Boughtout Material : </label>
                            <div class="col-sm-2">
                                {{$order_detail->imported_boughtout}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Manufacturing :</label>
                            <div class="input-group col-sm-2">
                                {{$order_detail->manufacturing}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Quality Inspection :</label>
                            <div class="col-sm-2">
                                {{$order_detail->quality_inspection}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Fixture Assembly : </label>
                            <div class="col-sm-2">
                                {{$order_detail->fixture_assembly}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label" style="padding-right: 5px;">Receipt Of Component :</label>
                            <div class="input-group col-sm-2">
                                {{$order_detail->receipt_of_component}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Assembly CMM Inspection :</label>
                            <div class="col-sm-2">
                                {{$order_detail->assembly_cmm}}
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Machining Trial : </label>
                            <div class="col-sm-2">
                                {{$order_detail->machining_trial}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Remarks If Any : </label>
                            <div class="input-group col-sm-10">
                                {{$order_detail->remark}}
                            </div>
                        </div>

                        <?php
                        $role = Auth::user()->role;

                        $i = 1;
                        $design_detail = \App\Design::select('*')->where('order_id', '=', $order_detail->order_id)->first();
                        $ppc_detail = \App\ProductionPlanning::select('*')->where('order_id', '=', $order_detail->order_id)->first();
                        $raw_detail = \App\Material::select('*')->where('order_id', '=', $order_detail->order_id)->first();
                        $operator_detail = \App\Operator::select('*')->where('order_id', '=', $order_detail->order_id)->first();
                        $quality_detail = \App\QualityInspection::select('*')->where('order_id', '=', $order_detail->order_id)->first();
                        $assembly_detail = \App\AssemblyFinish::select('*')->where('order_id', '=', $order_detail->order_id)->first();
                        ?>
                        <?php // if($role == 1) { ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="card">  
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Department</th>
                                                <th>Plan Date</th>
                                                <th>Status</th>
                                                <th>Date</th>
                                            </tr>
                                            <?php
                                            $date=date('Y-m-d');
                                            $j=1;
                                            for ($i = 0; $i < count($dept_arr); $i++) {
                                                $last = DB::table($dept_table_arr[$i])->select('*')->where('order_id', '=', $order_detail->order_id)->first();
                                                if($i==0)
                                                $ppc_status = \App\Design::where(['order_id' => $order_detail->order_id, $status_arr[$i] => 0])->count();
                                                else
                                                $ppc_status = \App\DesignDetail::where(['order_id' => $order_detail->order_id, $status_arr[$i] => 0])->count();    
                                                 $plan_date = $dept_table_pdate[$i];
                                                $actual_date = $dept_table_adate[$i];
                                                if ($ppc_status > 0) {
                                                    $label = "label-primary";
                                                    $status = "In Progress";
                                                    if($order_detail->$plan_date<=$date)
                                                    {
                                                         $label = "label-danger";
                                                        $status = "Pending";
                                                    }
                                                } else {
                                                    $label = "label-success";
                                                    $status = "Completed";
                                                }
                                               
                                                if ($last != "") {
                                                    ?>
                                                    <tr>
                                                        <td>{{$j}}</td>
                                                        <td>{{$dept_arr[$i]}}</td>
                                                        <td>{{$order_detail->$plan_date}}</td>
                                                        <td><span class="label {{$label}}">{{$status}}</span></td>
                                                        <td>{{$last->$actual_date}}</td>
                                                    </tr>            
                                                <?php }
                                           $j++;}
                                            ?>
                                           
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
<?php //}   ?>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <?php if ($role == 1) { ?>
                                <a href="{{url('home')}}" class="btn btn-danger pull-right" >Back</a>
<?php } else { ?>
                                <a href="{{url('home')}}" class="btn btn-danger pull-right" >Back</a>
                                <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info pull-right" value="Submit" style="margin-right: 5px;" disabled />
<?php } ?>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>                    
    </div>
    <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
    <script type="text/javascript">
$(document).ready(function () {
    $(".select").on("change", function () {
        var a = $(this).val();
        //        alert(a);
        if (a == "2")
        {
            $("#btnsubmit").prop('disabled', false);
        } else {
            $("#btnsubmit").prop('disabled', true);
        }
    })
})
    </script>
    @endsection