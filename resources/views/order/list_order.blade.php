@extends('layouts.app')
@section('title', 'Order List')
@section('content')
<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Order List</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('home')}}">Order List</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>  
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    <div class="container-fluid">
<!--    <div class="col-md-12">
         START DEFAULT DATATABLE 
        <div class="panel panel-default">
            <div class="panel-heading">                                
                <h3 class="panel-title">Order List</h3>
               <a href="{{url('add-order')}}" class="panel-title" style="margin-left: 70%"><span class="fa fa-plus-square"></span>Add New Order</a>
                <ul class="panel-controls">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                    <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                </ul>                                
            </div>
            <div class="panel-body">
                <table class="table datatable">-->
<div class="row">
        <div class="col-12">
            <!-- START PROJECTS BLOCK -->
            <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">ORDER LIST <a href="{{url('add-order')}}" class="panel-title" style="margin-left: 70%;color: #dc3d59;"><span class="fa fa-plus-square"></span>Add New Order</a></h5>
                        <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>Time Plan as on Date</th>
                            <th>Fixture Number</th>
                            <th>Quantity (No's)</th>
                            <th>Priority</th>
                            <th>Action</th>
                            <!--<th>Salary</th>-->
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orderData as $order)
                        <tr>
                            <td>{{$order->time_plan}}</td>
                            <td>{{$order->fixture_number}}</td>
                            <td>{{$order->quantity_no}}</td>
                            <td>{{$order->priority}}</td>
                            <td>
                                <a href="{{url('dept-view?id='.$order->order_id)}}" style="color: #dc3d59;"><i class="fas fas fa-eye"></i></a>&nbsp;&nbsp; 
                            <?php if($order->flag == 0) { ?>
                                <a href="javascript::vaid(0)" ><i class="fas fa-file"></i></a>&nbsp;&nbsp; 
                            <?php } ?>
                                <a href="{{url('edit-order?id='.$order->order_id)}}"><i class="fa fa-edit"></i></a>
                                <a href="{{url('delete-order/'.$order->order_id)}}" style="color: red;"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END DEFAULT DATATABLE -->
    </div>
</div>                              
</div>
    </div>
</div>
@endsection

