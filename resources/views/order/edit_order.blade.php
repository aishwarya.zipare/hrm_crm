@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<style>
  .input-group-text{
      color: #8f9095;
      /*background-color: #dc3d59;*/
  }
  label {
    font-weight: bold;
}
.error{
    color:red;
}
 .input-group{ height: 35px; }
</style>
<div class="page-wrapper">
    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Edit Order</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('list-order')}}">Order</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Order</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
     <div class="row">
        <div class="col-md-12">
            <div class="card">
            {!! Form::model($orderData, [
                            'method' => 'PUT',
                            'url' => ['update-order', $orderData->order_id],
                            'class' => 'form-wizard',
                            'id' => 'orderForm',
                        ]) !!}
                        {{ csrf_field() }}
                <div class="card-body">   
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Time Plan as on Date</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="time_plan" class="form-control datepicker-autoclose" value="{{$orderData->time_plan}}" placeholder="YY-MM-DD" required>
                            </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Fixture Number</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="fixture_number" id="fixture_number" value="{{$orderData->fixture_number}}" placeholder="Fixture Number" required <?php if($orderData->flag == 1){ ?>readonly <?php } ?> />
                            <code id="fixture_validate"></code>
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Quantity (No's)</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control abc" name="quantity_no" id="quantity_no" value="{{$orderData->quantity_no}}" placeholder="Quantity (No's)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cono1" class="col-sm-2 text-right control-label col-form-label">Project Description</label>
                        <div class="col-sm-10">
                            <textarea name="project_desc" id="project_desc" class="form-control abc" rows="5">{{$orderData->project_desc}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Priority</label>
                        <div class="col-sm-2">                                                                                            
                            <select class="select2 form-control custom-select abc" name="priority" id="priority" >
                                <option value="Low" <?php if($orderData->priority == "Low") echo "selected"; ?>>Low</option>
                                <option value="Medium" <?php if($orderData->priority == "Medium") echo "selected"; ?>>Medium</option>
                                <option value="High" <?php if($orderData->priority == "High") echo "selected"; ?>>High</option>
                                <option value="Urgent" <?php if($orderData->priority == "Urgent") echo "selected"; ?>>Urgent</option>
                                <option value="Hold" <?php if($orderData->priority == "Hold") echo "selected"; ?>>Hold</option>
                            </select>
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Order Type</label>
                        <div class="col-sm-2">                                                                                            
                            <select class="select2 form-control custom-select abc" name="order_type" id="order_type" required>
                                <option value="New fixture" <?php if($orderData->order_type == "New fixture") echo "selected"; ?>>New fixture</option>
                                <option value="Repeat order" <?php if($orderData->order_type == "Repeat order") echo "selected"; ?>>Repeat order</option>
                                <option value="Modification fixture" <?php if($orderData->order_type == "Modification fixture") echo "selected"; ?>>Modification fixture</option>
                                <option value="Spare order" <?php if($orderData->order_type == "Spare order") echo "selected"; ?>>Spare order</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Packing & Dispatch</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="packing_dispatch" id="packing_dispatch" class="form-control datepicker-autoclose abc" value="{{$orderData->packing_dispatch}}" placeholder="YY-MM-DD" required />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">DAP Send To Customer</label>
                        <div class="input-group col-sm-2">
                              <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="dap_send_to_customer" id="dap_send_to_customer" class="form-control datepicker-autoclose abc" value="{{$orderData->dap_send_to_customer}}" placeholder="YY-MM-DD" required />
                          
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Confirmation Against DAP From Customer</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="conf_dap" id="conf_dap" class="form-control datepicker-autoclose abc" value="{{$orderData->conf_dap}}" placeholder="YY-MM-DD" required />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Design Release</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="design_release" id="design_release" value="{{$orderData->design_release}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Raw Material Procurement</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="raw_material" id="raw_material" value="{{$orderData->raw_material}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Imported Boughtout Material</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="imported_boughtout" id="imported_boughtout" value="{{$orderData->imported_boughtout}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Manufacturing</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="manufacturing" id="manufacturing" value="{{$orderData->manufacturing}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Quality Inspection</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="quality_inspection" id="quality_inspection" value="{{$orderData->quality_inspection}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Fixture Assembly</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="fixture_assembly" id="fixture_assembly" value="{{$orderData->fixture_assembly}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Receipt Of Component</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="receipt_of_component" id="receipt_of_component" value="{{$orderData->receipt_of_component}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Assembly CMM Inspection</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="assembly_cmm" id="assembly_cmm" value="{{$orderData->assembly_cmm}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Machining Trial</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text "><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="machining_trial" id="machining_trial" value="{{$orderData->machining_trial}}" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" required />
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label for="cono1" class="col-sm-2 text-right control-label col-form-label">Remarks If Any</label>
                        <div class="col-sm-10">
                            <textarea name="remark" id="remark" class="form-control abc" rows="5">{{$orderData->remark}}</textarea>
                        </div>
                    </div>
                </div>
                    <div class="border-top">
                        <div class="card-body">
                        <?php if($orderData->flag == 0){ ?>
                        <input type="submit" name="btnsave"  id="btnsave" class="btn btn-success" value="Save">
                        <?php } ?>
                        <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info" value="Submit">
                        <a href="{{url('list-order')}}" class="btn btn-danger" >Cancel</a>  
                        </div>
                        <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                    </div>
                </div>
            </form>
        </div>
    </div>                    
</div>
 <script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type='text/javascript' src='js/plugins/jquery-validation/jquery.validate.js'></script>
<script type="text/javascript">
//            $("#btnsubmit").on("click",function(){
            var jvalidate = $("#orderForm").validate({
                rules: {                                            
                        fixture_number: {required: true},
                        quantity_no: {required: true},
                        packing_dispatch: {required: true},
                        dap_send_to_customer: {required: true},
                        conf_dap: {required: true},
                        design_release: {required: true},
                        raw_material: {required: true},
                        imported_boughtout: {required: true},
                        manufacturing: {required: true},
                        quality_inspection: {required: true},
                        fixture_assembly: {required: true},
                        receipt_of_component: {required: true},
                        assembly_cmm : {required: true},
                        machining_trial: {required:true}
                    }                                        
                });
                
                $('#btnsubmit').on('click', function() {
                    $("#orderForm").valid();
                });
                
                $("#btnsave").click(function(){
                    $(".abc").removeAttr('required');
                    $("#orderForm").validate({
                        rules: {  
                                time_plan : {required: true},
                                fixture_number: {required: true},
                            }                                        
                        });
                    $("#orderForm").submit();
                })
//            })
                 $("#fixture_number").focusout(function(){
                     var fixture_no = $(this).val();
                     $.ajax({
                            url: 'fixture-validate/' + fixture_no,
                            type: "GET",
                            success: function(data) {
                            console.log(data);
                            $("#fixture_validate").html(data);
                            if (data!= ""){
                                    $("#fixture_number").val("");
                            }
                            }
                    });
                    });
                

        </script>
        
@endsection