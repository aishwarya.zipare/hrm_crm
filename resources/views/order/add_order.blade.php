@extends('layouts.app')
@section('title', 'Add New Order')
@section('content')
<style>
  .input-group-text{
      color: #8f9095;
      /*background-color: #dc3d59;*/
  }
  label {
    font-weight: bold;
}
.error{
    color:red;
}
 .input-group{ height: 35px; }
</style>
<div class="page-wrapper">
    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Add New Order</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('list-order')}}">Order</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add New Order</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- PAGE CONTENT WRAPPER -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <form class="form-horizontal" id="orderForm" method="post" action="{{ url('add-order') }}">
                {{ csrf_field() }}
                <div class="card-body">                                                                       
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Time Plan as on Date</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="time_plan" class="form-control datepicker-autoclose" value="<?php echo date('Y/m/d'); ?>" placeholder="YY-MM-DD" required>
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Fixture Number</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="fixture_number" id="fixture_number" placeholder="Fixture Number" required>
                            <code id="fixture_validate"></code>
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Fixture Name</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control" name="fixture_name" id="fixture_name" placeholder="Fixture Name" required>
                            <code id="fixture_validate"></code>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label ">Customer Name/Code</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control abc" name="customer_name" id="customer_name"  placeholder="Customer Name" required>
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label ">Part No. & Name</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control abc" name="part_no_name" id="part_no_name"  placeholder="Part No. & Name" required>
                        </div>
                        <label for="fname" class="col-sm-2 text-right control-label col-form-label ">Quantity (No's)</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control abc" name="quantity_no" id="quantity_no"  placeholder="Quantity (No's)" required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cono1" class="col-sm-2 text-right control-label col-form-label">Project Description</label>
                        <div class="col-sm-10">
                            <textarea name="project_desc" id="project_desc" class="form-control abc" rows="5" required></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Priority</label>
                        <div class="col-sm-2">                                                                                            
                            <select class="select2 form-control custom-select abc" name="priority" id="priority" required>
                                <option value="Low">Low</option>
                                <option value="Medium">Medium</option>
                                <option value="High">High</option>
                                <option value="Urgent">Urgent</option>
                                <option value="Hold">Hold</option>
                            </select>
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Order Type</label>
                        <div class="col-sm-2">                                                                                            
                            <select class="select2 form-control custom-select abc" name="order_type" id="order_type" required>
                                <option value="New fixture">New fixture</option>
                                <option value="Repeat order">Repeat order</option>
                                <option value="Modification fixture">Modification fixture</option>
                                <option value="Spare order">Spare order</option>
                                <option value="Hold">Hold</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Packing & Dispatch</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="packing_dispatch" id="packing_dispatch" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD"  />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">DAP Send To Customer</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="dap_send_to_customer" id="dap_send_to_customer" class="form-control abc datepicker-autoclose" placeholder="YY-MM-DD"  />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Confirmation Against DAP From Customer</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="conf_dap" id="conf_dap" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD"  />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Design Release</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="design_release" id="design_release" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD"  />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Raw Material Procurement</label>
                        <div class="input-group col-sm-2">
                            <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="raw_material" id="raw_material" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD"  />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Imported Boughtout Material</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="imported_boughtout" id="imported_boughtout" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD"  />
                        </div>
                    </div>    
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Manufacturing</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="manufacturing" id="manufacturing" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD"  />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Quality Inspection</label>
                        <div class="input-group col-sm-2">
                              <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="quality_inspection" id="quality_inspection" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Fixture Assembly</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="fixture_assembly" id="fixture_assembly" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" />
                        </div>
                    </div>   
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Receipt Of Component</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="receipt_of_component" id="receipt_of_component" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD"  />
                            
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Assembly CMM Inspection</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="assembly_cmm" id="assembly_cmm" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD"  />
                        </div>
                        <label class="col-sm-2 text-right control-label col-form-label">Machining Trial</label>
                        <div class="input-group col-sm-2">
                             <div class="input-group-append">
                                <span class="input-group-text text-pink"><i class="fas fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="machining_trial" id="machining_trial" class="form-control datepicker-autoclose abc" placeholder="YY-MM-DD" />
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label for="cono1" class="col-sm-2 text-right control-label col-form-label">Remarks If Any</label>
                        <div class="col-sm-10">
                            <textarea name="remark" id="remark" class="form-control abc" rows="5" ></textarea>
                        </div>
                    </div>
                        </div>
                    <div class="border-top">
                        <div class="card-body">
                        <input type="button" name="btnsave"  id="btnsave" class="btn btn-success" value="Save">
                        <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info" value="Submit">
                        <a href="{{url('list-order')}}" class="btn btn-danger" >Cancel</a>
                        <!--<button type="button" class="btn btn-default">Save</button>-->                                    
                        <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                        </div>
                    </div>
                
            </form>
                </div>
        </div>
    </div>                    
</div>
</div>
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type='text/javascript' src='js/plugins/jquery-validation/jquery.validate.js'></script>
<script type="text/javascript">
//            $("#btnsubmit").on("click",function(){

            var jvalidate = $("#orderForm").validate({
                rules: {                                            
                        fixture_number: {required: true},
                        quantity_no: {required: true},
                        packing_dispatch: {required: true},
                        dap_send_to_customer: {required: true},
                        conf_dap: {required: true},
                        design_release: {required: true},
                        raw_material: {required: true},
                        imported_boughtout: {required: true},
                        manufacturing: {required: true},
                        quality_inspection: {required: true},
                        fixture_assembly: {required: true},
                        receipt_of_component: {required: true},
                        assembly_cmm : {required: true},
                        machining_trial: {required:true}
                    }                                        
                });
                
                $('#btnsubmit').on('click', function() {
                    $("#orderForm").valid();
                });
                
                $("#btnsave").click(function(){
                    $(".abc").removeAttr('required');
                    $("#orderForm").validate({
                        rules: {  
                                time_plan : {required: true},
                                fixture_number: {required: true},
                            }                                        
                        });
                    $("#orderForm").submit();
                })
//            })
                 $("#fixture_number").focusout(function(){
                     var fixture_no = $(this).val();
                     $.ajax({
                            url: 'fixture-validate/' + fixture_no,
                            type: "GET",
                            success: function(data) {
                            console.log(data);
                            $("#fixture_validate").html(data);
                            if (data!= ""){
                                    $("#fixture_number").val("");
                            }
                            }
                    });
                    });
               

        </script>
        
@endsection