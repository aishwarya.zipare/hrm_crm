@extends('layouts.app')
@section('title', 'Leave Approval')
@section('content')
//<?php // print_r($leave_list);
//exit;?>
<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <!--<h4 class="page-title">User</h4>-->
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('home')}}">Leave Approval</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Leave Approval</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    <!-- START WIDGETS -->                   
    <div class="row">
        <div class="col-md-12">
                <?php $x = 1; ?>
                            <div class="card"><div class="card-body">
                                <h5 class="card-title">LEAVES LIST</h5>
                        <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="font-weight: bold;width:50px;">Emp ID</th>
                                    <th>Name</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Reason</th>
                                    <th>Total Days</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                @foreach($leave_list as $row)
                                <tr>
                                    <td>{{$row->emp_id}}</td>
                                    <td>{{$row->emp_name}}</td>
                                    <td>{{$row->from_date}}</td>
                                    <td>{{$row->to_date}}</td>
                                    <td>{{$row->reason}}</td>
                                    <td>{{$row->total_days}}</td>
                                    <td>
                                        <a href="{{ url('approve?id='.$row->id)}}" style="color:green"><button class="btn btn-success">Approve</button></a>
                                        <a href="{{ url('decline?id='.$row->id)}}" style="color:red" class="delete"><button class="btn btn-danger">Decline</button></a>
                                    </td>

                                </tr>
                                <?php $i++;?>
                             @endforeach   
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->   
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script>
$(document).ready(function(){
//    alert();
//    $(".delete").on("click",function(){
//        return confirm('Are you sure to delete user');
//    });
});
</script>
@endsection
