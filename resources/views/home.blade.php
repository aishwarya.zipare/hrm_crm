@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<!--{{ config('constants.APP_KEY') }}-->
<?php 
$data = array();
$pi_arr = array();
$user_id = Auth::user()->id;
//echo $user_id;exit;
$process_count = App\Process::count();
$ticket_count = DB::table('tbl_tickets')
              ->count();
$rating_count = DB::table('tbl_rating_form')
              ->select('tbl_rating_form.process','tbl_process.process_name')
              ->leftjoin('tbl_process','tbl_process.id','=','tbl_rating_form.process')
              ->distinct()
              ->get();
$i =0;
foreach($rating_count as $row){
    $data['name'][] = $row->process_name;
    $pi_arr[$i]['name'] = $row->process_name;
    $rating_count = DB::table('tbl_rating_form')
              ->select('tbl_rating_form.ticket_no')
              ->where(['process'=>$row->process])
              ->count();
    $pi_arr[$i]['y'] = $rating_count;
    $data['data'][] = $rating_count;
    $i++;
}
//echo "<pre>";print_r($pi_arr);exit;
              
?>
<link href="notepad-css/bootstrap.css" rel="stylesheet">
<link href="notepad-css/dist/summernote.css" rel="stylesheet">
<!-- START BREADCRUMB -->
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Dashboard</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>   
     @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-lg-2">
              
            </div>
<!--            <div class="col-md-6 col-lg-3">
                <a href="" >
                <div class="card card-hover">
                    <div class="box bg-cyan text-center">
                        <h1 class="font-light text-white"><i class="fas fa-shopping-cart"></i></h1>
                        <h6 class="text-white"> Process Count {{$process_count}}</h6>
                    </div>
                </div>
                </a>
            </div>-->
            <div class="col-md-6 col-lg-2">
               
            </div>
<!--            <div class="col-md-6 col-lg-3">
                <a href="" >
                <div class="card card-hover">
                    <div class="box bg-success text-center">
                        <h1 class="font-light text-white"><i class="fas fa-shopping-cart"></i></h1>
                        <h6 class="text-white"> Ticket Count {{$ticket_count}}</h6>
                    </div>
                </div>
                </a>
            </div>-->
            <div class="col-md-6 col-lg-2">
               
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-6">
                <div id="container"></div>
            </div>
            <div class="col-md-6">
                <div id="container1" ></div>
            </div>
        </div>
    </div>
     <!--END BREADCRUMB-->    
     <!--PAGE CONTENT WRAPPER--> 
</div>
<br>
<br>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="notepad-css/js/jquery.js"></script> 
<script>
Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Process Wise Ticket Count'
    },
    xAxis: {
        categories: <?php // echo json_encode($data['name']); ?>,
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Ticket Count'
        },
        stackLabels: {
            enabled: true,
            style: {
                fontWeight: 'bold',
                color: ( // theme
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || 'gray'
            }
        }
    },
    legend: {
        align: 'right',
        x: -30,
        verticalAlign: 'top',
        y: 25,
        floating: true,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || 'white',
        borderColor: '#CCC',
        borderWidth: 1,
        shadow: false
    },
    tooltip: {
        headerFormat: '<b>{point.x}</b><br/>',
        pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
//                enabled: true
            }
        }
    },
    series: [{
        name: 'Process',
        data: <?php // echo json_encode($data['data']); ?>

    }],
    credits: {
                        enabled: false
                    },
});


// Build the chart
Highcharts.chart('container1', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Top Trending Process'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: <?php echo json_encode($pi_arr); ?>
    }],
    credits: {
                        enabled: false
                    },
});

</script>
@endsection