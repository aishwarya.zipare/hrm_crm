@extends('layouts.app')
@section('title', 'Activity Calender')
@section('content')
<div class="page-wrapper">
        <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Activity</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Activity</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
       <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="">
                                <div class="row">
                                    <div class="col-lg-3 border-right p-r-0">
                                        <div class="card-body border-bottom">
                                            <h4 class="card-title m-t-10">Drag & Drop Event</h4>
                                        </div>
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="calendar-events" class="">
                                                        <div class="calendar-events m-b-20" data-class="bg-info"><i class="fa fa-circle text-info m-r-10"></i>Event One</div>
                                                        <div class="calendar-events m-b-20" data-class="bg-success"><i class="fa fa-circle text-success m-r-10"></i> Event Two</div>
                                                        <div class="calendar-events m-b-20" data-class="bg-danger"><i class="fa fa-circle text-danger m-r-10"></i>Event Three</div>
                                                        <div class="calendar-events m-b-20" data-class="bg-warning"><i class="fa fa-circle text-warning m-r-10"></i>Event Four</div>
                                                    </div>
                                                    <!-- checkbox -->
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="drop-remove">
                                                        <label class="custom-control-label" for="drop-remove">Remove after drop</label>
                                                    </div>
                                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#add-new-event" class="btn m-t-20 btn-info btn-block waves-effect waves-light">
                                                            <i class="ti-plus"></i> Add New Event
                                                        </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="card-body b-l calender-sidebar" id="eventcalendar">
                                            <div id="calendar"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- BEGIN MODAL -->
                <div class="modal none-border" id="my-event">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><strong>Add Event</strong></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                                <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal Add Category -->
                <div class="modal fade none-border" id="add-new-event">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><strong>Add</strong> a category</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form id="form" method="post" class="form-horizontal">
                                          {{ csrf_field() }}
                                                        <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Process Name</label>
                        <div class="col-sm-4">                                                                                            
                            <select class="select2 form-control custom-select fixture_no" id="fixture_number" name="owner">
                                <option value="1">-- Select --</option>
                
                            </select>
                        </div>
                                  <label class="col-sm-2 text-right control-label col-form-label">Description</label>
                        <div class="col-sm-4">                                                                                            
                            <textarea name="description" id="description" class="required form-control" rows="5"></textarea>
                        </div>
            
                    </div>
                                    <div class="form-group row">
                                        <!--<div class="col-md-6">-->
                                            <label class="col-sm-2 text-right control-label">Category Name</label>
                                            <div class="col-sm-4">
                                            <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Status</label>
                                            <select class="form-control form-white" data-placeholder="Choose a color..." name="status">
                                                <option value="success">Success</option>
                                                <option value="danger">Danger</option>
                                                <option value="info">Info</option>
                                                <option value="primary">Primary</option>
                                                <option value="warning">Warning</option>
                                                <option value="inverse">Inverse</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                
                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade " id="add_activity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container1">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Activity</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="activity_form" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div id="cover-spin"></div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Activity Name</label>
                        <div class="col-sm-4">
                            <input id="detail_no" name="activity_name" type="text" class="required form-control checkblank" required>
                        </div>
                        <label for="surname" class="col-sm-2">Owner</label>
                        <div class="col-sm-4">
                            <select class="form-control form-white" data-placeholder="Choose a color..." name="owner" id="owner">
                                <option value="">Select Owner</option>    
                                    @foreach($user_data as $data)
                                               <option value="{{$data->id}}">{{$data->name}}</option>
                                               @endforeach
                                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Status</label>
                        <div class="col-sm-4">
                                         <select class="form-control form-white" data-placeholder="Choose a color..." name="status" id="status">
                                                <option value="">Select Status</option> 
                                                <option value="Planned">Planned</option>
                                                <option value="Missed">Missed</option>
                                                <option value="Completed">Completed</option>
                                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Description</label>
                        <div class="col-sm-10">
                            <textarea name="description" id="description" class="required form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="activity_date" id="activity_date"  />
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                  <button type="button" id="btnsubmit" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                    <!--<button type="button" class="btn btn-primary btn_sbt" >Save changes</button>-->
                </div>
            </form>
        </div>
    </div>
</div>
                 <div class="modal fade " id="view_activity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container1">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Activity Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="activity_update" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div id="cover-spin"></div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Activity Name</label>
                        <div class="col-sm-4">
                            <input id="activity_name" name="activity_name" type="text" class="required form-control checkblank1" required>
                        </div>
                        <label for="surname" class="col-sm-2">Owner</label>
                        <div class="col-sm-4">
                            <select class="form-control form-white"  name="owner" id="owner">
                                <option value="">Select Owner</option>    
                                    @foreach($user_data as $data)
                                               <option value="{{$data->id}}">{{$data->name}}</option>
                                               @endforeach
                                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Status</label>
                        <div class="col-sm-4">
                                         <select class="form-control form-white"  name="status" id="status">
                                             <option value="">Select Status</option> 
                                                <option value="Planned">Planned</option>
                                                <option value="Missed">Missed</option>
                                                <option value="Completed">Completed</option>
                                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Description</label>
                        <div class="col-sm-10">
                            <textarea name="description" id="description" class="required form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id"  />
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                  <button type="button" id="btnupdate" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                    <!--<button type="button" class="btn btn-primary btn_sbt" >Save changes</button>-->
                </div>
            </form>
        </div>
    </div>
</div>
                <div id="calendarModal" class="modal fade">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
            <h4 id="modalTitle" class="modal-title"></h4>
        </div>
        <div id="modalBody" class="modal-body"> </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
</div>
    </div>   
</div> 
<script src="assets/libs/jquery/dist/jquery.min.js"></script>
<script src="js/jquery.steps.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $("#eventcalendar").show();
  $('#calendar').fullCalendar({
            // put your options and callbacks here
            header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
         editable: true,
            selectable: true,

            //When u select some space in the calendar do the following:
            select: function (start, end, allDay) {
                //do something when space selected
                //Show 'add event' modal
                $('#activity_date').val(start);
                $('#add_activity').modal('show');
            },
            events : [
                @foreach($activity_data as $data)
                {
                    title : '{{ $data->activity_name }}',
                    id : '{{ $data->id }}',
                    start : '{{ $data->activity_date }}',
                    owner : '{{ $data->owner }}',
                    desc : '{{ $data->description }}',
                    status : '{{ $data->status }}',
                   
                },
                @endforeach
            ],
            eventClick:  function(event, jsEvent, view) {
            $("#btnupdate").attr("disabled", false);
            $('#modalTitle').html(event.title);
            $('#modalBody').html(event.description);
            $('#eventUrl').attr('href',event.url);
           // alert(event.owner);
           $("#owner").val(event.owner).attr("selected", "selected");
            $("#owner").trigger('change.select2');
            $('#id').val(event.id);
            $('#activity_name').val(event.title);
            $("#status").val(event.status).attr("selected", "selected");
            $("#status").trigger('change.select2');
            $('#description').val(event.desc);
            if(event.status == "Completed")
            $("#btnupdate").attr("disabled", true);
            $('#view_activity').modal();
        },
        })
        var flag=0;
 $("#btnsubmit").click(function(){
       $(".checkblank").each(function() {
                    var val=$(this).val();
                    if($(this).val() == "")
                    {
                         flag=1;
                         return false;
                    }
                    else
                    {
                        flag=0;
                    }                    
                });
    if(flag == 1)
    {
         swal({
  position: 'top-end',
  type: 'warning',
  title: 'Please Fill All Fields',
  showConfirmButton: false,
  timer: 1500
}); 
    }
    else
    {
         $.ajax({
                    type: "POST",
                            url: 'save_activity',
                            async: false,
                            cache: false,
                            beforeSend: function(){
                                    // Show image container
                                    //alert("hjl");
                                    $("#loader").show();
                                    },
                            data: $("#activity_form").serialize(),
                            success: function(data){
                            console.log(data);
                            // swal({ type: "success", title: "Good Job!", confirmButtonColor: "#292929", text: "LogSheet Sumbmitted Successfully", confirmButtonText: "Ok" });
                            swal({
                            title: "Successfully Inserted!",
                                    text: "",
                                    type: "success",
                                    showCancelButton: false,
                                    closeOnConfirm: false,
                                    confirmButtonColor: "#e74c3c",
                                    showLoaderOnConfirm: true
                            },
                                    function(){
                                    setTimeout(function(){
                                    location.reload(true);
                                    }, 1000);
                                    });
                                    $('.next_btn').attr("disabled",false);
                            },
                            complete:function(data){
                                    // Hide image container
                                    $("#loader").hide();
                                    }
                    });  
    }
   
 });
 $("#btnupdate").click(function(){
       $(".checkblank1").each(function() {
                    var val=$(this).val();
                    if($(this).val() == "")
                    {
                         flag=1;
                         return false;
                    }
                    else
                    {
                        flag=0;
                    }                    
                });
    if(flag == 1)
    {
         swal({
  position: 'top-end',
  type: 'warning',
  title: 'Please Fill All Fields',
  showConfirmButton: false,
  timer: 1500
}); 
    }
    else
    {
         $.ajax({
                    type: "POST",
                            url: 'update_activity',
                            async: false,
                            cache: false,
                            beforeSend: function(){
                                    // Show image container
                                    //alert("hjl");
                                    $("#loader").show();
                                    },
                            data: $("#activity_update").serialize(),
                            success: function(data){
                            console.log(data);
                            // swal({ type: "success", title: "Good Job!", confirmButtonColor: "#292929", text: "LogSheet Sumbmitted Successfully", confirmButtonText: "Ok" });
                            swal({
                            title: "Successfully Inserted!",
                                    text: "",
                                    type: "success",
                                    showCancelButton: false,
                                    closeOnConfirm: false,
                                    confirmButtonColor: "#e74c3c",
                                    showLoaderOnConfirm: true
                            },
                                    function(){
                                    setTimeout(function(){
                                    location.reload(true);
                                    }, 1000);
                                    });
                                    $('.next_btn').attr("disabled",false);
                            },
                            complete:function(data){
                                    // Hide image container
                                    $("#loader").hide();
                                    }
                    });  
    }
   
 });
});
</script>
@endsection

