@extends('layouts.app')
@section('title', 'Activity Calender')
@section('content')
<div class="page-wrapper">
        <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Activity</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Activity</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
       <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="">
                                <div class="row">
                                    <div class="col-lg-1"></div>
                                    <div class="col-lg-10">
                                        <div class="card-body b-l calender-sidebar">
                                            <div id="calendar"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- BEGIN MODAL -->
                <div class="modal none-border" id="my-event">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><strong>Add Event</strong></h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body"></div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                                <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal Add Category -->
                <div class="modal fade none-border" id="add-new-event">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title"><strong>Add</strong> a category</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form id="form" method="post" class="form-horizontal">
                                          {{ csrf_field() }}
                                                        <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Process Name</label>
                        <div class="col-sm-4">                                                                                            
                            <select class="select2 form-control custom-select fixture_no" id="fixture_number" name="owner">
                                <option value="1">-- Select --</option>
                
                            </select>
                        </div>
                                  <label class="col-sm-2 text-right control-label col-form-label">Description</label>
                        <div class="col-sm-4">                                                                                            
                            <textarea name="description" id="description" class="required form-control" rows="5"></textarea>
                        </div>
            
                    </div>
                                    <div class="form-group row">
                                        <!--<div class="col-md-6">-->
                                            <label class="col-sm-2 text-right control-label">Category Name</label>
                                            <div class="col-sm-4">
                                            <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
                                        </div>
                                        <div class="col-md-6">
                                            <label class="control-label">Status</label>
                                            <select class="form-control form-white" data-placeholder="Choose a color..." name="status">
                                                <option value="success">Success</option>
                                                <option value="danger">Danger</option>
                                                <option value="info">Info</option>
                                                <option value="primary">Primary</option>
                                                <option value="warning">Warning</option>
                                                <option value="inverse">Inverse</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                
                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade " id="add_activity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container1">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Activity</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="activity_form" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div id="cover-spin"></div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Activity Name</label>
                        <div class="col-sm-4">
                            <input id="detail_no" name="activity_name" type="text" class="required form-control checkblank" required>
                        </div>
                        <label for="surname" class="col-sm-2">Owner</label>
                        <div class="col-sm-4">
                            <select class="select2 required form-control custom-select"  name="owner" id="owner" style="width:90%;">
                                <option value="">Select Owner</option>    
                                    @foreach($user_data as $data)
                                               <option value="{{$data->id}}">{{$data->name}}</option>
                                               @endforeach
                                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Status</label>
                        <div class="col-sm-4">
                                         <select class="select2 required form-control custom-select" data-placeholder="Choose a color..." name="status" style="width:90%;">
                                                <option value="Planned">Planned</option>
                                                <option value="Missed">Missed</option>
                                                <option value="Completed">Completed</option>
                                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Description</label>
                        <div class="col-sm-10">
                            <textarea name="description" id="description" class="required form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="activity_date" id="activity_date"  />
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                  <button type="button" id="btnsubmit" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                    <!--<button type="button" class="btn btn-primary btn_sbt" >Save changes</button>-->
                </div>
            </form>
        </div>
    </div>
</div>
                 <div class="modal fade " id="view_activity" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container1">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Activity Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="activity_update" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div id="cover-spin"></div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Activity Name</label>
                        <div class="col-sm-4">
                            <input id="activity_name" name="activity_name" type="text" class="required form-control checkblank1" required>
                        </div>
                        <label for="surname" class="col-sm-2">Owner</label>
                        <div class="col-sm-4">
                             <select class="select2 required form-control custom-select"  name="owner" id="update_owner" style="width:90%;">
                                <option value="">Select Owner</option>    
                                    @foreach($user_data as $data)
                                               <option value="{{$data->id}}">{{$data->name}}</option>
                                               @endforeach
                                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Status</label>
                        <div class="col-sm-4">
                                         <select class="select2 required form-control custom-select" data-placeholder="Choose a color..." name="status" id="status" style="width:90%;">
                                              <option value="">Select Status</option>      
                                             <option value="Planned">Planned</option>
                                                <option value="Missed">Missed</option>
                                                <option value="Completed">Completed</option>
                                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Description</label>
                        <div class="col-sm-10">
                            <textarea name="description" id="update_description" class="required form-control" rows="5"></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id"  />
                   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                  <button type="button" id="btnupdate" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                    <!--<button type="button" class="btn btn-primary btn_sbt" >Save changes</button>-->
                </div>
            </form>
        </div>
    </div>
</div>
               
    </div>   
</div> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha.6/css/bootstrap.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.min.js"></script>
  <script>
   
  $(document).ready(function() {
   var calendar = $('#calendar').fullCalendar({
    editable:true,
    header:{
     left:'prev,next today',
     center:'title',
     right:'month,agendaWeek,agendaDay'
    },
    events : [
                @foreach($activity_data as $data)
                {
                    title : '{{ $data->activity_name }}',
                    id : '{{ $data->id }}',
                    start : '{{ $data->activity_date }}',
                    owner : '{{ $data->owner }}',
                    desc : '{{ $data->description }}',
                    status : '{{ $data->status }}',
                   
                },
                @endforeach
                       {
                    title : 'abc',
                    id : '1',
                    start : '2019-10-16',
                    owner : 'aish',
                    desc : 'fghf',
                    status : 'Planned',
                   
                },
            ],
    selectable:true,
    selectHelper:true,
    select: function(start, end, allDay)
    {
     var title = prompt("Enter Event Title");
     if(title)
     {
      var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
      var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
      $.ajax({
       url:"insert.php",
       type:"POST",
       data:{title:title, start:start, end:end},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Added Successfully");
       }
      })
     }
    },
    editable:true,
    eventResize:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function(){
       calendar.fullCalendar('refetchEvents');
       alert('Event Update');
      }
     })
    },

    eventDrop:function(event)
    {
     var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
     var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
     var title = event.title;
     var id = event.id;
     $.ajax({
      url:"update.php",
      type:"POST",
      data:{title:title, start:start, end:end, id:id},
      success:function()
      {
       calendar.fullCalendar('refetchEvents');
       alert("Event Updated");
      }
     });
    },

    eventClick:function(event)
    {
     if(confirm("Are you sure you want to remove it?"))
     {
      var id = event.id;
      $.ajax({
       url:"delete.php",
       type:"POST",
       data:{id:id},
       success:function()
       {
        calendar.fullCalendar('refetchEvents');
        alert("Event Removed");
       }
      })
     }
    },

   });
  });
   
  </script>
@endsection