@extends('layouts.app')
@section('title', 'Edit Role')
@section('content')
<style>
    .error{
        color:red;
    }
</style>
<div class="page-wrapper">
    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Edit Role</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('user_list')}}">Role</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Role</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                    {!! Form::model($roles,[
                        'method' => 'PUT',
                        'url' => ['update-role',$roles->id],
                        'class'=> 'form-horizontal',
                        'id'=>'orderForm'
                    ]) !!}
                      {{ csrf_field() }} 
                    <div class="card-body">  
                        <div class="form-group row">
                             <label class="col-sm-2 text-right control-label col-form-label">Name</label>
                             <div class="col-sm-4">
                                 <input type="text" class="form-control" name="role_name" id="name" value="{{$roles->role_name}}" required />
                             </div>
                         </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info" value="Submit">
                            <a href="{{url('role_list')}}" class="btn btn-danger" >Cancel</a>
                        </div>
                    </div>
                      </form>
                </div>
                </div>
        </div>
    </div>   
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type='text/javascript' src='js/plugins/jquery-validation/jquery.validate.js'></script>
<script type="text/javascript">
//            $("#btnsubmit").on("click",function(){
            var jvalidate = $("#orderForm").validate({
                rules: {                                            
                        name: {required: true},
                        email: {required: true},
//                        password: {required: true},
                        dept_id: {required: true},
                        role: {required: true},
                    }                                        
                });
                
                $('#btnsubmit').on('click', function() {
                    $("#orderForm").valid();
                });
                
        </script>
        
@endsection
