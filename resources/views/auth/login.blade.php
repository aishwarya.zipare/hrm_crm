@extends('layouts.login')
@section('title', 'Login')
@section('content')
<style>
/*    .help-block{
        color: red;
    }
  .img_back {
        background-image: url("img/IMG_2654.jpg");
        background-repeat:no-repeat;
        background-color: #cccccc;
        background-size: 1400px 700px;
   }
   .auth-wrapper .auth-box {
    background: #fff;
    padding: 20px;
    max-width: 400px;
    width: 90%;
    margin: -14% 10% 10% 15%;
}*/
.btn-block {
    display: block;
    width: 100%;
}
</style>
<div class="main-wrapper">
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Preloader - style you can find in spinners.css -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
<!--        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center img_back">
            <div class="auth-box bg-dark border-top border-secondary">
                <div id="loginform">
                    <div class="text-center p-t-20 p-b-20">
                        <span class="db"><img src="img/Facor.png" alt="logo" style="height: auto;max-width: 35%;vertical-align: middle;border: 0;"/></span>
                    </div>
                     Form 
                    <form action="{{ route('login') }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                    <div class="row p-b-30" style="margin-top: 16px;">
                            <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg" id="email" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" aria-label="E-Mail Address" aria-describedby="basic-addon1" required="">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required="">
                                    
                                </div>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                            </div>
                        </div>
                        <div class="row border-top border-secondary">
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="p-t-20">
                                        <button class="btn btn-info" id="to-recover" type="button"><i class="fa fa-lock m-r-5"></i> Lost password?</button>
                                        <button class="btn btn-success float-right" type="submit">Login</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>-->
         <div class="auth-wrapper d-flex no-block justify-content-center align-items-center bg-dark">
            <div class="auth-box bg-dark border-top border-secondary">
                <div>
                    <div class="text-center p-t-20 p-b-20">
                        <span class="db"><img src="img/mlogo.png" width="150px" height="136px" /></span>
                    </div>
                    <br/>
                    <!-- Form -->
                     <form action="{{ route('login') }}" class="form-horizontal" method="POST">
                    {{ csrf_field() }}
                        <div class="row p-b-30">
                             <div class="col-12">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-success text-white" id="basic-addon1"><i class="ti-user"></i></span>
                                    </div>
                                    <input type="text" class="form-control form-control-lg" id="email" name="email" value="{{ old('email') }}" placeholder="E-Mail Address" aria-label="E-Mail Address" aria-describedby="basic-addon1" required="">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text bg-warning text-white" id="basic-addon2"><i class="ti-pencil"></i></span>
                                    </div>
                                    <input type="password" class="form-control form-control-lg" id="password" name="password" placeholder="Password" aria-label="Password" aria-describedby="basic-addon1" required="">
                                    
                                </div>
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                            </div>
                        </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <div class="p-t-20">
                                        <!--<button class="btn btn-success float-right" type="submit">Login</button>-->
                                        <button class="btn btn-block btn-lg btn-info" type="submit">Login</button>
                                    </div>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Login box.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper scss in scafholding.scss -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right Sidebar -->
        <!-- ============================================================== -->
    </div>
@endsection
