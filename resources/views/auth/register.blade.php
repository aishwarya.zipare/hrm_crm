@extends('layouts.app')
@section('title','Add User')
@section('content')
<style>
    .error{
        color:red;
    }
/*.select2-container--classic .select2-selection--single, .select2-container--default .select2-selection--multiple, .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow, .select2-container--default .select2-selection--single .select2-selection__rendered {
    border-color: #e9ecef;
    height: auto;
    color: #3e5569;
    line-height: 20px;
}*/
    
.select2-container--default .select2-selection--multiple{
    border-color: #e9ecef;
    height: auto;
    color: #3e5569;
    line-height: 20px;
}

</style>
<div class="page-wrapper">

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add New User</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('user_list')}}">User</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Add New User</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" id="orderForm" method="post" action="{{ url('add-user') }}">
                        {{ csrf_field() }}
                        <div class="card-body">  
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="name" id="name" required />
                                </div>
                                <label for="fname" class="col-sm-2 text-right control-label col-form-label">Email</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="email" id="email" required />
                                    <code id="email_validate"></code>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Password</label>
                                <div class="col-sm-4">
                                    <input type="password" class="form-control" name="password" id="password" required />
                                </div>
                                 <label for="fname" class="col-sm-2 text-right control-label col-form-label">Role</label>
                                <div class="col-sm-4">                                                                                            
                                    <select class="select2 form-control custom-select" name="user_role" id="user_role">
                                        @foreach($roles as $role)
                                        <option value="{{$role->id}}">{{$role->role_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>    
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Reporting<br/>Manager</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="reporting_manager_name" id="reporting_manager_name"  />
                                </div>
                                   <label for="fname" class="col-sm-2 text-right control-label col-form-label">Reporting Manager<br/>Mail ID</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="reporting_manager_mail" id="reporting_manager_mail"  />
                                    <code id="email_validate"></code>
                                </div>
                                
                            </div> 
                            <div class="form-group row p_val" style="display:none;">
                             <div class="card col-md-12">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" border="0">
                                        <thead>
                                            <tr>
                                                <th style="width:5px;"><b>Action</b></th>
                                                <th style="width:20px;"><b>Agent ID</b></th>
                                                <th><b>Agent Name</b></th>
                                                <th><b>Agent Email ID</b></th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="h_lost">
                                            <tr class="input_fields_wrap">
                                                <td style="width:0.5%;"><i class="fas fa-plus-circle add_field_button" style="color: #0c8a54;"></i></td>
                                                <td class="parameter" ><input type="text" name="agent_detail[0][0]" class="form-control parameter checkblank" placeholder="Agent ID" style="width:100px;"/></td>
                                                <td class="parameter"><input type="text" name="agent_detail[0][1]" class="form-control parameter checkblank" placeholder="Agent Name" /></td>
                                                
                                                <td><input type="text" name="agent_detail[0][2]" class="form-control  checkblank"  placeholder="Agent Email ID" /></td>
                                                
                            
                                               
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>    
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info" value="Submit">
                                <a href="{{url('user_list')}}" class="btn btn-danger" >Cancel</a>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>                    
    </div>
</div>

<script src="assets/libs/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src='js/plugins/jquery-validation/jquery.validate.js'></script>
<script type="text/javascript">
//            $("#btnsubmit").on("click",function(){
$("document").ready(function () {
    $("#role").on("change", function () {
        var role_val = $(this).val();
//    alert(role_val);
        if (role_val == "3") {
            $(".p_val").css("display", "block");
        } else {
            $(".p_val").css("display", "none");
        }
    })
        //    var max_fields      = 6; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var i = 2;
    var x = 1; //initlal text box count
    var s = 14;
    var l=1;
       $(document).on('click','.add_field_button',function(){
        var v = $(this).parents("td").prevAll(".parameter").eq(1).val();
        i++; 
        l++;
        $("#h_lost").append('<tr class="input_fields_wrap delparam_'+l+'">\n\
            <td style="width:2%;"><i class="fas fa-plus-circle add_field_button" style="color: #0c8a54;"></i><i class="fas fa-minus-circle remove_field" style="color: red;"></i></td>\n\
            <td><input type="text" name="agent_detail['+i+'][]" class="form-control parameter checkblank"   style="width:100px;" placeholder="Agent ID"/></td>\n\
            <td><input type="text" name="agent_detail['+i+'][]" class="form-control parameter checkblank"  placeholder="Agent Name"/></td>\n\
            <td><input type="text" name="agent_detail['+i+'][]" class="form-control parameter checkblank"  placeholder="Agent Email"/></td>\n\
        </tr>'); 
            $('select').select2();
            $('.datepicker-autoclose').datepicker();
        $("#h_lost").on('click','.remove_field',function(){
             $(this).parent().parent().remove();
            
        });
    });
})

$("#email").focusout(function () {
    var email = $(this).val();
    $.ajax({
        url: 'email-validate/' + email,
        type: "GET",
        success: function (data) {
            console.log(data);
            $("#email_validate").html(data);
            if (data != "") {
                $("#email").val("");
            }
        }
    });
});



var jvalidate = $("#orderForm").validate({
    rules: {
        name: {required: true},
        email: {required: true},
        password: {required: true},
        dept_id: {required: true},
        role: {required: true},
    }
});

$('#btnsubmit').on('click', function () {
    $("#orderForm").valid();
});

$("#dept_id").on("change", function () {
    var dept = $(this).val();
    //            var date = $("#date").val();
    $.ajax({
        url: 'sub-dept/' + dept,
        type: "get",
        //data: {batch:batch,date:date},
        success: function (data) {
//                           var obj = JSON.parse(data);
            $("#sub_dept").html(data);
        }
    });
})
</script>

@endsection