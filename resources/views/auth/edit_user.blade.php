@extends('layouts.app')
@section('title', 'Edit user')
@section('content')
<style>
    .error{
        color:red;
    }
</style>
<div class="page-wrapper">
    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Edit User</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('user_list')}}">User</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit User</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                    {!! Form::model($users,[
                        'method' => 'PUT',
                        'url' => ['update-user',$users->id],
                        'class'=> 'form-horizontal',
                        'id'=>'orderForm'
                    ]) !!}
                      {{ csrf_field() }} 
                    <div class="card-body">  
                        <div class="form-group row">
                             <label class="col-sm-2 text-right control-label col-form-label">Name</label>
                             <div class="col-sm-4">
                                 <input type="text" class="form-control" name="name" id="name" value="{{$users->name}}" required />
                             </div>
                             <label for="fname" class="col-sm-2 text-right control-label col-form-label">Email</label>
                             <div class="col-sm-4">
                                 <input type="text" class="form-control" name="email" id="email" value="{{$users->email}}" required />
                             </div>
                         </div>
                        <div class="form-group row">
                            <label class="col-sm-2 text-right control-label col-form-label">Password</label>
                            <div class="col-sm-4">
                                <input type="password" class="form-control" name="password" id="password"  />
                            </div>
                            <label for="fname" class="col-sm-2 text-right control-label col-form-label">Role</label>
                            <div class="col-sm-4">                                                                                            
                                <select class="select2 form-control custom-select" name="user_role" id="user_role" >
                                    <option value="">-- Select Role-- </option>
                                    @foreach($roles as $role)
                                    <option value="{{$role->id}}" <?php if($role->id == $users->user_role) echo "selected"; ?>>{{$role->role_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        <div class="form-group row">
                                    <label class="col-sm-2 text-right control-label col-form-label">Reporting<br/>Manager</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="reporting_manager_name" id="reporting_manager_name"  value="{{$users->reporting_manager_name}}"/>
                                </div>
                                   <label for="fname" class="col-sm-2 text-right control-label col-form-label">Reporting Manager<br/>Mail ID</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="reporting_manager_mail" id="reporting_manager_mail" value="{{$users->reporting_manager_mail}}" />
                                    <code id="email_validate"></code>
                                </div>
                        </div> 
                    </div>
                      <?php if($users->role==3){ ?>
                       <div class="form-group row p_val">
                             <div class="card col-md-12">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" border="0">
                                        <thead>
                                            <tr>
                                                <th style="width:5px;"><b>Action</b></th>
                                                <th style="width:20px;"><b>Agent ID</b></th>
                                                <th><b>Agent Name</b></th>
                                                <th><b>Agent Email ID</b></th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="h_lost">
                                            <?php $i=1;foreach($agent_data as $data) {?>
                                            <tr class="input_fields_wrap">
                                                <td style="width:0.5%;"><i class="fas fa-plus-circle add_field_button" style="color: #0c8a54;"></i></td>
                                                <td class="parameter" ><input type="text" name="agent_detail[{{$i}}][0]" class="form-control parameter checkblank" placeholder="Agent ID" value="{{$data->agent_id}}" style="width:100px;"/></td>
                                                <td class="parameter"><input type="text" name="agent_detail[{{$i}}][1]" class="form-control parameter checkblank" placeholder="Agent Name" value="{{$data->agent_name}}" /></td>
                                                <input type="hidden"name="agent_detail[{{$i}}][3]" value="{{$data->id}}"/>
                                                <td><input type="text" name="agent_detail[{{$i}}][2]" class="form-control  checkblank"  placeholder="Agent Email ID" value="{{$data->agent_email}}"/></td>
                                                
                            
                                               
                                            </tr>
                                            <?php $i++;} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>    
                            </div>
                      <?php } ?>
                    <div class="border-top">
                        <div class="card-body">
                            <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info" value="Submit">
                            <a href="{{url('user_list')}}" class="btn btn-danger" >Cancel</a>
                        </div>
                    </div>
                      </form>
                </div>
                </div>
        </div>
    </div>   
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type='text/javascript' src='js/plugins/jquery-validation/jquery.validate.js'></script>
<script type="text/javascript">
//            $("#btnsubmit").on("click",function(){
            var jvalidate = $("#orderForm").validate({
                rules: {                                            
                        name: {required: true},
                        email: {required: true},
//                        password: {required: true},
                        dept_id: {required: true},
                        role: {required: true},
                    }                                        
                });
                
                $('#btnsubmit').on('click', function() {
                    $("#orderForm").valid();
                });
                
        </script>
        
@endsection
