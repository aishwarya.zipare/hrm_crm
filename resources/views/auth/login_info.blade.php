@extends('layouts.app')
@section('title', 'LogIn-Info')
@section('content')
 <div id="content-header">
    <div id="breadcrumb"> <a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> LogIn User Detail</a> <a href="#" class="current">User Detail</a> </div>
  </div>
<div class="container-fluid">
    <div class="row-fluid" style="margin-top: 0px">
        <div class="span10 box" id="Daily_Bkg" >
            <div class="widget-box">
                <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
                    <h5 style="color: white">LogIn User Detail</h5>
                </div>
                <form action="{{url('login-info')}}" method="post" id="report_form" >
                    {{ csrf_field() }}
                    <div class="widget-content">
                        <div class="controls controls-row ">
                            <label class="control-label span2">From Date <span style='color: red'>*</span></label>
                                <div data-date="12-02-2012" class="input-append date datepicker span4">
                                    <span class="add-on"><i class="icon-th"></i></span> 
                                    <input type="text" name="from_date" id="from_date" data-date="01-02-2013" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" tabindex=1 style="width: 173px;" >
                                </div>
                            <label class="control-label span2">To Date <span style='color: red'>*</span></label>
                                <div data-date="12-02-2012" class="input-append date datepicker span4">
                                    <span class="add-on"><i class="icon-th"></i></span> 
                                    <input type="text" name="to_date" id="to_date" data-date="01-02-2013" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy" tabindex=1 style="width: 173px;" >
                                </div>
                        </div>
                        <div class="form-actions">
                            <button type="button" id="btnsubmit" class="btn btn-success" >Get Report</button>
                            <!--<button type="submit" id="btnsubmit1" class="btn btn-success">Download</button>-->
                            <a href="{{url('login-info')}}" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div id="report_view">
            
        </div>
    </div>
</div>
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/bootstrap-datepicker.js"></script>
<script>
$(document).ready(function () {
    $('input[type=checkbox],input[type=radio],input[type=file]').uniform();
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
           }).on('changeDate', function (ev) {
                $(this).datepicker('hide');
        });
        $('select').select2();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $("#btnsubmit").on('click',function(){
            $.ajax({
              url: 'login-info',
              type: 'post',
              data: $("#report_form").serialize(),
              success: function (data) {
                  console.log(data);
                  $("#report_view").html(data);
              }
            })
        })
});
</script>
@endsection


