<?php 
$user_id = Auth::user()->id;
//echo $user_id;exit;
$date = date('Y-m-d');
    $approval_notification = DB::table('tbl_approval_notification')
                        ->where('read_status' ,'=', 0)
                        ->where('user_id','=',$user_id)
                        ->get();
//    print_r($user_id);exit;
    ?>
<style>
    .fa-2x {
     font-size: 2.5em; 
}
.fa-stack-1x {
    left: 0px;
    position: absolute;
    text-align: center;
    width: 100%;
}

#ex3 .fa-stack[data-count]:after{
  position:absolute;
  right:0%;
  top:1%;
  content: attr(data-count);
  font-size:30%;
  padding:.6em;
  border-radius:50%;
  line-height:.8em;
  color: white;
  background:rgba(255,0,0,.85);
  text-align:center;
  min-width: 1em;
  font-weight:bold;
}
.fa-stack {
    display: inline-block;
    height: 1em;
    line-height: 1em;
    position: relative;
    vertical-align: middle;
    width: 1em;
}
.topbar .mailbox, .topbar .user-dd {
    min-width: 375px;
}

.topbar .mailbox, .topbar .user-dd {
    width: 30em;
    overflow-x: auto;
    white-space: nowrap;
}
::-webkit-scrollbar {width: 6px; height: 4px; background: #ffffff; }
::-webkit-scrollbar-thumb { background-color: #eeeeee; -webkit-border-radius: 1ex; }
</style>
<header class="topbar" data-navbarbg="skin5" id="div_refersh">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <div class="navbar-header" data-logobg="skin5">
                    <!-- This is for the sidebar toggle which is visible on mobile only -->
                    <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                    <!-- ============================================================== -->
                    <!-- ============================================================== -->
                    <a class="navbar-brand" href="{{url('home')}}">
                        <b class="logo-icon p-l-10" style="padding-left: 0px;">
                            <img src="img/mlogo.png" alt="homepage" class="light-logo" style="    width: 90px;height: 70px;margin-left: 50px;margin-top: 20px;"/>
                        </b>
                    </a>
                    <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                </div>
                <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                    <ul class="navbar-nav float-left mr-auto">
                        <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                    </ul>
                   
                    <ul class="navbar-nav float-right">
                          <li class="nav-item dropdown" >
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" id="ex3" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="p1 fa-stack fa-2x has-badge" id="notification_count" data-count="<?php  echo (count($approval_notification));  ?>">
                                    <i class="p3 mdi mdi-bell font-24 fa-stack-1x xfa-inverse" data-count="4b"></i>
                                </span>
                                <audio id="audio" src="censor-beep-2.wav"></audio>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown scroll" aria-labelledby="2" style="height: 300px;">
                                <ul class="list-style-none">
                                    <li>
                                        <div class="">
                                        <?php // if($roll!= 1) { 
                                            foreach($approval_notification as $msg) { 
                                                $fixture_no = App\Process::select('process_name')->where(['id'=>$msg->process_id])->first();
                                            ?>
                                            <a class="link border-top notify_link" href="{{url('approval_process?id='.$msg->id)}}">
                                                <div class="d-flex no-block align-items-center p-10">
                                                    <span class="btn btn-danger btn-circle"><i class="fas fa-shopping-cart"></i></span>
                                                    <div class="m-l-10">
                                                        <?php if($msg->notify_from =="QA") {?>
                                                            <span class="mail-desc">New Process Created : {{$fixture_no->process_name}}</span> 
                                                        <?php }// else { ?>
                                                            <!--<span class="mail-desc">Approved Process : {{$fixture_no->process_name}}</span>--> 
                                                        <?php //} ?>
                                                    </div>
                                                </div>
                                            </a>
                                            <!--<div class="dropdown-divider">Design Release</div>-->
                                            <?php } ?>   
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                      <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="assets/images/users/1.jpg" alt="user" class="rounded-circle" width="31"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                
                                <div class="dropdown-divider"></div>
                                <a href="{{route('logout')}}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item"><i class="fa fa-power-off m-r-5 m-l-5"></i>  Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->   
                    </ul>
                       
                    </ul>
                </div>
            </nav>
        </header>   
<script src="assets/libs/jquery/dist/jquery.min.js"></script>
<script>
    setInterval(function() {
        var data1 = $("#notification_count").attr("data-count");
//        $('.gritter-item').css('background','green');
        $.ajax({
           url: 'check-notifications/'+data1,
           type: 'get',
           success: function (data) {
              var Data = JSON.parse(data);
              console.log(Data);
                    if(Data.complition_flag == 0){
//                       $('#audio')[0].play();
                        $.gritter.add({
                            title:	'New Notification',
                            text:	'You have '+ Data.alert +' unread messages.',
                            image: 	'img/envelope.png',
                            sticky: false
                        });
                        $('#div_refersh').load('header').fadeIn("slow");
//                        var playPromise = audio.play();
//
//                        if (playPromise !== undefined) {
//                          playPromise.then(_ => {
//                              $('#audio')[0].play();
//                          })
//                          .catch(error => {
//                            // Auto-play was prevented
//                            // Show paused UI.
//                          });
//                        }                        
                    }
           }
       });
       }, 10000); 
</script>