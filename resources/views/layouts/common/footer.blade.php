<style>
.footer {
  position: fixed;
  left: 0;
  bottom: 0;
  width: 100%;
  background-color: red;
  color: white;
  text-align: center;
}
</style>

<div class="footer text-center">
     <?php echo date("Y"); ?> &copy; iPing Data Labs
</div>

<script src="assets/js/jquery.min.js"></script> 
<script src="assets/js/jquery.ui.custom.js"></script> 
<script src="assets/js/bootstrap.min.js"></script> 
<script src="assets/js/bootstrap-datepicker.js"></script> 
<script src="js/bootstrap-timepicker.js"></script>
<script src="assets/js/jquery.uniform.js"></script>
<script src="js/flash.js"></script>
<script src="js/select2.min.js"></script>
<!--<script src="assets/js/select2.min.js"></script>-->
<script src="assets/js/jquery.validate.js"></script> 
<script src="assets/js/jquery.wizard.js"></script> 
<script src="assets/js/matrix.js"></script> 
<script src="assets/js/matrix.wizard.js"></script>


</script>
</body>
</html>