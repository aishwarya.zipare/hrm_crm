<?php 
    $user_role = Auth::user()->role; 
    $department = explode(",", Auth::user()->dept_id);
?>
<aside class="left-sidebar" data-sidebarbg="skin5">
<div class="scroll-sidebar">
    <nav class="sidebar-nav">
        <ul id="sidebarnav" class="p-t-30">
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('home')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
           
           
<!--            <li class="sidebar-item <?php if ((Request::is('user_list')) || (Request::is('add-user')) || (Request::is('process_form')) || (Request::is('rating_form')) || (Request::is('approval_form'))) { ?> active <?php } ?>"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Master Data</span></a>
                <ul aria-expanded="false" class="collapse  first-level">
                    <li class="sidebar-item <?php if(Request::is('user_list'))  { ?> active <?php } ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('user_list')}}" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">User List</span></a></li>
                    <li class="sidebar-item <?php if(Request::is('role_list'))  { ?> active <?php } ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('role_list')}}" aria-expanded="false"><i class="mdi mdi-account"></i><span class="hide-menu">Add Role</span></a></li>
           <
            <li class="sidebar-item <?php if(Request::is('process_form'))  { ?> active <?php } ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('process_form')}}" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Add Process</span></a></li>
            <li class="sidebar-item <?php if(Request::is('rating_form'))  { ?> active <?php } ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('rating_form')}}" aria-expanded="false"><i class="mdi mdi-pencil-box"></i><span class="hide-menu">Rating</span></a></li>
            <li class="sidebar-item <?php if(Request::is('approval_form'))  { ?> active <?php } ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('approval_form')}}" aria-expanded="false"><i class="mdi mdi-check"></i><span class="hide-menu">Process Approval</span></a></li>
            
                </ul>
            </li>-->
            <!--<li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('calender')}}" aria-expanded="false"><i class="mdi mdi-calendar-check"></i><span class="hide-menu">Activity Calender</span></a></li>-->
<!--            <li class="sidebar-item <?php if ((Request::is('process_report')) || (Request::is('rating_report_form'))) { ?> active <?php } ?>"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-file-alt"></i><span class="hide-menu">Report</span></a>
                <ul aria-expanded="false" class="collapse  first-level">
                    <li class="sidebar-item <?php if(Request::is('process_report'))  { ?> active <?php } ?>"><a href="{{url('process_report')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"></span>Process Report</a></li>
                    <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('rating_report_form')}}" aria-expanded="false"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Rating Report</span></a></li>
                </ul>
            </li>-->
            <?php if($user_role=="admin")
            {?>
            <li class="sidebar-item <?php if ((Request::is('upload-att-details')) || (Request::is('emp-details')) ) { ?> active <?php } ?>"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="m-r-10 mdi mdi-contacts"></i><span class="hide-menu">Employee</span></a>
                <ul aria-expanded="false" class="collapse  first-level">
                    <li class="sidebar-item <?php if(Request::is('emp-details'))  { ?> active <?php } ?>"><a href="{{url('emp-details')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"></span>Employee Details</a></li>
                    <li class="sidebar-item <?php if(Request::is('upload-att-details'))  { ?> active <?php } ?>"><a href="{{url('upload-att-details')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"></span>Upload Attendance</a></li>
                </ul>
            </li>
            <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('sal-details')}}" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Salary Details</span></a></li>
             <li class="sidebar-item <?php if ((Request::is('leave_app')) || (Request::is('show_leaves')) ) { ?> active <?php } ?>"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="m-r-10 mdi mdi-contacts"></i><span class="hide-menu">Leaves</span></a>
                <ul aria-expanded="false" class="collapse  first-level">
                    <li class="sidebar-item <?php if(Request::is('leave_app'))  { ?> active <?php } ?>"><a href="{{url('leave_app')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"></span>Leave Approval</a></li>
                    <li class="sidebar-item <?php if(Request::is('show_leaves'))  { ?> active <?php } ?>"><a href="{{url('show_leaves')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"></span>Show Leaves</a></li>
                </ul>
            </li>
                <?php }?>
            <?php if($user_role=="employee")
            {?>
            <li class="sidebar-item <?php if(Request::is('emp_leave_app'))  { ?> active <?php } ?>"><a href="{{url('emp_leave_app')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"></span>Leave Application</a></li>
            <?php }?>
            
            
            
             
            
<!--<li class="sidebar-item <?php if(Request::is('leave_app'))  { ?> active <?php } ?>"><a href="{{url('leave_app')}}" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"></span>Leave Approval</a></li>-->
<!--            <li class="sidebar-item <?php if(Request::is('feedback-form'))  { ?> active <?php } ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('feedback-form')}}" aria-expanded="false"><i class="fas fa-file-alt"></i><span class="hide-menu">Feedback Form</span></a></li>
            <li class="sidebar-item <?php if(Request::is('call-eval-form'))  { ?> active <?php } ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{url('call-eval-form')}}" aria-expanded="false"><i class="fas fa-file-alt"></i><span class="hide-menu">Call Evaluation</span></a></li>-->
        </ul>
    </nav>
</div>
</aside>
