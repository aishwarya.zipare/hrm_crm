@extends('layouts.app')
@section('title', 'View Rating')
@section('content')
<?php
//echo "<pre>";
//echo $process_name;
//exit;
$para_details=json_decode($rating_details->para_details);
//print_r($para_details);exit;
?>
<style>

    /*    .wizard > .content {
        background: #fff;}*/
    span .select2-selection__rendered{
        width: 308.063px;
    }
    table{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    .container1 {
        /*width: 30em;*/
        overflow-x: auto;
        white-space: nowrap;
    }
    table, tr, td,th {
     border: 1px solid #d2cdcd;   
    }
    div.details{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    div.details1{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
.loading-image {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 10;
}
.loader
{
    display: none;
    width:200px;
    height: 200px;
    position: fixed;
    top: 50%;
    left: 50%;
    text-align:center;
    margin-left: -50px;
    margin-top: -100px;
    z-index:2;
}
@media only screen and (max-width:600px){
	.abc{
		font-size : 10px;
	}
        .loader
        {
            display: none;
            width:200px;
            height: 200px;
            position: fixed;
            top: 50%;
            left: 50%;
            text-align:center;
            margin-left: -180px;
            margin-top: -100px;
            z-index:2;
        }
}
</style>
<link href="css/sweetalert.css" rel="stylesheet" />
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Rating Details</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <!--<li class="breadcrumb-item"><a href="#">BOM</a></li>-->
                            <li class="breadcrumb-item active" aria-current="page">Rating Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Error!</h4>
        {{ Session::get('error') }}
    </div>
    @endif
    <!-- END BREADCRUMB -->
    <!-- PAGE CONTENT WRAPPER -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" id="rating-form" method="post" action="{{ url('save_rating') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-sm-1">Ticket No.</label>
                                <label>:</label>
                                <div class="input-group col-sm-4">
                                    <label>{{$rating_details->ticket_no}}</label>
                                </div>
                                <label class="col-sm-1"></label>
                                <label class="col-sm-1">Date</label>
                                <label>:</label>
                                <div class="input-group col-sm-4">
                                    <label>{{$rating_details->date}}</label>
                                </div>
                                
                                
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-1">Name</label>
                                <label>:</label>
                                <div class="input-group col-sm-4">
                                    <label>{{$rating_details->t_name}}</label>
                                </div>
                                <label class="col-sm-1"></label>
                                <label class="col-sm-1">Mobile No.</label>
                                 <label>:</label>
                                <div class="input-group col-sm-4">
                                    <label>{{$rating_details->t_mobile_no}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-1">Location</label>
                                <label>:</label>
                                <div class="input-group col-sm-4">
                                    <label>{{$rating_details->t_location}}</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-1">Process</label>
                                <label>:</label>
                                <div class="input-group col-sm-4">
                                    <label>{{$process_name}}</label>
                                </div>
                            </div>
                            <div class="card" id="ticket_info" border="1">        
                            </div>                                                     
                            <div class="card" id="para_details">
                                <div class='card-body'>
                                    <div class='table-responsive'>
                                        <table class='table table-bordered' border='0' id='info'>
                                            <thead>
                                                <th style='width:50px;'><b>Sr No.</b></th>
                                                <th colspan="2" style='width:500px;'><b>Parameter Name</b></th>
                                                <th style="text-align: center;"><b>Target Rating</b></th>
                                                <th style='width:10px;text-align: center;'><b>Achieved Rating</b></th>
                                            </thead>
                                            <tbody id='h_lost'>
                                                <?php 
                                                $i=1;
                                                foreach($para_details as $p)
                                                {
                                                    $length=sizeof($p);
                                                    if($p[2]=="")
                                                    {
                                                        $p[2]="0";
                                                    }
                                                    if($p[1]=="")
                                                    {
                                                        $p[1]="0";
                                                    }
                                                    ?>
                                                    <tr style="background-color: #bbbbbb12;">
                                                        <td>{{$i}}</td>
                                                        <td colspan="2"><b>{{$p[0]}}</b></td>
                                                        <td><b>{{$p[1]}}</b></td>
                                                        <?php
                                                        if($p[2]>$p[1])
                                                        {?>
                                                            <td style="color:red;"><b>{{$p[2]}}</b></td>
                                                        <?php }  
                                                        else
                                                        {?>
                                                            <td><b>{{$p[2]}}</b></td>
                                                        <?php }
                                                        ?>
                                                        
                                                    </tr>
                                                    <?php
                                                    for ($x = 3; $x <$length; $x++) 
                                                    {?>
                                                        <tr> 
                                                            <td></td>
                                                            <td style="width:50px;"></td>
                                                            <td>{{$p[$x]}}</td>
                                                            <td style="width:50px;"></td>
                                                            <td style="width:50px;"></td>
                                                        </tr>
                                                    <?php
                                                    } 
                                                    $i++;
                                                }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>  
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript">
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
//    alert(msg);
//    alert(exist);
//    if(exist){
//        swal({ type: "success", title: "Success!", confirmButtonColor: "#292929", text: "Form Submitted Successfully", confirmButtonText: "Ok", showLoaderOnConfirm: true }); 
//    }
    $(document).ready(function () {
        $("#btn_submit").on("click",function()
        {
//            alert("submit click");
            if($("#datepicker-autoclose").val()=="")
            {
                swal("Please select a Date", "", "error");
                return false;
            }
            if($("#ticket_no").val()=="")
            {
                swal("Please select a Ticket No.", "", "error");
                return false;
            }
            if($("#process").val()=="")
            {
                swal("Please select a Process", "", "error");
                return false;
            }
            $('#rating-form').submit();
            
        });
        
    });
</script>

    @endsection