@extends('layouts.app')
@section('title', 'Report')
@section('content')
<?php
//echo "<pre>";print_r($fixture_detail);exit;
$curr_date = date('Y-m-d');
?>
<style>

    /*    .wizard > .content {
        background: #fff;}*/
    span .select2-selection__rendered{
        width: 308.063px;
    }
    table{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    .container1 {
        /*width: 30em;*/
        overflow-x: auto;
        white-space: nowrap;
    }
    table, tr, td,th {
     border: 1px solid #d2cdcd;   
    }
    div.details{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    div.details1{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
.loading-image {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 10;
}
.loader
{
    display: none;
    width:200px;
    height: 200px;
    position: fixed;
    top: 50%;
    left: 50%;
    text-align:center;
    margin-left: -50px;
    margin-top: -100px;
    z-index:2;
}
@media only screen and (max-width:600px){
	.abc{
		font-size : 10px;
	}
        .loader
        {
            display: none;
            width:200px;
            height: 200px;
            position: fixed;
            top: 50%;
            left: 50%;
            text-align:center;
            margin-left: -180px;
            margin-top: -100px;
            z-index:2;
        }
}
</style>
<link href="css/sweetalert.css" rel="stylesheet" />
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Rating Report Form</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">Rating Report Form</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Error!</h4>
        {{ Session::get('error') }}
    </div>
    @endif
    <!-- END BREADCRUMB -->
    <!-- PAGE CONTENT WRAPPER -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" id="rating-report-form" method="post" action="{{ url('rating_report') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-sm-1">Date</label>
                                    <div class="input-group col-sm-4">
                                        <input type="text" class="form-control datepicker-autoclose datepicker_change" id="from_date" name="from_date" placeholder="yyyy-mm-dd" autocomplete="off" value="{{$curr_date}}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="input-group col-sm-1">
                                    </div>
                                    <label class="col-sm-1">Date</label>
                                    <div class="input-group col-sm-4">
                                        <input type="text" class="form-control datepicker-autoclose datepicker_change" id="to_date" name="to_date" placeholder="yyyy-mm-dd" autocomplete="off">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                            </div>
                            <div class="card" id="ticket_info" border="1">        
                            </div> 
                            <div class="form-group row">
                                    <label class="col-sm-1">Process</label>
                                    <div class="col-sm-4">                                                                                            
                                        <select class="select2 form-control custom-select abc" name="process" id="process" onchange="load_tickets(this)">
                                            <option value="">Select</option>
                                            <?php foreach($process_details as $p){?>
                                            <option value="<?php echo $p->id?>">{{$p->process_name}}</option>
                                            <?php }?> 
                                        </select>
                                    </div>
                                    <div class="input-group col-sm-1">
                                    </div>
                                    <label class="col-sm-1">Ticket No.</label>
                                    <div class="col-sm-4">                                                                                            
                                        <select class="select2 form-control custom-select abc" name="ticket_no" id="ticket_no">
                                            <option value="">Select</option>
                                        </select>
                                    </div>
                            </div>
                            
                            <div class="card" id="para_details">
                            </div>  
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary" name="btn_submit" id="btn_submit">Submit</button>
                                </div>
                            </div>
                        </div>
                        <div id="card-body">
                        </div>
                    </form>
                </div>
                <div class="card list_view">
                </div>  
            </div>             
        </div>
    </div>
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript">
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
    if(exist){
        swal({ type: "success", title: "Success!", confirmButtonColor: "#292929", text: "Form Submitted Successfully", confirmButtonText: "Ok", showLoaderOnConfirm: true }); 
    }
    $(document).ready(function () {
        $("#list_view").html("abc");   
        $("#btn_submit").on("click",function()
        {
            var from_date=$("#from_date").val();
            var to_date=$("#to_date").val();
            var process=$("#process").val();
            var ticket_no=$("#ticket_no").val();
            $.ajax({
                url: 'get_parameter_list',
                type: "get",
                data: {from_date:from_date,to_date:to_date,process:process,ticket_no:ticket_no},
                success: function(reportdata) {
//                        console.log(reportdata);
                        var data = JSON.parse(reportdata);
                        if(data=="")
                        {
                        }
                        else
                        {
                            $(".list_view").html(data);   
                        }
                }
                });   
            
        });
        
        $(".datepicker_change").on('changeDate', function (ev) {
            var from_date=$("#from_date").val();
            var to_date=$("#to_date").val();
             $.ajax({
                url: 'get_processes',
                type: "get",
                data: {from_date:from_date,to_date:to_date},
                success: function(reportdata) {
                        console.log(reportdata);
                        var data = JSON.parse(reportdata);
                        if(data=="")
                        {
                            $("#process").empty();
                            $("#ticket_no").empty();
                            $("#ticket_no").append("<option value=''>Select</option>");
                            $("#process").append("<option value=''>Select</option>");
                        }
                        else
                        {
                            $("#process").empty();
                            $("#ticket_no").empty();
                            $("#ticket_no").append("<option value=''>Select</option>");
                            $("#process").append("<option value=''>Select</option>");
                            for(j=0;j<data.length;j++)
                            {
                                $("#process").append("<option value='"+data[j].id+"'>"+data[j].process_name+"</option>");
                            } 
                        }
                }
                });     
        });             
    });
    function load_tickets(pro_val)
    {
        from_date=$("#from_date").val();
        to_date=$("#to_date").val();
        process_id=pro_val.value;
        $.ajax({
                url: 'get_tickets',
                type: "get",
                data: {process_id:process_id,from_date:from_date,to_date:to_date},
                success: function(reportdata) {
                        console.log(reportdata);
                        var data = JSON.parse(reportdata);
                        if(data=="")
                        {
                            $("#ticket_no").empty();
                        }
                        else
                        {
//                            alert("data");
                            $("#ticket_no").empty();
                            $("#ticket_no").append("<option value=''>Select</option>");
                            for(j=0;j<data.length;j++)
                            {
                                $("#ticket_no").append("<option value='"+data[j].ticket_no+"'>"+data[j].ticket_no+"</option>");
                            } 
                        }
                }
                });        
    }
</script>

    @endsection