@extends('layouts.app')
@section('title', 'Report')
@section('content')
<style>
  .input-group-text{
      color: #8f9095;
      /*background-color: #dc3d59;*/
  }
  label {
    font-weight: bold;
}
span .select2-selection__rendered{
        width: 308.063px;
    }
   .input-group col-sm-4{ height: 35px; }
</style>
<div class="page-wrapper">
    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Process Report</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('process_report')}}">Report</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Download Report</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <form  id="orderForm" method="post" action="{{ url('download_process_report') }}">
                {{ csrf_field() }}
                <div class="card-body">  
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Process Name</label>
                        <div class="col-sm-4">                                                                                            
                            <select class="select2 form-control custom-select required" name="process_name" id="process_name" required>
                                <option value="">-- Select --</option>
@foreach($process_detail as $fixture)
                                        <option value="{{$fixture->id}}">{{$fixture->process_name}}</option>
                                        @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info" value="Submit">
                        <a href="{{url('process_report')}}" class="btn btn-danger" >Cancel</a>
                        <!--<button type="button" class="btn btn-default">Save</button>-->                                    
                        <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                    </div>
                </div>
                </form>
                </div>
        </div>
    </div>                    
</div>
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script type='text/javascript' src='js/plugins/jquery-validation/jquery.validate.js'></script>
<script type="text/javascript">
$(document).ready(function () {
$("#fixture_number").on("change",function(){
    var order_id = $(this).val();
    $.ajax({
        url: 'edit_detail_no/' + order_id,
        type: "GET",
        success: function(response) {
            var data = JSON.parse(response);
            console.log(data[0]);
            $("#detail_no").empty();
            $("#detail_no").append('<option value="">-- Select Detail No. --</option>');
            $.each(data, function(key, value) {
                $("#detail_no").append('<option value="' + value.id + '">' + value.detail_no + '</option>');
            });
        }
   });
}); 
});
</script>
@endsection