@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<?php 
//echo "<pre>";print_r($fixture_detail);exit;
?>
<style>
    
/*    .wizard > .content {
    background: #fff;}*/
span .select2-selection__rendered{
        width: 308.063px;
    }
    #cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:9999;
    display:none;
}

@-webkit-keyframes spin {
	from {-webkit-transform:rotate(0deg);}
	to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
	from {transform:rotate(0deg);}
	to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}

.error{
    color: red;
}
 .table td, .table th {
    padding: 0.5rem;
    align: center; 
    border-top: 1px solid #dee2e6;
}
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Upload Designs Of Details</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Design Department</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Upload Designs Of Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('alert-success'))
<div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
    <h4 class="alert-heading">Success!</h4>
    {{ Session::get('alert-success') }}
</div>
@endif
@if (Session::has('error'))
<div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
    <h4 class="alert-heading">Error!</h4>
    {{ Session::get('error') }}
</div>
@endif
<!-- END BREADCRUMB -->
<!-- PAGE CONTENT WRAPPER -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <form class="form-horizontal" id="design-form" action="{{url('detail_no')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="card-body">      
                    <div class="form-group row">
                            <label for="fixture" class="col-sm-2">Fixture Number</label>
                        <div class="col-sm-4">                                                                                            
                            <select class="select2  form-control custom-select" name="order_id" id="order_id" required >
                                <option value="">-- Select Fixture No. --</option>
                                @foreach($fixture_detail as $fixture)
                                    <option value="{{$fixture->order_id}}">{{$fixture->fixture_no}}</option>
                                @endforeach
                            </select>
                        </div>
                        <label id="order_id-error" class="error" class="col-sm-4" for="order_id"></label>
                    </div>
<!--                    <div class="form-group row">
                        <label class="col-md-2"></label>
                        <label style="color:red;">You are only allowed to upload a maximum of 10 files</label>
                    </div>-->
                    <div class="form-group row">
                        <label class="col-md-2">Bulk Upload Design PDF</label>
                        <div class="col-md-4">
                            <input type="file" name="design_file[]" id="design_file"  multiple="multiple" directory="" webkitdirectory="" mozdirectory="" class="form-control" >
                        </div>
                        <div class="col-md-1">
                            <input type="button" id="upload_pdf"  class="btn btn-primary" value="Upload" />
                        </div>
                        <label class="col-md-5" style="color:red;">You are only allowed to upload a maximum of 10 files</label>
                    </div>
                    <span id="file_validate" style="color:red"></span>
                    <span id="max_file_upload" style="color:red"></span>
                    <div id="cover-spin"></div>
                    <input type="hidden" name="up_id" id="up_id" value="1" />
                    <div class="border-top">
                        <div class="card-body">
                            <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info" value="Submit" disabled >
                        <a href="{{url('design_upload')}}" class="btn btn-danger" >Cancel</a>
                        </div>
                    </div>
                </div>    
            </form>
              
            <div class="col-md-12">
                <div class="card" id="img_div">
                        
                </div>
            </div>
              
            </div>
        </div>
    </div>
</div>
</div>
<div class="modal fade " id="Modal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content container1">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Design Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="design-upload" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">
                    <div id="cover-spin"></div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Detail DRG. No</label>
                        <div class="col-sm-4">
                            <input id="detail_no" name="detail_no" type="text" class="required form-control" readonly >
                        </div>
                        <label for="surname" class="col-sm-2">Item Description</label>
                        <div class="col-sm-4">
                            <input id="item_desc" name="item_desc" type="text" class="required form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Item Type</label>
                        <div class="col-sm-4">
                            <input id="item_type" name="item_type" type="text" class="required form-control" readonly>
                        </div>
                        <label for="surname" class="col-sm-2">Material Type</label>
                        <div class="col-sm-4">
                            <input id="material_type" name="material_type" type="text" class="required form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="surname" class="col-sm-2">Finish Size</label>
                        <div class="col-sm-4">
                            <input id="finish_size" name="finish_size" type="text" class="required form-control" readonly>
                        </div>
                        <label for="surname" class="col-sm-2">Qty</label>
                        <div class="col-sm-4">
                            <input id="qty" name="qty" type="text" class="required form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-2">Upload Design</label>
                        <div class="col-md-10">
                            <input type="file" name="upload_file" id="upload_file" class="required form-control" />
                        </div>
                    </div>
                    <div class="form-group row" style="height:400px;">
                        <iframe class="pdf_img" src="" width="100%"></iframe>
                    </div>
                    <input type="hidden" name="id" id="id" value="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary " data-dismiss="modal">Close</button>
                    <button class="btn btn-primary btn_sbt">Submit</button>
                    <!--<button type="button" class="btn btn-primary btn_sbt" >Save changes</button>-->
                </div>
            </form>
        </div>
    </div>
</div>
<script src="assets/libs/jquery/dist/jquery.min.js"></script>
<script src="assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
<script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">

$(document).on("click",".img_model", function () {
        $('#Modal2').modal({backdrop: 'static', keyboard: false});
        var detail_id = $(this).closest('td').find('.imgName').val();//$(this).siblings('.imgName').val();
//    alert(detail_id);
        $.ajax({
            url: 'upload_design_det/' + detail_id,
            type: "GET",
            success: function (response) {
                var data = JSON.parse(response);
                console.log(data);
                $("#detail_no").val(data.detail_no);
                $("#item_desc").val(data.item_desc);
                $("#item_type").val(data.item_type);
                $("#material_type").val(data.material_type);
                $("#finish_size").val(data.finish_size);
                $("#qty").val(data.qty);
                $("#id").val(data.id);
                var url = "Fixture_Design/" + data.fixture_no + "/" + data.upload_file;
                $('.pdf_img').attr('src', url);
//            $('#myModal1').modal('hide');
            }
        });
    });
  
$(document).ready(function () {   
   $("form#design-upload").submit(function (e) {
        e.preventDefault();
        var formData = new FormData(this);
        $("#cover-spin").show();
        $.ajax({
            url: 'upload_design_det',
            type: 'POST',
            data: formData,
            success: function (data) {
                $("#cover-spin").hide();
                fileInput = document.getElementById('upload_file');
                fileInput.value = '';
                $('#Modal2').modal('hide');
                swal({type: "success", title: "Good Job!", confirmButtonColor: "#292929", text: "Form Submitted Successfully", confirmButtonText: "Ok"});
                
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }); 
    
 
    
$("#order_id").on("change",function(){
    var order_id = $(this).val();
    $.ajax({
        url: 'detail_no/' + order_id,
        type: "GET",
        success: function(response) {
            var data = JSON.parse(response);
            console.log(data[0]);
            $("#detail_no").empty();
            $("#detail_no").append('<option value="">-- Select Detail No. --</option>');
            $.each(data, function(key, value) {
                $("#detail_no").append('<option value="' + value.id + '">' + value.detail_no + '</option>');
            });
        }
   });
}); 


$("#btnsubmit").click(function(){
    $("#design-form").validate({
        rules: {  
                order_id: {required: true},
            }                                        
        });
})

//$('#btnsubmit').on('click', function() {
//    $("#design-form").valid();
//});

$('#upload_pdf').on('click', function() {
    var order_id = $("#order_id").val();
    if(order_id == ""){
        $("#order_id-error").html('This field is required.');
        return false;
    }else{
        $("#order_id-error").html('');
    }
    var fileInput = document.getElementById('design_file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.pdf)$/i;
    if(!allowedExtensions.exec(filePath)){
        $("#file_validate").html('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
    $("#file_validate").html('');    
//    var file_data = $('#design_file').prop('files')[0];   
    var form_data = new FormData();  
    var ins = document.getElementById('design_file').files.length;
    for (var x = 0; x < ins; x++) {
        form_data.append("design_file[]", document.getElementById('design_file').files[x]);
    }
    var $fileUpload = $("input[type='file']");
    if (parseInt($fileUpload.get(0).files.length) > 35){
           $("#max_file_upload").html("You are only allowed to upload a maximum of 10 files");
    }else{
        form_data.append('order_id', $("#order_id").val());
        form_data.append('up_id', 0);
//        $("#cover-spin").show();                         
        $.ajax({
            url: 'detail_no', // point to server-side PHP script 
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,    
            type: 'post',
            beforeSend:function(){
            $("#cover-spin").show();
           },
           complete:function(data){
            $("#cover-spin").hide();
           },
            success: function(response){
                $("#btnsubmit").attr("disabled",false);
                var $el = $('#design_file');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
                
                $("#img_div").html(response); // display response from the PHP script, if any
            }
        });
    }
    }
});

});
    


function fileValidation(){
    var fileInput = document.getElementById('design_file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif|\.pdf)$/i;
    if(!allowedExtensions.exec(filePath)){
        $("#file_validate").html('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        $("#file_validate").html('');
    }
}

</script>
        
@endsection