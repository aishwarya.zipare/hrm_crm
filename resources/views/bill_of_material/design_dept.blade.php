@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
<?php
//echo "<pre>";print_r($fixture_detail);exit;
?>
<style>

    /*    .wizard > .content {
        background: #fff;}*/
    span .select2-selection__rendered{
        width: 308.063px;
    }
</style>
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Design Department</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">BOM</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Design Department</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Error!</h4>
        {{ Session::get('error') }}
    </div>
    @endif
    <!-- END BREADCRUMB -->
    <!-- PAGE CONTENT WRAPPER -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" id="design-form" method="post" action="{{ url('design_dept') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-sm-2">Date</label>
                                    <div class="input-group col-sm-4">
                                        <input type="text" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                    <label for="fname" class="col-sm-2 text-right control-label col-form-label">Upload Details</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="sample_file" id="sample_file" onchange="return fileValidation()" class="required form-control" />
                                    </div>
                                    <span id="file_validate" style="color:red"></span>
                            </div>
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function () {
        $("#order_id").on("change", function () {
            var order_id = $(this).val();
            $.ajax({
                url: 'fixure_det/' + order_id,
                type: "GET",
                success: function (response) {
                    var data = JSON.parse(response);
                    console.log(data[0]);
                    $("#fixture_no").val(data[0].fixture_number);
                    $("#fixture_qty").val(data[0].quantity_no);
                    $("#customer_part_desc").val(data[0].project_desc);
                    $("#priority").val(data[0].priority).attr("selected", "selected");
                    $("#priority").trigger('change.select2');
                }
            });
        });

        //jQuery.validator.setDefaults({
        //  debug: true,
        //  success: "valid"
        //});
        //$( "#design-form" ).validate({
        //  rules: {
        //    design_file: {
        //      required: true,
        //      extension: "xls|csv"
        //    }
        //  }
        //});

    });

    function fileValidation() {
        var fileInput = document.getElementById('sample_file');
        var filePath = fileInput.value;
        var allowedExtensions = /(\.xls|\.xlsx|\.csv)$/i;
        if (!allowedExtensions.exec(filePath)) {
            $("#file_validate").html('Please upload file having extensions .xls/.xlsx/.csv only.');
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            $("#file_validate").html('');
        }
    }
</script>

    @endsection