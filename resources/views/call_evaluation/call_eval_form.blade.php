@extends('layouts.app')
@section('title', 'Call Evaluation Form')
@section('content')
<?php
//echo "<pre>";print_r($call_details);exit;
$curr_date = date('Y-m-d');
?>
<style>
span .select2-selection__rendered{
        width: 308.063px;
    }
    /*    .wizard > .content {
        background: #fff;}*/
    span .select2-selection__rendered{
        width: 308.063px;
    }
    table{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    .container1 {
        /*width: 30em;*/
        overflow-x: auto;
        white-space: nowrap;
    }
    table, tr, td,th {
     border: 1px solid #d2cdcd;   
    }
    div.details{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    div.details1{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
.loading-image {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 10;
}
.loader
{
    display: none;
    width:200px;
    height: 200px;
    position: fixed;
    top: 50%;
    left: 50%;
    text-align:center;
    margin-left: -50px;
    margin-top: -100px;
    z-index:2;
}
@media only screen and (max-width:600px){
	.abc{
		font-size : 10px;
	}
        .loader
        {
            display: none;
            width:200px;
            height: 200px;
            position: fixed;
            top: 50%;
            left: 50%;
            text-align:center;
            margin-left: -180px;
            margin-top: -100px;
            z-index:2;
        }
}
</style>
<link href="css/sweetalert.css" rel="stylesheet" />
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Call Evaluation Form</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <!--<li class="breadcrumb-item"><a href="#">BOM</a></li>-->
                            <li class="breadcrumb-item active" aria-current="page">Call Evaluation Form</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Error!</h4>
        {{ Session::get('error') }}
    </div>
    @endif
    <!-- END BREADCRUMB -->
    <!-- PAGE CONTENT WRAPPER -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" id="calleval-form" method="post" action="{{ url('save_calleval_form') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-sm-2">Phone Number</label>
                                    <div class="col-sm-4">                                                                                            
                                        <select class="select2 form-control custom-select abc" name="phone_number" id="phone_number" onchange="view_ticket_info(this)">
                                            <option value="">Select</option>
                                            <?php foreach($call_details as $t){?>
                                            <option value="<?php echo $t->id?>">{{$t->phone_number}}</option>
                                            <?php }?> 
                                        </select>
                                    </div>
                                    <label class="col-sm-1"></label>
                                    <label class="col-sm-1">Date</label>
                                    <div class="input-group col-sm-4">
                                        <input type="text" class="form-control datepicker-autoclose" id="datepicker-autoclose" name="date" placeholder="yyyy-mm-dd" autocomplete="off" value="{{$curr_date}}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                            </div>
                            <div class="card" id="ticket_info" border="1">        
                            </div> 
                            <div class="form-group row">
                                    <label class="col-sm-2">Process</label>
                                    <div class="col-sm-4">                                                                                            
                                        <select class="select2 form-control custom-select abc" name="process_id" id="process_id" onchange="view_parameter(this)">
                                            <option value="">Select</option>
                                            <?php foreach($process_details as $p){?>
                                            <option value="<?php echo $p->id?>">{{$p->process_name}}</option>
                                            <?php }?> 
                                        </select>
                                    </div>
                            </div>
                            
                            <div class="card" id="parameter_details">
                            </div>  
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary" name="btn_submit" id="btn_submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript">
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
//    alert(msg);
//    alert(exist);
    if(exist){
        swal({ type: "success", title: "Success!", confirmButtonColor: "#292929", text: "Form Submitted Successfully", confirmButtonText: "Ok", showLoaderOnConfirm: true }); 
    }
    $(document).ready(function () {
        $("#btn_submit").on("click",function()
        {
//            alert("submit click");
            if($("#datepicker-autoclose").val()=="")
            {
                swal("Please select a Date", "", "error");
                return false;
            }
            if($("#phone_number").val()=="")
            {
                swal("Please select a Phone No.", "", "error");
                return false;
            }
            if($("#process").val()=="")
            {
                swal("Please select a Process", "", "error");
                return false;
            }
            $('#calleval-form').submit();
            
        });
        
    });
    function view_ticket_info(t_no)
    {
        $call_details_id=t_no.value;
        $.ajax({
                url: 'get_call_info',
                type: "get",
                data: {$call_details_id:$call_details_id},
                success: function(reportdata) {
                        console.log(reportdata);
                        var data = JSON.parse(reportdata);
                        if(data=="")
                        {
                            swal("No Data", "", "error");
                        }
                        else
                        {
//                            alert("data");
                            $("#ticket_info").html(data);   
                        }
                }
                });        
    }
    function view_parameter(pro_val)
    {
//        alert(pro_val.value);
        process_id=pro_val.value;
        $.ajax({
                url: 'get_calleval_parameter',
                type: "get",
                data: {process_id:process_id},
                success: function(reportdata) {
//                        console.log(reportdata);
                        var data = JSON.parse(reportdata);
                        if(data=="")
                        {
//                            alert("no data");
                        }
                        else
                        {
//                            alert("data");
                            $("#parameter_details").html(data);   
                        }
                }
                });        
    }
    function check_rating(i)
    {
//        alert(i);0
        t_rating= $("#parameter_details_"+i+"_1").val();
        a_rating= $("#parameter_details_"+i+"_2").val();
        if(parseInt(a_rating)>parseInt(t_rating))
        {
            $("#parameter_details_"+i+"_2").css('color','red');
        }
        else
        {
            $("#parameter_details_"+i+"_2").css('color','');
        }
    }
    function check_sub_rating(i,j)
    {
//        alert(i);
//        alert("j="+j);
        t_rating= $("#parameter_details_"+i+"_"+j+"1").val();
        a_rating= $("#parameter_details_"+i+"_"+j+"2").val();
        if(parseInt(a_rating)>parseInt(t_rating))
        {
            $("#parameter_details_"+i+"_"+j+"2").css('color','red');
        }
        else
        {
            $("#parameter_details_"+i+"_"+j+"2").css('color','');
        }
    }
</script>

    @endsection