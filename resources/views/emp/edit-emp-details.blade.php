@extends('layouts.app')
@section('title','Add Employee')
@section('content')
<?php
$curr_date = date('Y-m-d');
?>
<style>
    .error{
        color:red;
    }
/*.select2-container--classic .select2-selection--single, .select2-container--default .select2-selection--multiple, .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow, .select2-container--default .select2-selection--single .select2-selection__rendered {
    border-color: #e9ecef;
    height: auto;
    color: #3e5569;
    line-height: 20px;
}*/
    
.select2-container--default .select2-selection--multiple{
    border-color: #e9ecef;
    height: auto;
    color: #3e5569;
    line-height: 20px;
}

</style>
<div class="page-wrapper">

    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add New Employee</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{url('emp-details')}}">User</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Add New Employee</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" id="orderForm" method="post" action="{{ url('emp_update') }}">
                        {{ csrf_field() }}
                        <div class="card-body">  
                             <h4>Employee Details :</h4>
                            <input type="hidden" name="id" id="id" value="{{$emp->id}}">
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Emp ID</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="emp_id" id="emp_id" value="{{$emp->emp_id}}" required />
                                </div>
                                <label class="col-sm-2 text-right control-label col-form-label">Name</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="emp_name" id="emp_name" value="{{$emp->emp_name}}" required />
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Mobile No.</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="emp_mobile" id="emp_mobile" value="{{$emp->emp_mobile}}" required />
                                </div>
                                <label for="fname" class="col-sm-2 text-right control-label col-form-label">Email</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="email" id="email" value="{{$emp->email}}" required />
                                    <code id="email_validate"></code>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Designation</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="designation" id="designation" value="{{$emp->designation}}"/>
                                </div>
                                <label for="fname" class="col-sm-2 text-right control-label col-form-label">Employment type</label>
                                <div class="col-sm-4">
                                    @if($emp->gender=="confirmed")
                                        <input type="radio" name="emp_type" value="confirmed" checked>Confirmed
                                        <input type="radio" name="emp_type" value="not_confirmed">Not Confirmed<br>
                                    @else
                                        <input type="radio" name="emp_type" value="confirmed">Confirmed
                                        <input type="radio" name="emp_type" value="not_confirmed" checked>Not Confirmed<br>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">DOB</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control datepicker-autoclose" id="datepicker-autoclose" name="dob" placeholder="yyyy-mm-dd" autocomplete="off" value="{{$emp->dob}}">
<!--                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>-->
                                </div>
                                <label class="col-sm-2 text-right control-label col-form-label">Gender</label>
                                <div class="col-sm-4">
                                    @if($emp->gender=="male")
                                        <input type="radio" name="gender" value="male" checked> Male
                                        <input type="radio" name="gender" value="female"> Female<br>
                                    @else
                                        <input type="radio" name="gender" value="male"> Male
                                        <input type="radio" name="gender" value="female" checked> Female<br>
                                    @endif
                                </div>
                            </div>
                             <h4>Salary Details :</h4>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Account No:</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="bank_acc_no" id="bank_acc_no" placeholder="Rs." value="{{$emp->bank_acc_no}}"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Basic</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="basic_sal" id="basic_sal" placeholder="Rs." value="{{$emp->basic_sal}}"/>
                                </div>
                                <label for="fname" class="col-sm-2 text-right control-label col-form-label">HRA</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="hra" id="hra" placeholder="Rs." value="{{$emp->hra}}"/>
                                    <code id="email_validate"></code>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Conveyance</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="conveyance" id="conveyance" placeholder="Rs." value="{{$emp->conveyance}}"/>
                                </div>
                                <label for="fname" class="col-sm-2 text-right control-label col-form-label">Gross</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="gross_sal" id="gross_sal" placeholder="Rs." value="{{$emp->gross_sal}}"/>
                                    <code id="email_validate"></code>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">PF No.</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="pf_no" id="pf_no" value="{{$emp->pf_no}}"/>
                                </div>
                                <label for="fname" class="col-sm-2 text-right control-label col-form-label">PF</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="esi" id="esi" placeholder="Rs." value="{{$emp->pf}}"/>
                                    <code id="email_validate"></code>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">ESIA No.</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="esi_no" id="esi_no" value="{{$emp->esi_no}}"/>
                                </div>
                                <label for="fname" class="col-sm-2 text-right control-label col-form-label">ESIA</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="esi" id="esi" placeholder="Rs." value="{{$emp->esi}}"/>
                                    <code id="email_validate"></code>
                                </div>
                            </div>
                             <div class="form-group row">
                                <label class="col-sm-2 text-right control-label col-form-label">Incentive</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="incentive" id="incentive" placeholder="Rs." value="{{$emp->incentive}}"/>
                                </div>
                                <label class="col-sm-2 text-right control-label col-form-label">Other</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" name="other" id="other" placeholder="Rs." value="{{$emp->other}}"/>
                                </div>
                            </div>
                        <div class="border-top">
                            <div class="card-body">
                                <input type="submit" name="btnsubmit" id="btnsubmit" class="btn btn-info" value="Submit">
                                <a href="{{url('emp-details')}}" class="btn btn-danger" >Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>                    
    </div>
</div>

<script src="assets/libs/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src='js/plugins/jquery-validation/jquery.validate.js'></script>
<script type="text/javascript">
//            $("#btnsubmit").on("click",function(){
$("document").ready(function () {
    $("#role").on("change", function () {
        var role_val = $(this).val();
//    alert(role_val);
        if (role_val == "3") {
            $(".p_val").css("display", "block");
        } else {
            $(".p_val").css("display", "none");
        }
    })
        //    var max_fields      = 6; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var i = 2;
    var x = 1; //initlal text box count
    var s = 14;
    var l=1;
       $(document).on('click','.add_field_button',function(){
        var v = $(this).parents("td").prevAll(".parameter").eq(1).val();
        i++; 
        l++;
        $("#h_lost").append('<tr class="input_fields_wrap delparam_'+l+'">\n\
            <td style="width:2%;"><i class="fas fa-plus-circle add_field_button" style="color: #0c8a54;"></i><i class="fas fa-minus-circle remove_field" style="color: red;"></i></td>\n\
            <td><input type="text" name="agent_detail['+i+'][]" class="form-control parameter checkblank"   style="width:100px;" placeholder="Agent ID"/></td>\n\
            <td><input type="text" name="agent_detail['+i+'][]" class="form-control parameter checkblank"  placeholder="Agent Name"/></td>\n\
            <td><input type="text" name="agent_detail['+i+'][]" class="form-control parameter checkblank"  placeholder="Agent Email"/></td>\n\
        </tr>'); 
            $('select').select2();
            $('.datepicker-autoclose').datepicker();
        $("#h_lost").on('click','.remove_field',function(){
             $(this).parent().parent().remove();
            
        });
    });
})

$("#email").focusout(function () {
    var email = $(this).val();
    $.ajax({
        url: 'email-validate/' + email,
        type: "GET",
        success: function (data) {
            console.log(data);
            $("#email_validate").html(data);
            if (data != "") {
                $("#email").val("");
            }
        }
    });
});



var jvalidate = $("#orderForm").validate({
    rules: {
        name: {required: true},
        email: {required: true},
        password: {required: true},
        dept_id: {required: true},
        role: {required: true},
    }
});

$('#btnsubmit').on('click', function () {
    $("#orderForm").valid();
});

$("#dept_id").on("change", function () {
    var dept = $(this).val();
    //            var date = $("#date").val();
    $.ajax({
        url: 'sub-dept/' + dept,
        type: "get",
        //data: {batch:batch,date:date},
        success: function (data) {
//                           var obj = JSON.parse(data);
            $("#sub_dept").html(data);
        }
    });
})
</script>

@endsection