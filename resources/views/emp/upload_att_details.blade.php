@extends('layouts.app')
@section('title', 'Upload Attendance Form')
@section('content')
<?php
$curr_date = date('Y-m-d');
?>
<style>
span .select2-selection__rendered{
        width: 308.063px;
    }
    span .select2-selection__rendered{
        width: 308.063px;
    }
    table{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    .container1 {
        /*width: 30em;*/
        overflow-x: auto;
        white-space: nowrap;
    }
    table, tr, td,th {
     border: 1px solid #d2cdcd;   
    }
    div.details{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    div.details1{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
.loading-image {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 10;
}
.loader
{
    display: none;
    width:200px;
    height: 200px;
    position: fixed;
    top: 50%;
    left: 50%;
    text-align:center;
    margin-left: -50px;
    margin-top: -100px;
    z-index:2;
}
@media only screen and (max-width:600px){
	.abc{
		font-size : 10px;
	}
        .loader
        {
            display: none;
            width:200px;
            height: 200px;
            position: fixed;
            top: 50%;
            left: 50%;
            text-align:center;
            margin-left: -180px;
            margin-top: -100px;
            z-index:2;
        }
}
</style>
<link href="css/sweetalert.css" rel="stylesheet" />
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Upload Attendance Details</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <!--<li class="breadcrumb-item"><a href="#">BOM</a></li>-->
                            <li class="breadcrumb-item active" aria-current="page">Upload Attendance Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Error!</h4>
        {{ Session::get('error') }}
    </div>
    @endif
    <!-- END BREADCRUMB -->
    <!-- PAGE CONTENT WRAPPER -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" id="upload-call-form" method="post" action="{{ url('upload-att-details') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-sm-1">Date</label>
                                    <div class="input-group col-sm-4">
                                        <input type="text" class="form-control datepicker-autoclose" id="datepicker-autoclose" name="date" placeholder="yyyy-mm-dd" autocomplete="off" value="{{$curr_date}}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                     <label class="col-md-1"></label>
<!--                                     <label class="col-md-2">File Upload</label>
                                    <div class="input-group col-sm-4">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="call_file" name="call_file">
                                            <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                            <div class="invalid-feedback">Example invalid custom file feedback</div>
                                        </div>
                                    </div>-->
                                    <label class="col-sm-1">File</label>
                                    <div class="input-group col-sm-4">
                                        <input type="file" name="call_file" id="call_file">
                                    </div>
                                    
                            </div>
                            <div class="card" id="para_details">
                            </div>  
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="button" class="btn btn-primary" name="btn_submit" id="btn_submit">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript">
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
//    alert(msg);
//    alert(exist);
    if(exist){
        swal({ type: "success", title: "Success!", confirmButtonColor: "#292929", text: "Form Submitted Successfully", confirmButtonText: "Ok", showLoaderOnConfirm: true }); 
    }
    $(document).ready(function () {
        $("#btn_submit").on("click",function()
        {
//            alert("submit click");
            if($("#datepicker-autoclose").val()=="")
            {
                swal("Please select a Date", "", "error");
                return false;
            }
            var file = $('#call_file')[0].files[0]
            var f=$('#call_file').val();
            if(f=="")
            {
               swal("Please select a file","","warning");r
               return false;   
            }
            $('#upload-call-form').submit();
            
        });
        
    });
</script>
@endsection