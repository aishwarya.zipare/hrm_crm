@extends('layouts.app')
@section('title', 'Employee-List')
@section('content')
<div class="page-wrapper">
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <!--<h4 class="page-title">User</h4>-->
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('home')}}">Employee</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Employee Details</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    <!-- START WIDGETS -->                   
    <div class="row">
        <div class="col-md-12">
                <?php $x = 1; ?>
                            <div class="card"><div class="card-body">
                                <h5 class="card-title">EMPLOYEE LIST <a href="{{url('add_emp')}}" class="panel-title" style="margin-left: 70%;color: #dc3d59;"><span class="fa fa-plus-square"></span>Add New Employee</a></h5>
                        <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th style="font-weight: bold;width:50px;">Emp ID</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile No.</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                             @foreach($emp_data as $emp)
                                <tr>
                                    <td>{{$emp->emp_id}}</td>
                                    <td>{{$emp->emp_name}}</td>
                                    <td>{{$emp->email}}</td>
                                    <td>{{$emp->emp_mobile}}</td>
                                    <td>
                                        <a href="{{ url('emp-edit?id='.$emp->id)}}"><span class="fa fa-edit"></span></a>
                                        <a href="{{ url('emp-delete')}}/{{$emp->id}}" style="color:red" class="delete"><span class="fa fa-trash"></span></a>
                                    </td>

                                </tr>
                             @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->   
<script type="text/javascript" src="js/plugins/jquery/jquery.min.js"></script>
<script>
$(document).ready(function(){
//    alert();
    $(".delete").on("click",function(){
        return confirm('Are you sure to delete user');
    });
});
</script>
@endsection
