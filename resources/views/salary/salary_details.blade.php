@extends('layouts.app')
@section('title', 'Salary Details')
@section('content')
<?php
$curr_date = date('Y-m-d');
?>
<style>
span .select2-selection__rendered{
        width: 308.063px;
    }
    span .select2-selection__rendered{
        width: 308.063px;
    }
    table{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    .container1 {
        /*width: 30em;*/
        overflow-x: auto;
        white-space: nowrap;
    }
    table, tr, td,th {
     border: 1px solid #d2cdcd;   
    }
    div.details{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
    div.details1{
        overflow: scroll; /* Scrollbar are always visible */
        overflow: auto;
    }
.loading-image {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 10;
}
.loader
{
    display: none;
    width:200px;
    height: 200px;
    position: fixed;
    top: 50%;
    left: 50%;
    text-align:center;
    margin-left: -50px;
    margin-top: -100px;
    z-index:2;
}
@media only screen and (max-width:600px){
	.abc{
		font-size : 10px;
	}
        .loader
        {
            display: none;
            width:200px;
            height: 200px;
            position: fixed;
            top: 50%;
            left: 50%;
            text-align:center;
            margin-left: -180px;
            margin-top: -100px;
            z-index:2;
        }
}
</style>
<link href="css/sweetalert.css" rel="stylesheet" />
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Salary Details</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <!--<li class="breadcrumb-item"><a href="#">BOM</a></li>-->
                            <li class="breadcrumb-item active" aria-current="page">Salary Details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('alert-success'))
    <div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Success!</h4>
        {{ Session::get('alert-success') }}
    </div>
    @endif
    @if (Session::has('error'))
    <div class="alert alert-danger alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading">Error!</h4>
        {{ Session::get('error') }}
    </div>
    @endif
    <!-- END BREADCRUMB -->
    <!-- PAGE CONTENT WRAPPER -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <form class="form-horizontal" action="{{ url('emp_view_slip') }}" id="upload-call-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="card-body">
                            <div class="form-group row">
                                    <label class="col-sm-1">Date</label>
                                    <div class="input-group col-sm-4">
                                        <input type="text" name="month" class="form-control datepicker" id="month" data-date-format="mm/yyyy" placeholder="mm/yyyy" tabindex=1 style="width: 173px;" autocomplete="off" required>
                                        <!--<input type="text" class="form-control datepicker-autoclose monthPicker" id="datepicker-autoclose" name="date" placeholder="yyyy-mm-dd" autocomplete="off" value="{{$curr_date}}">-->
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </div>
                                     <label class="col-md-1"></label> 
                            </div>
                            <div class="card" id="para_details">
                            </div>  
                            <div class="border-top">
                                <div class="card-body">
                                    <button type="button" id="view_slip" name="view_slip" class="btn btn-primary btn-large">Generate</button>
                                    <!--<button type="button" class="btn btn-primary" name="btn_submit" id="btn_submit">Submit</button>-->
                                </div>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="col-md-12">
                                <div class="card">

           <div class="span2"></div>
           <div id="report_view" class="span9" align="center"></div>  </div> 
           </div>
        </div>  
    </div>
    <script src="assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="js/sweetalert.min.js"></script>
    <script src="assets/libs/jquery-steps/build/jquery.steps.min.js"></script>
    <script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript">
    var msg = '{{Session::get('alert')}}';
    var exist = '{{Session::has('alert')}}';
//    alert(msg);
//    alert(exist);
    if(exist){
        swal({ type: "success", title: "Success!", confirmButtonColor: "#292929", text: "Form Submitted Successfully", confirmButtonText: "Ok", showLoaderOnConfirm: true }); 
    }
    $(document).ready(function () {
        $('.datepicker').datepicker({
           format: "mm/yyyy",
           viewMode: "months", 
           minViewMode: "months",
           autoclose: true,
          }).on('changeDate', function (ev) {
               $(this).datepicker('hide');
});

        
        $("#view_slip").click(function () 
        {
//            alert("view");
            var month=$("#month").val();
            console.log(month);
            $.ajax({
             url: 'emp_view_slip',
             type: 'GET',
             data:{month:month},
             success: function (response)
             {
                 console.log(response);
                 var data=JSON.parse(response);
                 if(data=="")
                 {
                    window.location.href ='sal-details'; 
                 }
                 else
                 {
                 $("#report_view").html(data);
                 } 
             }
             });  
     });
        
    });
</script>
@endsection