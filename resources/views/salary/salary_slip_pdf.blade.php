<?php
//$timestemp =$emp_record->month;
//$year = Carbon\Carbon::createFromFormat('Y-m-d',$timestemp)->year;
//$month = Carbon\Carbon::createFromFormat('Y-m-d',$timestemp)->month;
//$monthName = date("F", mktime(0, 0, 0, $month, 10));
//echo $monthName;
$net_pay=$emp_record->gross_sal-$emp_record->total_deduc;
?>
<div style="border:1px solid black;"> 
<table style="width:100%" align="center">
    <tr>
        <td><center><img src="{{asset('employee_images/logo.png')}}" width="80" height="80"/></center></td>
        <td><h2><center>iPing Data Labs</center></h2><h5><center>A Division of Suchetan Commercial & Marketing Pvt. Ltd.</center></h5></td>
    </tr>
</table>
<hr>
<table style="width:100%" align="center">
    <tr>
        <td>Employee Name</td>
        <td>:</td>
        <td>{{$emp_record->emp_name}}</td>
        <td>Month and Year</td>
        <td>:</td>
        <td>{{$emp_record->month}} {{$emp_record->year}}</td>
    </tr>
    <tr>
        <td>Bank Acc no.</td>
        <td>:</td>
        <td>{{$emp_record->bank_acc_no}}</td>
         <td>PF no.</td>
        <td>:</td>
        <td>{{$emp_record->pf_no}}</td>
    </tr>
    <tr>
        <td>Dated</td>
        <td>:</td>
        <td>{{date("Y-m-d")}}</td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>
<table style="width:100%;border-collapse: collapse; border: 1px solid black;" align="center">
    <tr style="border: 1px solid black;">
        <th style="border: 1px solid black;" bgcolor="#DCDCDC">Earnings</th>
        <th style="border: 1px solid black;" bgcolor="#DCDCDC">Amount</th>
        <th style="border: 1px solid black;" bgcolor="#DCDCDC">Deduction</th>
        <th style="border: 1px solid black;" bgcolor="#DCDCDC">Amount</th>
    </tr>
    <tr>
        <td style="border-right:1px solid black;">Basic</td>
        <td style='text-align:right;'>{{$emp_record->basic_sal}}</td>
        <td style="border-right:1px solid black;border-left:1px solid black;">PF</td>
        <td style='text-align:right;'>{{$emp_record->pf}}</td>
    </tr>
    <tr>
        <td style="border-right:1px solid black;">HRA</td>
        <td style='text-align:right;'>{{$emp_record->hra}}</td>
        <td style="border-right:1px solid black;border-left:1px solid black;">ESI</td>
        <td style='text-align:right;'>{{$emp_record->esi}}</td>
    </tr>  
    <tr>
        <td style="border-right:1px solid black;">Conveyance</td>
        <td style='text-align:right;'>{{$emp_record->conveyance}}</td>
        <td style="border-right:1px solid black;border-left:1px solid black;">PT</td>
        <td style='text-align:right;'></td>
    </tr>
    <tr>
        <td style="border-right:1px solid black;">Gross</td>
        <td style='text-align:right;'>{{$emp_record->gross_sal}}</td>
        <td style="border-right:1px solid black;border-left:1px solid black;">Other</td>
        <td style='text-align:right;'>{{$emp_record->other}}</td>
    </tr>
    <tr>
        <td style="border-right:1px solid black;">Advance</td>
        <td style='text-align:right;'></td>
        <td style="border-right:1px solid black;border-left:1px solid black;">Total Deduction</td>
        <td style='text-align:right;'>{{$emp_record->total_deduc}}</td>
    </tr>
    <tr>
        <td style="border-right:1px solid black;">Incentive</td>
        <td style='text-align:right;'>{{$emp_record->incentive}}</td>
        <td style="border-right:1px solid black;border-left:1px solid black;"> </td>
        <td style='text-align:right;'></td>
    </tr>
    <tr style="border: 1px solid black;">
        <td style="border: 1px solid black;"></td><td style="border: 1px solid black;"></td>
        <td style="border: 1px solid black;">Net Pay</td>
        <td style="border: 1px solid black;text-align:right;">{{$net_pay}}</td>
    </tr>
</table>
<br><br><br>
<div align="right">Authorised Signature&nbsp;&nbsp;&nbsp;</div>
</div>
<?php // exit;?>