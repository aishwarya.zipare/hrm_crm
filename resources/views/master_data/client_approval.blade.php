@extends('layouts.app')
@section('title', 'Process Approval')
@section('content')
<div class="page-wrapper">
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Process Approval</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Process Approval</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form class="form-horizontal" id="form" method="post" action="{{ url('save_client_approval') }}" enctype="multipart/form-data">
                <div class="card">
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-2">Date : </label>
                            <div class="input-group col-sm-4">
                                {{$process_detail->process_date}}
                            </div>
                            <label class="col-sm-2">Process Version : </label>
                            <div class="input-group col-sm-4">
                                {{$process_detail->version}}
                            </div>
                        </div>   
                        <div class="form-group row">
                            <label class="col-sm-2">Process Name : </label>
                            <div class="input-group col-sm-4">
                                {{$process_detail->process_name}}
                            </div>
                            <label class="col-sm-2">Area : </label>
                            <div class="input-group col-sm-4">
                                {{$process_detail->area}}
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label class="col-sm-2">Team Leader : </label>
                            <?php $user_data = \App\User::select('*')->where(['id'=>$process_detail->team_leader])->first(); ?>
                            <div class="input-group col-sm-4">
                                {{@$user_data->name}}
                            </div>
                            <label class="col-sm-2">Quality Auditor  : </label>
                            <div class="input-group col-sm-4">
                                {{$process_detail->quality_auditor}}
                            </div>
                        </div> 
                                  <div class="form-group row">
                            <label class="col-sm-2">Quality Manager : </label>
                            <div class="input-group col-sm-4">
                                {{$process_detail->quality_manager}}
                            </div>
                            <label class="col-sm-2">Client  : </label>
                            <div class="input-group col-sm-4">
                                {{$process_detail->clients}}
                            </div>
                        </div> 
                        <br>
                        <div class="row">
                             <div class="table-responsive">
                                    <table class="table table-striped table-bordered" border="0">
                                        <thead>
                                            <tr>
                                                <th style="width:5px;"><b>Sr No.</b></th>
                                                <th><b>Parameter Name</b></th>
                                                <th><b>Sub Parameter Name</b></th>
                                                <th><b>Rating</b></th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="h_lost">
                                            <tr class="input_fields_wrap">
                                                <?php $i=1;foreach($parameter_detail as $data){ ?>
                                                <td style="width:0.5%;">{{$i}}</td>
                                                <td class="parameter" colspan="2">{{$data->parameter_name}}</td>
                                                <!--<td class="parameter"><input type="text" name="parameter_detail[1][]" class="form-control parameter checkblank" value=" " style="width: 500px;height:70px;" /></td>-->
                                 
                                                <td style="width:5%;"></td>
                                                <?php 
                                                $sub_parameter_detail = \App\SubParameter::select('*')->where('parameter_id','=',$data->id)->get();
                                                foreach($sub_parameter_detail as $sub)
                                                {
                                                ?>
                                                           <tr class="subprocess_row">
                                                <td style="width:2%;"></td>
                                                <td></td>
                                                <!--<td></td>-->
                                                <td>{{$sub->subparameter_name}}</td>
                                                <td>{{$sub->rating}}</td>
                                            </tr>
                                                <?php } ?>
                                            </tr>
                                             <?php $i++;}?>
                                
                                        </tbody>
                                    </table>
                                </div>
                        </div>
   
                        <div class="form-group row">
                                <label for="name" class="col-sm-2">Remarks</label>
                                <div class="col-sm-8">
                                     <?php if($process_detail->approval_flag == 1) {
                                    $approve_data = \App\ClientApproval::select('*')->where('process_id','=',$process_detail->id)->first();
                                     } ?>
                                    <textarea name="remark" id="remark" class="required form-control" rows="5">{{@$approve_data->remark}}</textarea>
                                </div>
                        </div>
                                             <div class="form-group row">
                                                 <label for="name" class="col-sm-2">&nbsp;&nbsp;</label>
                            <div class="input-group col-sm-4">
                                
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input ppc" name="status" value="1" id="status" <?php if($process_detail->approval_flag==1) echo "checked";?>>
                                    <label class="custom-control-label" for="status">Approve</label>
                                </div>
                            </div>
                                                 <div class="input-group col-sm-4">

                                                     <div class="custom-control custom-checkbox mr-sm-2">
                                                         <input type="checkbox" class="custom-control-input ppc" name="changes_flag" value="1" id="changes_flag" <?php if ($process_detail->changes_flag == 1) echo "checked"; ?>>
                                                         <label class="custom-control-label" for="changes_flag">Need Changes</label>
                                                     </div>
                                                 </div>
                        </div>
                        <input type="hidden" name="process_id" value="{{$process_detail->id}}" />
                                 <div class="border-top">
                                <div class="card-body">
                                    <?php if($process_detail->approval_flag == 0) { ?>
                                    <button type="submit" id="btnsave" class="btn btn-success pull-left">Save</button>
                                    <?php } ?>
                                    <a href="{{url('home')}}" class="btn btn-danger pull-right" >Cancel</a>
                                    
                                </div>
                            </div>
                    </div>
                    
                </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
