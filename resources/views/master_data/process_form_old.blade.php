@extends('layouts.app')
@section('title', 'Add Process')
@section('content')
<style>
/*    .wizard > .content {
    background: #fff;}*/
span .select2-selection__rendered{
        width: 308.063px;
    }
    i{
        font-style: normal;
        width: 35px;
        line-height: 25px;
        font-size: 23px;
        
        display: inline-block;
        text-align: center;
    }
    .remove_field{
        color: red;
    }
</style>

<link href="css/sweetalert.css" rel="stylesheet" />
<div class="page-wrapper">
      <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Add Process</h4>
                <div class="ml-auto text-right">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#">Forms</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Process Form</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
     @if (Session::has('alert-success'))
<div class="alert alert-success alert-block"> <a class="close" data-dismiss="alert" href="#">×</a>
    <h4 class="alert-heading">Success!</h4>
    {{ Session::get('alert-success') }}
</div>
@endif
<!-- END BREADCRUMB -->
<!-- PAGE CONTENT WRAPPER -->
<div class="container-fluid">
    <form class="form-horizontal" id="form" method="post" action="{{ url('save_data') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
    <div class="form-group row">
                                <label for="name" class="col-sm-2">Date</label>
                                <div class="col-sm-4">
                                    <input type="text" name="date" class="form-control" value="<?php print strftime('%Y-%m-%d');?>" placeholder="mm/dd/yyyy" readonly/>
                                </div>
                                
                            </div>
    <div class="form-group row">
        <label for="name" class="col-sm-2">Process Name</label>
                                <div class="col-sm-8">

<input list="browsers" name="process_name" class="form-control process_name" style="width:500px;" id="process_name">
  <datalist id="browsers">
    @foreach($process_detail as $fixture)
                                        <option value="{{$fixture->process_name}}">{{$fixture->process_name}}</option>
                                        @endforeach
  </datalist>
                                    <!--<input type="text" name="process_name" id="process_name" value="" class="form-control process_name" autocomplete="off"/>-->
                                </div>
                <label for="name" class="col-sm-1">Version</label>
                                <div class="col-sm-1">
                                    <input type="text" name="version" id="version" value="1" class="form-control" value="1" readonly/>
                                </div>
                <input type="hidden" name="process_id" id="process_id" value=""/>
    </div>
        <div class="form-group row">
         <label for="name" class="col-sm-2">Area</label> 
         <div class="col-sm-4">
           <select class="select2 required form-control custom-select" name="area" id="area" >
                                        <option value="">-- Select Area --</option>
                                        <option value="Solapur">Solapur</option>
                                        <option value="Goregaon">Goregaon</option>
                                              </select>   
         </div>
         <label for="name" class="col-sm-2">Client</label> 
         <div class="col-sm-4">
             <select class="select2 required form-control custom-select" name="client_id[]" id="client_id">
                                        <option value="">-- Select Client --</option>
                                        @foreach($client_detail as $c)
                                        <option value="{{$c->id}}">{{$c->name}}</option>
                                        @endforeach
                                              </select>   
         </div>                                     
        </div>
        <div class="form-group row">
         <label for="name" class="col-sm-2">Quality Auditor</label> 
         <div class="col-sm-4">
           <select class="select2 required form-control custom-select" name="quality_auditor" id="quality_auditor" >
                                        <option value="">-- Select QA --</option>
                                       @foreach($qa_detail as $c)
                                        <option value="{{$c->id}}">{{$c->name}}</option>
                                        @endforeach
                                              </select>   
         </div>
         <label for="name" class="col-sm-2">Quality Manager</label> 
         <div class="col-sm-4">
             <select class="select2 required form-control custom-select" name="quality_manager" id="quality_manager">
                                        <option value="">-- Select QM --</option>
                                        @foreach($qm_detail as $c)
                                        <option value="{{$c->id}}">{{$c->name}}</option>
                                        @endforeach
                                              </select>   
         </div>                                     
        </div>
        <div class="form-group row">
                <label for="name" class="col-sm-2">Team Leader</label> 
         <div class="col-sm-4">
           <select class="select2 required form-control custom-select" name="team_leader" id="team_leader" >
                                        <option value="">-- Select TL --</option>
                                        @foreach($tl_detail as $c)
                                        <option value="{{$c->id}}">{{$c->name}}</option>
                                        @endforeach
                                              </select>   
         </div>    
        </div>
<div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" border="0">
                                        <thead>
                                            <tr>
                                                <th style="width:5px;"><b>Action</b></th>
                                                <th><b>Parameter Name</b></th>
                                                <th><b>Sub Parameter Name</b></th>
                                                <th><b>Rating</b></th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody id="h_lost">
                                            <tr class="input_fields_wrap">
                                                <td style="width:0.5%;"><i class="fas fa-plus-circle add_field_button" style="color: #0c8a54;"></i></td>
                                                <td class="parameter" colspan="2"><textarea name="parameter_detail[1][]" id="remark" class="form-control parameter checkblank" rows="1"  aria-required="true"></textarea></td>
                                                <!--<td class="parameter"><input type="text" name="parameter_detail[1][]" class="form-control parameter checkblank" value=" " style="width: 500px;height:70px;" /></td>-->
                                 
                                                <td style="width:5%;"></td>
                                                
                            
                                               
                                            </tr>
                                            <tr class="subprocess_row">
                                                <td style="width:2%;"></td>
                                                <td><i class="fas fa-plus-circle add_field_button1" style="color: blue;"></i><i class="fas fa-minus-circle remove_field1" style="color: red;"></i><br/></td>
                                                <!--<td></td>-->
                                                <td><textarea name="parameter_detail[1][]" id="remark" class="form-control checkblank" rows="1"  aria-required="true"></textarea></td>
                                                <td><input type="text" name="parameter_detail[1][]" class="form-control number checkblank"  style="width: 50px;" /></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>  
         <input type="hidden" name="prod_flag" id="prod_flag" value="0" />
         <div class="border-top">
                                <div class="card-body">
                                    <button type="submit" id="btnsave" class="btn btn-success">Save</button>
                                    <button type="button" name="btnsubmit" id="btnsubmit" class="btn btn-primary" value="Submit">Submit</button>
                                    <button type="button" name="btnupdate" id="btnupdate" class="btn btn-warning" value="Submit" style="display:none;">Update</button>
                                </div>
                            </div>
    </form>
       
</div>
</div>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">  
<script src="assets/libs/jquery/dist/jquery.min.js"></script>
<script src="js/jquery.steps.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="assets/libs/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('.number').keypress(function(event) {
                    var $this = $(this);
                    if ((event.which != 46 || $this.val().indexOf('.') != - 1) &&
                            ((event.which < 48 || event.which > 57) &&
                                    (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                    }

                    var text = $(this).val();
                    if ((event.which == 46) && (text.indexOf('.') == - 1)) {
                    setTimeout(function() {
                    if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                    $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                    }
                    }, 1);
                    }

                    if ((text.indexOf('.') != - 1) &&
                            (text.substring(text.indexOf('.')).length > 2) &&
                            (event.which != 0 && event.which != 8) &&
                            ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
                    }
                    });
                    
                    $('.number').bind("paste", function(e) {
                    var text = e.originalEvent.clipboardData.getData('Text');
                    if ($.isNumeric(text)) {
                    if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > - 1)) {
                    e.preventDefault();
                    $(this).val(text.substring(0, text.indexOf('.') + 3));
                    }
                    }
                    else {
                    e.preventDefault();
                    }
                    });
    //    var max_fields      = 6; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var i = 2;
    var x = 1; //initlal text box count
    var s = 14;
    var l=1;
   // var i =  $('.input_fields_wrap').length;
//    $(add_button).on('click', function () {
       $(document).on('click','.add_field_button',function(){
//        $(document).on('click', add_button, function (e) {
        var v = $(this).parents("td").prevAll(".parameter").eq(1).val();
       // alert($(this).parent().prev('.parameter').html());
              //  alert(v);
                
//                    $('#h_lost tr:not(:after)').each(function() 
//                       {
//                           var row = $(this).closest('tr');
//                           var in1=row.index();
//                           in1=in1-1;
//                           var type1 = row.find('td:eq(1)');
//                          alert("type"+type1);
//                       });
//        alert(i);
        i++; 
        l++;
        $("#h_lost").append('<tr class="input_fields_wrap delparam_'+l+'">\n\
            <td style="width:2%;"><i class="fas fa-plus-circle add_field_button" style="color: #0c8a54;"></i><i class="fas fa-minus-circle remove_field" style="color: red;"></i></td>\n\
            <td colspan="2"><textarea name="parameter_detail['+i+'][]" id="remark" class="form-control parameter checkblank" rows="1"  aria-required="true"></textarea></td>\n\
            <td style="width:5%;"></td>\n\
        </tr>'); //add input box
                  $("#h_lost").append('<tr class="subprocess_row delparam_'+l+'">\n\
            <td style="width:2%;"></td>\n\
            <td><i class="fas fa-plus-circle add_field_button1" style="color: blue;"></i><i class="fas fa-minus-circle remove_field1" style="color: red;"></i></td>\n\
            <td><textarea name="parameter_detail['+i+'][]" id="remark" class="form-control checkblank" rows="1"  aria-required="true"></textarea></td>\n\
            <td><input type="text" class="form-control number checkblank" name="parameter_detail['+i+'][]"  style="width: 50px;" /></td>\n\
    </tr>'); //add input box
            $('select').select2();
            $('.datepicker-autoclose').datepicker();
//            $('.timepicker1').timepicker();
//        $("#h_lost").on('click','.remove_field',function(){
//             $(this).closest('tr').next().remove();
//            $(this).parent().parent().remove();
//            
//        });
    });
    j=2;
     var add_button1      = $(".add_field_button1"); 
      var row = $(this).closest("tr .subprocess_row");
      //$(document).on('click', '.add_field_button1', function (g) {
         // alert("Hello");
//       $(add_button1).click(function(){ 
        $(document).on('click','.add_field_button1',function(){
//            alert("Hello");
           // alert( $(this).parent().siblings('td.parameter').html());
             var v =  $(this).closest('tr').find('.contact_name').val();
             alert(v);
//                $('#h_lost tr:not(:after)').each(function() 
//                       {
//                           var row = $(this).closest('tr');
//                           var in1=row.index();
//                           in1=in1-1;
//                           var type1 = row.find('td:eq(1)');
//                          alert("type"+type1);
//                       });
      //  var i =  $('.input_fields_wrap').length;
//        alert(i);
        i++; 
       
        $(this).closest('tr').after('<tr class="subprocess_row delparam_'+l+'">\n\
            <td style="width:2%;"></td>\n\
            <td><i class="fas fa-plus-circle add_field_button1" style="color: blue;"></i><i class="fas fa-minus-circle remove_field1" style="color: red;"></i></td>\n\
            <td><textarea name="parameter_detail['+i+'][]" id="remark" class="form-control checkblank" rows="1"  aria-required="true"></textarea></td>\n\
            <td><input type="text" class="form-control number checkblank" name="parameter_detail['+i+'][]"  style="width: 50px;" /><input type="hidden" name="parameter_detail['+i+'][7]" value="'+v+'" class="contact_name"/></td>\n\
</tr>'); //add input box
                //i++;
                if(v!="undefined")
                {
                }
            $('select').select2();
            $('.datepicker-autoclose').datepicker();
            j++;
//            $('.timepicker1').timepicker();
//        $("#h_lost").on('click','.remove_field',function(){
//            $(this).parent().parent().remove();
//        });
}); 

 $("#h_lost").on('click','.remove_field',function(){
           // $(this).closest('tr').next().remove();
           // $(this).parent().parent().remove();
           var classname = $(this). closest('tr').attr('class');
           var ret = classname.split(" ");
           var ret1 = ret[1].split("_");
            $('.delparam_'+ret1[1]).remove();
//$(this).parents('tr').next('.subprocess_row').remove();
            // $(this).parents('tr').nextUntil('.input_fields_wrap').andSelf().remove();
        });
        $("#h_lost").on('click','.remove_field1',function(){
            $(this).parent().parent().remove();
        });
        
var flag=0;
var bflag=0;
  $("#btnsubmit").click(function(){
   
    var process_name=$('.process_name').val();
    if(process_name == "")
    flag=1;
    $(".checkblank").each(function() {
                    var val=$(this).val();
                    if($(this).val() == "")
                    {
                         flag=1;
                         return false;
                    }
                    else
                    {
                        flag=0;
                    }                    
                });
    $("#prod_flag").val(1);
    $('.loader').show();
    //$("#btnsubmit").attr('disabled',true);
    if(flag == 1)
    {
         swal({
  position: 'top-end',
  type: 'warning',
  title: 'Please Fill All Fields',
  showConfirmButton: false,
  timer: 1500
}); 
    }
    else
    {
         $.ajax({
                    type: "POST",
                            url: 'save_data',
                            async: false,
                            cache: false,
                            beforeSend: function(){
                                    // Show image container
                                    //alert("hjl");
                                    $("#loader").show();
                                    },
                            data: $("#form").serialize(),
                            success: function(data){
                            console.log(data);
                            // swal({ type: "success", title: "Good Job!", confirmButtonColor: "#292929", text: "LogSheet Sumbmitted Successfully", confirmButtonText: "Ok" });
                            swal({
                            title: "Successfully Inserted!",
                                    text: "",
                                    type: "success",
                                    showCancelButton: false,
                                    closeOnConfirm: false,
                                    confirmButtonColor: "#e74c3c",
                                    showLoaderOnConfirm: true
                            },
                                    function(){
                                    setTimeout(function(){
                                    location.reload(true);
                                    }, 1000);
                                    });
                                    $('.next_btn').attr("disabled",false);
                            },
                            complete:function(data){
                                    // Hide image container
                                    $("#loader").hide();
                                    }
                    });
    }
    });
//    $("#btnsave").click(function(){
//   
//    var process_name=$('.process_name').val();
//    if(process_name == "")
//    flag=1;
//    $(".checkblank").each(function() {
//                    var val=$(this).val();
//                    if($(this).val() == "")
//                    {
//                         flag=1;
//                         return false;
//                    }
//                    else
//                    {
//                        flag=0;
//                    }
//                    
//                });
//    $("#prod_flag").val(0);
//    $('.loader').show();
//    //$("#btnsubmit").attr('disabled',true);
//    if(flag == 1)
//    {
//         swal({
//  position: 'top-end',
//  type: 'warning',
//  title: 'Please Fill All Fields',
//  showConfirmButton: false,
//  timer: 1500
//}); 
//    }
//    else
//    {
//         $.ajax({
//                    type: "POST",
//                            url: 'save_data',
//                            async: false,
//                            cache: false,
//                            beforeSend: function(){
//                                    // Show image container
//                                    //alert("hjl");
//                                    $("#loader").show();
//                                    },
//                            data: $("#form").serialize(),
//                            success: function(data){
//                            console.log(data);
//                            // swal({ type: "success", title: "Good Job!", confirmButtonColor: "#292929", text: "LogSheet Sumbmitted Successfully", confirmButtonText: "Ok" });
////                            swal({
////                            title: "Successfully Inserted!",
////                                    text: "",
////                                    type: "success",
////                                    showCancelButton: false,
////                                    closeOnConfirm: false,
////                                    confirmButtonColor: "#e74c3c",
////                                    showLoaderOnConfirm: true
////                            },
////                                    function(){
////                                    setTimeout(function(){
////                                    location.reload(true);
////                                    }, 1000);
////                                    });
//                                    $('.next_btn').attr("disabled",false);
//                            },
//                            complete:function(data){
//                                    // Hide image container
//                                    $("#loader").hide();
//                                    }
//                    });
//    }
//    });
    $("#btnupdate").click(function(){
   
    var process_name=$('.process_name').val();
//    var version=$('#version').val();
//    var new_version=parseInt(version) + 1;
    if(process_name == "")
    flag=1;
    $(".checkblank").each(function() {
                    var val=$(this).val();
                   if($(this).val() == "")
                    {
                         flag=1;
                         return false;
                    }
                    else
                    {
                        flag=0;
                    }
                    
                });
    $("#prod_flag").val(0);
    //$("#version").val(new_version);
    $('.loader').show();
    //$("#btnsubmit").attr('disabled',true);
    if(flag == 1)
    {
         swal({
  position: 'top-end',
  type: 'warning',
  title: 'Please Fill All Fields',
  showConfirmButton: false,
  timer: 1500
}); 
    }
    else
    {
         $.ajax({
                    type: "POST",
                            url: 'save_data',
                            async: false,
                            cache: false,
                            beforeSend: function(){
                                    // Show image container
                                    //alert("hjl");
                                    $("#loader").show();
                                    },
                            data: $("#form").serialize(),
                            success: function(data){
                            console.log(data);
                            // swal({ type: "success", title: "Good Job!", confirmButtonColor: "#292929", text: "LogSheet Sumbmitted Successfully", confirmButtonText: "Ok" });
                            swal({
                            title: "Successfully Inserted!",
                                    text: "",
                                    type: "success",
                                    showCancelButton: false,
                                    closeOnConfirm: false,
                                    confirmButtonColor: "#e74c3c",
                                    showLoaderOnConfirm: true
                            },
                                    function(){
                                    setTimeout(function(){
                                    location.reload(true);
                                    }, 1000);
                                    });
                                    $('.next_btn').attr("disabled",false);
                            },
                            complete:function(data){
                                    // Hide image container
                                    $("#loader").hide();
                                    }
                    });
    }
    });

        $('.process_name').on('change', function () {
    var res = $(this).val();
    var new_version;
    $('#process_id').val("");
    $("#btnsubmit").show();
    $("#btnsave").attr('disabled',false);
    $('#btnupdate').hide();
            $.ajax({
                                            url: 'fetch_process_details/' + res,
                                            type: "GET",
                                            success: function (response) {
                                                var data = JSON.parse(response);
                                                new_version=data.version;
                                                console.log(data);
                                                $('#h_lost').html(data.result);
                                                $('#process_id').val(data.process_id);
                                                
                                                i=data.index;
                                                if(data.flag == 1)
                                                {
                                                    $("#btnsubmit").hide();
                                                    $("#btnsave").attr('disabled',true);
                                                    $('#btnupdate').show();
                                                    new_version=parseInt(data.version) + 1;
                                                }
                                                $('#version').val(new_version);
                                                l=data.del_flag;

                                            }
                                        });
    });
});
function change_param(val,index)
{
    $("#h_lost").find(".delparam_"+index).each(function() { //get all rows in table
    var ratingTd = $(this).find('.contact_name').val();//Refers to TD element
    $(this).find('.contact_name').val(val);
});
}
</script>
@endsection
