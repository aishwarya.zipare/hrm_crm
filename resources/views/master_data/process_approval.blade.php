@extends('layouts.app')
@section('title', 'Edit Design Dept')
@section('content')
<style>
  .input-group-text{
      color: #8f9095;
      /*background-color: #dc3d59;*/
  }
  label {
    font-weight: bold;
}
   span .select2-selection__rendered{
        width: 308.063px;
    }
</style>
<div class="page-wrapper">
    
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 d-flex no-block align-items-center">
            <h4 class="page-title">Process Approval</h4>
            <div class="ml-auto text-right">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('home')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Process Approval</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            <form  id="orderForm" method="get" action="{{ url('approval_process') }}">
                <div class="card-body">  
                    <div class="form-group row">
                        <label class="col-sm-2 text-right control-label col-form-label">Process Name</label>
                        <div class="col-sm-4">                                                                                            
                            <select class="select2 form-control custom-select fixture_no" name="process_id" id="fixture_number" >
                                <option value="">-- Select --</option>
                                 @foreach($process_detail as $p)
                                        <option value="{{$p->id}}">{{$p->process_name}}</option>
                                    @endforeach
                            </select>
                        </div>
            
                    </div>
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <input type="submit"  id="btnsubmit" class="btn btn-info" value="Submit">
                        <a href="{{url('edit_design_dept')}}" class="btn btn-danger" >Cancel</a>
                        <!--<button type="button" class="btn btn-default">Save</button>-->                                    
                        <!--<button type="submit" class="btn btn-primary">Submit</button>-->
                    </div>
                </div>
                </form>
                </div>
        </div>
    </div>                    
</div>
</div>
 @endsection