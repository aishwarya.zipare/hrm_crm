<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//Add User
Route::get('email-validate/{id}','UserController@validateEmail');

Route::get('check-notifications/{id}','HomeController@fetch_notify');
Route::get('header','HomeController@getHeader');

Route::get('user_list','UserController@userList');
Route::get('add-user','UserController@addUser');
Route::post('add-user','UserController@saveUser');
Route::get('edit-user','UserController@ediUser');
Route::put('update-user/{id}','UserController@updateUser');
Route::get('delete-user/{id}','UserController@deletUser');
Route::get('sub-dept/{id}','UserController@subDept');
//Login Info Report 
Route::get('login-info','UserController@loginInfo');
Route::post('login-info','UserController@reportInfo');

Route::get('/','Auth\LoginController@login');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/forms', 'HomeController@forms')->name('forms');
Route::get('list','HomeController@statusList');
//Order routes
Route::get('list-order','OrderController@list_order');
Route::get('edit-order','OrderController@edit_order');
Route::put('update-order/{id}','OrderController@update_order');
Route::get('add-order','OrderController@add_order');
Route::post('add-order','OrderController@addOrder');
Route::get('fixture-validate/{id}','OrderController@validatefixture');

//Admin view
Route::get('dept-view','DepartmentController@view_status');
Route::get('dept_msg','DepartmentController@view_msg');

Route::get('dept-status','DepartmentController@add_status');
Route::post('update-status','DepartmentController@updateStatus');

//Report
Route::get('timeplanner_report','ReportController@index');
Route::post('download-report','ReportController@downloadReport');
Route::get('bom_report','ReportController@getBom');
Route::post('download_bom_report','ReportController@downloadBomreport');

Route::get('notify_report','ReportController@notifyReport');
Route::post('notify_report','ReportController@downloadNotify');

//Bill of material
Route::get('design_dept','DesignDepartment@get_design');
Route::get('fixure_det/{id}','DesignDepartment@fixture_detail');
Route::post('design_dept','DesignDepartment@saveDesign');
Route::get('edit_design_dept','DesignDepartment@get_fixture');
Route::post('edit_design_dept','DesignDepartment@edit_fixture');
Route::get('upload_design_det/{id}','DesignDepartment@uploadDetail');
Route::post('upload_design_det','DesignDepartment@update_uploadDetail');
Route::post('design_dept_edit/{id}','DesignDepartment@saveditDesign');

//Design Upload
Route::get('design_upload','DesignDepartment@designUpload');
Route::get('detail_no/{id}','ProductionPlanningController@getDetailNo');
Route::get('edit_detail_no/{id}','ProductionPlanningController@geteditDetailNo');
Route::post('detail_no','DesignDepartment@uploadDesign');

Route::get('production_planning','ProductionPlanningController@get_ppc');
Route::post('production_planning','ProductionPlanningController@save_ppc');
Route::get('edit_production_planning','ProductionPlanningController@get_fixture');
Route::post('edit_production_planning','ProductionPlanningController@edit_fixture');
Route::post('update_production_planning','ProductionPlanningController@update_ProductionDetail');
//Route::get();

//Machine Name
Route::get('machine_name/{id}','ProductionPlanningController@machine_name');

///Raw Material Department
Route::get('material_procurment','RawDepartment@get_material');
Route::get('raw_detail_no/{id}','RawDepartment@getDetailNo');
Route::post('material_procurment','RawDepartment@saveDesign');
//Operator Department
Route::get('out_scource_dept','OperatorDepartment@get_operator');
Route::get('ppcdetail_no/{id}','OperatorDepartment@getDetailNo');
Route::get('fetch_fixtures/{id}','OperatorDepartment@getFixtures');
Route::get('fetch_opration/{id}','OperatorDepartment@getOperation');
Route::get('cal_wt','OperatorDepartment@calculateWeight');
Route::post('out_scource_dept','OperatorDepartment@saveOperator');
Route::post('fetch_machine_time','OperatorDepartment@fetchTime');
Route::post('save_rework_data','OperatorDepartment@saveRework');
Route::post('check-response','OperatorDepartment@reworkResponse');

//supervisior
Route::get('super_form','SupervisiorDepartment@view_form');
Route::post('super_dept','SupervisiorDepartment@update_form');
//Quality Department
Route::get('quality_dept','QualityDepartment@get_quality');
Route::post('quality_dept','QualityDepartment@saveQuality');
//Assembly Dept
Route::get('assembly_dept','AssemblyDepartment@get_assembly');
Route::post('assembly_dept','AssemblyDepartment@saveAssembly');


//Edit RawMaterial Forms
Route::get('edit_material_procurment','RawDepartment@edit_raw');
Route::get('edit_raw_detail_no/{id}','RawDepartment@getEditDetailNo');
Route::post('edit_material_procurment','RawDepartment@fetch_raw');
Route::put('update-material/{id}','RawDepartment@update_raw');

//Edit Quality Forms
Route::get('edit_quality_dept','QualityDepartment@edit_quality');
Route::get('fetch_quality_fixture/{id}','QualityDepartment@getFixture');
Route::get('fetch_detail_no','QualityDepartment@getDetail');
Route::post('edit_quality_dept','QualityDepartment@fetch_quality');
Route::post('fetch_quality_data','QualityDepartment@fetch_data');
Route::post('update-quality','QualityDepartment@update_quality');

//Edit Assembly Department
Route::get('edit_assembly_dept','AssemblyDepartment@edit_assembly');
Route::get('fetch_assembly_fixture/{id}','AssemblyDepartment@getFixture');
Route::get('fetch_assembly_detail_no','AssemblyDepartment@getDetail');
Route::post('edit_assembly_dept','AssemblyDepartment@fetch_assembly');
Route::post('fetch_assembly_data','AssemblyDepartment@fetch_data');
Route::post('update-assembly','AssemblyDepartment@update_assembly');
//Excel Sheet
Route::get('student', 'StudentController@index')->name('index');
Route::post('import', 'StudentController@importFile')->name('import');