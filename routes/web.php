<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//notification
Route::get('check-notifications/{id}','HomeController@fetch_notify');
Route::get('header','HomeController@getHeader');
Route::get('/','Auth\LoginController@login');
Route::get('/home', 'HomeController@index')->name('home');
//user roles
Route::get('user_list','UserController@userList');
Route::get('add-user','UserController@addUser');
Route::post('add-user','UserController@saveUser');
Route::get('edit-user','UserController@ediUser');
Route::put('update-user/{id}','UserController@updateUser');
Route::get('delete-user/{id}','UserController@deletUser');
Route::get('sub-dept/{id}','UserController@subDept');
//role list
Route::get('role_list','UserController@roleList');
Route::get('add-role','UserController@addRole');
Route::post('add-role','UserController@saveRole');
Route::get('edit-role','UserController@editRole');
Route::put('update-role/{id}','UserController@updateRole');
Route::get('delete-role/{id}','UserController@deleteRole');
//Happiness demo
Route::get('process_form','ProcessController@getform');
Route::post('save_data','ProcessController@saveform');
Route::get('fetch_process_details/{id}','ProcessController@getProcess');
Route::get('search_process','ProcessController@search');
//approval process client
Route::get('approval_process','ClientController@approval_client');
Route::post('save_client_approval','ClientController@approval_process');
//approval process view
Route::get('approval_form','ClientController@approval_form');
//approval process by quality manager
Route::get('approval_manager','ClientController@approval_manager');
//process report
Route::get('process_report','ProcessController@report');
Route::post('download_process_report','ProcessController@download');

//rating form
Route::get('rating_form','RatingController@get_design');
Route::get('get_parameter','RatingController@get_parameter');
Route::get('get_ticket_info','RatingController@get_ticket_info');
Route::post('save_rating','RatingController@saveRating');

//upload audio
Route::get('upload_audio','AudioController@view_form');
Route::post('upload_audio','AudioController@save_data');

//calender activity
Route::get('calender','ActivityController@show_calender');
Route::post('save_activity','ActivityController@save_activity');
Route::post('update_activity','ActivityController@update_activity');
Route::get('check_activity','ActivityController@check_calender');
//rating report
Route::get('rating_report_form','RatingReportController@get_design');
Route::get('get_tickets','RatingReportController@get_tickets');
Route::get('get_parameter_list','RatingReportController@get_parameter_list');
Route::get('get_processes','RatingReportController@get_processes');
Route::get('get_rating_report','RatingReportController@get_rating_report');
Route::get('view_rating','RatingReportController@view_rating_form');

//upload att details
Route::get('upload-att-details','AttendanceController@getUploadForm');
Route::post('upload-att-details','AttendanceController@saveUploadForm');

//emp details
Route::get('emp-details','EmpController@getEmpData');
Route::get('add_emp','EmpController@getEmp');
Route::post('add-emp','EmpController@addEmp');
Route::get('emp-edit','EmpController@editEmp');
Route::post('emp_update','EmpController@updateEmp');
Route::get('emp-delete/{id}','EmpController@deleteEmp');

//sal details
Route::get('sal-details','SalaryController@getSalData');
Route::get('emp_view_slip','SalaryController@emp_view_slip');
Route::get('download-salary-slip','SalaryController@downloadSalarySlip');
Route::get('print-salary-slip','SalaryController@printSalarySlip');

//Leave details
Route::get('emp_leave_app','LeaveController@emp_leave_app');
Route::post('apply_leave','LeaveController@apply_leave');

//leave approval
Route::get('leave_app','LeaveController@leave_app');
Route::get('leave_approved','LeaveController@leave_approved');
Route::get('leave_declined','LeaveController@leave_declined');
Route::get('approve','LeaveController@approve');
Route::get('decline','LeaveController@decline');
Route::get('show_leaves','LeaveController@show_leaves');
Route::post('get_emp_leaves','LeaveController@get_emp_leaves');

//Route::get('add_emp','EmpController@getEmp');
//Route::post('add-emp','EmpController@addEmp');
//Route::get('emp-edit','EmpController@editEmp');
//Route::post('emp_update','EmpController@updateEmp');
//Route::get('emp-delete/{id}','EmpController@deleteEmp');

//feedback form
Route::get('feedback-form','FeedbackController@getFeedbackForm');
Route::get('get_call_info','FeedbackController@get_call_info');
Route::get('get_feedback_parameter','FeedbackController@get_feedback_parameter');
Route::post('save_feedback_form','FeedbackController@saveFeedbackForm');

//call evaluation form
Route::get('call-eval-form','CallEvalController@getCallEvalForm');
Route::post('save_calleval_form','CallEvalController@saveCallEvalForm');
Route::get('get_calleval_parameter','CallEvalController@get_calleval_parameter');