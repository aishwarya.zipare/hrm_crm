<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_agent";
    
    protected $fillable = [
        'user_id','agent_id','agent_name','agent_email','flag'
    ];
}
