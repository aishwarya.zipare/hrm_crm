<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubParameter extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_subparameter";
    public $timestamps = false;
    protected $fillable = [
        'process_id','parameter_id','subparameter_name','rating'
    ];
}
