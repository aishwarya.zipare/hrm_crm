<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_leaves";
    public $timestamps=false;
    protected $fillable = [
        'emp_id','emp_name','from_date','to_date','status','reason','total_days','short_notice_leave'
    ];
}

