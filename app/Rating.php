<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_rating_form";
    public $timestamps=false;
    protected $fillable = [
        'date','ticket_no','process','t_name','t_mobile_no','t_location','para_details'
    ];
}
