<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_process";
    public $timestamps = true;
    protected $fillable = [
        'process_date','process_name','area','quality_auditor','quality_manager','team_leader','clients','version','prod_flag','approval_flag'
    ];
}
