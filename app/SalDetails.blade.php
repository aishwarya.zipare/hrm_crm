<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalDetails extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_sal_details";
    public $timestamps=true;
    protected $fillable = [
        'year','month','emp_id','att_id','gross_sal','incentive','total_sal',
        'pf','esi','other','basic_sal','hra','conveyance'
    ];
}
