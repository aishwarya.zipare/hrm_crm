<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $primaryKey = "order_id";
    public $table = "tbl_order";
    
    protected $fillable = [
        'time_plan','fixture_number','quantity_no','project_desc','priority','packing_dispatch','dap_send_to_customer',
        'conf_dap','design_release','raw_material','imported_boughtout','manufacturing','quality_inspection','fixture_assembly',
        'receipt_of_component','assembly_cmm','machining_trial','remark','flag','msg_flag','created_date','updated_date',
        'task_dept','dept_task_status','final_status','fss_sheet','design_upload_status','order_date','order_time','operator_dept_status',
        'actual_packing_dispatch','actual_dap_send_to_customer','actual_conf_again_dap_from_customer',
        'actual_receipt_component','actual_assembly_cmm','actual_machining_trial','customer_name','fixture_name','part_no_name','order_type'
    ];
}
