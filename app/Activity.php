<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_activity_calender";
    
    protected $fillable = [
        'activity_name','owner','activity_date','user_id','description','status','mail_flag'
    ];
}
