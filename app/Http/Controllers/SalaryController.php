<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Session;
use File;
use App\Imports\ImportUsers;
use Excel;
use App\AttDetails;
use App\SalDetails;
use \PDF;

class SalaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getSalData()
    {
        return view('salary.salary_details');
    }
    public function emp_view_slip(Request $request)
{

        $curr_date = date('Y-m-d');
        $requestData = $request->all();
        $d=$requestData['month'];
//        echo "gchefg";
//        print_r($d);exit;
        
        if($d!="")
        {
        $date1 = strtr($_GET['month'], '/', '-');
        $date = explode('-', $date1);
        $month = $date[0];
        $year  = $date[1];
        $monthName = date("F", mktime(0, 0, 0, $month, 10));
//        echo $monthName; // Output: May
//        exit;
        if($date1!="")
        {
            $month_data = DB::table('tbl_att_details')
                        ->leftjoin('tbl_emp_details','tbl_emp_details.id','=','tbl_att_details.emp_id')
                        ->select('tbl_emp_details.*','tbl_att_details.*','tbl_att_details.id as att_id')
                        ->where('tbl_att_details.month','=',$monthName)
                        ->where('tbl_att_details.year','=',$year)
                        ->get();
//            echo "<pre>";
//            print_r($month_data);
//            exit;
            if(count($month_data)>0)
            { 
                $count1=0;
                foreach($month_data as $md)
                {
//                    print_r($md);
                    $total_days=$md->total_days;
                    $full_days=$md->full_days;
                    if($md->emp_type=="confirmed")
                    {
                        if($md->absent<=2)
                        {
                            $full_days=$full_days+$md->absent;
                        }
                    }
                    $basic_sal=round((($md->basic_sal/$total_days)*$full_days),0);
                    $hra=round((($md->hra/$total_days)*$full_days),0);
                    $conveyance=round((($md->conveyance/$total_days)*$full_days),0);
                    $total_sal=$basic_sal+$hra+$conveyance;
                    $pf=$md->pf;
                    $esi=$md->esi;
                    $total_dedc=$pf+$esi;
                    $net_pay=$total_sal-$total_dedc;
                    if(!empty($md))
                    {
                        if($md->pf=="")
                        {
                            $md->pf=0;
                        }
                        if($md->esi=="")
                        {
                            $md->esi=0;
                        }
                        $insert['month']=$monthName;
                        $insert['year']=$year;
                        $insert['emp_id']=$md->id;
                        $insert['att_id']=$md->att_id;
                        $insert['total_sal']=$total_sal;
                        $insert['basic_sal']=$basic_sal;
                        $insert['hra']=$hra;
                        $insert['conveyance']=$conveyance;
                        $insert['gross_sal']=$total_sal;
                        $insert['pf']=$md->pf;
                        $insert['esi']=$md->esi;
                        $insert['total_deduc']=$total_dedc;
                        $insert['net_pay']=$net_pay;
                        if($md->incentive!="")
                        {
                            $insert['incentive']=$md->incentive;
                        }
                        else
                        {
                            $insert['incentive']=0;
                        }
                        if($md->other!="")
                        {
                            $insert['other']=$md->other;
                        }
                        else
                        {
                            $insert['other']=0;
                        }
                    }
//                    print_r($insert);
                        $sal_list= DB::table('tbl_sal_details')->where('emp_id', '=', $md->id)
                                                    ->where('month','=',$monthName)
                                                    ->where('year','=',$year)
                                                    ->first();
                        if(!empty($sal_list))
                        {
                            $insertData = DB::table('tbl_sal_details')->where('emp_id','=',$insert['emp_id'])
                                                        ->where('tbl_sal_details.month','=',$monthName)
                                                        ->where('tbl_sal_details.year','=',$year)
                                                        ->update($insert);
                            $count1++;
                        }
                        else
                        {
                            $insertData = DB::table('tbl_sal_details')->insert($insert);
                            $count1++;
                        }     
                }
//                exit;
                $sal_data=DB::table('tbl_sal_details')
                        ->leftjoin('tbl_emp_details','tbl_emp_details.id','=','tbl_sal_details.emp_id')
                        ->select('tbl_emp_details.emp_name','tbl_sal_details.*','tbl_sal_details.emp_id as employee_id')
                        ->where('tbl_sal_details.month','=',$monthName)
                        ->where('tbl_sal_details.year','=',$year)
                        ->get();
                if(count($sal_data)>0)
                {
                    $report = "";
                    $report .= "<div class='widget-box'>
                        <div class='widget-title' style='background:#eeeeee;color: #000000;'><span class='icon'><i class='icon-th'></i></span>
                            <br>
                            <h5 style='background:#ffffff;'>Records</h5>
                        </div>
                        <div class='widget-content nopadding'>
                          <table class='table table-bordered'>
                            <thead>
                              <tr>
                                <th style='background:#27a9e3;color: #000000;' >Month</th>
                                <th style='background:#27a9e3;color: #000000;' >Employee name</th>
                                <th style='background:#27a9e3;color: #000000;' >Basic</th>
                                <th style='background:#27a9e3;color: #000000;' >Gross</th>
                                <th style='background:#27a9e3;color: #000000;' >PF</th>
                                <th style='background:#27a9e3;color: #000000;' >ESIA</th>
                                <th style='background:#27a9e3;color: #000000;' >Net Pay</th>
                                <th style='background:#27a9e3;color: #000000;' >Download</th>
                            </thead>
                            <tbody>";
                    foreach($sal_data as $data)
                    {
                       $report .= "<tr class='odd gradeX'>
                                    <td style='text-align:center;'>".$data->month."</td>
                                    <td style='text-align:center;'>".$data->emp_name."</td>
                                    <td style='text-align:center;'>".$data->basic_sal."</td>   
                                    <td style='text-align:center;'>".$data->gross_sal."</td>
                                    <td style='text-align:center;'>".$data->pf."</td>
                                    <td style='text-align:center;'>".$data->esi."</td>
                                    <td style='text-align:center;'>".$data->net_pay."</td>
                                    <td style='text-align:center;'><a href='download-salary-slip?data=$data->id' class='label label-success'><i class='icon-download'></i> Download</a></td>
                                  </tr>";
                    }
                    Session::flash('addmessage', 'Download Successfull');
                    $msg = "Yes";
                    echo json_encode($report);
                }
                else
                {
                    Session::flash('error', 'No Data Available');
                    $msg = "No";
                    echo json_encode("");
                }                
            } 
            else
            {
//                echo "in else";
//                exit;
                Session::flash('error', 'No Data Available');
                $msg = "No";
                echo json_encode("");
//               return view ('emp.emp_salary_slip',['emp'=>$emp]);
            }
        }
            else
            {
                Session::flash('error', 'Please select a month and year');
                $msg = "No";
                echo json_encode("");
//               return view ('emp.emp_salary_slip',['emp'=>$emp]);

            }
        }
        else
        {
            Session::flash('error', 'Please select a month and year');
            $msg = "No";
            echo json_encode("");
//           return view ('emp.emp_salary_slip',['emp'=>$emp]);

        }
    }
    
     public function downloadSalarySlip()
    {
        $id =$_GET['data'];
//        echo $id;
//        exit;
        $emp_record = DB::table('tbl_sal_details')
                ->leftjoin('tbl_emp_details','tbl_emp_details.id','=','tbl_sal_details.emp_id')
                ->select('tbl_sal_details.*','tbl_emp_details.emp_name','tbl_emp_details.pf_no','tbl_emp_details.bank_acc_no')
                ->where('tbl_sal_details.id','=',$id)
                ->first();
//        echo "<pre>";
//        print_r($emp_record);
//        exit;
       
//        $name=$emp_record->emp_id;
//        $emp = DB::table('tbl_att_details')->where('emp_name','=',$name)->first();
        $pdf = PDF::loadView('salary.salary_slip_pdf',['emp_record'=>$emp_record]);
        return $pdf->download('salary.salary_slip_pdf.pdf');  
    }
     public function printSalarySlip()
    {
        $id =$_GET['data'];
        $emp_record = DB::table('salary')->where('id','=',$id)->first();
        $pdf = PDF::loadView('emp.salary_slip_pdf',['emp_record'=>$emp_record]);
         echo "<pre>";print_r($pdf);exit;
        return $pdf->print('salary.salary_slip_pdf.pdf');  
    }
    
}
