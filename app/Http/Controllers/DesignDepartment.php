<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;
use File;
use Session;
use Excel;
use Samples;
use App\Imports\ImportUsers;
use Illuminate\Support\Facades\Auth;

class DesignDepartment extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = json_decode(Auth::user()->role,true);
//            echo "<pre>";print_r(Auth::user()->dept_id);exit;
            $user = Auth::user()->dept_id;
            $this->department = explode(",",$user);
           return $next($request);
        });
    }
  
    
    public function get_design(){
        if($this->role == 1 || $this->role == 13 ||$this->role == 14 || (in_array(3,$this->department) && ($this->role == 2 || $this->role == 3 || $this->role == 4))) {
            $fixture_detail = DB::table('tbl_design_manager')
                            ->select(['tbl_order.order_id','tbl_order.fixture_number'])
                            ->leftjoin('tbl_order','tbl_order.order_id','=','tbl_design_manager.order_id')
                            ->where('release_status','=',1)
                            ->where('tbl_order.design_status','=',0)
                            ->get();
//            $fixture_detail = \App\Order::select('order_id','fixture_number')->where(['design_status'=>0])->get();
            return view('bill_of_material.design_dept',['fixture_detail'=>$fixture_detail]);
        }else{
            return view('home');
        }
    }
    
    
    public function saveDesign(Request $request){
        $requestData = $request->all();
        date_default_timezone_set('Asia/Kolkata');
        $requestData['design_date'] = date('Y-m-d');
        $requestData['design_time'] = date("H:i:s");
        $design_data = \App\Design::create($requestData);
        $id = $design_data->design_id;
        $order_id = $design_data->order_id;
        $this->validate($request, array(
            'sample_file'      => 'required'
        ));
 
        if($request->hasFile('sample_file')){
            $extension = File::extension($request->sample_file->getClientOriginalName());
                if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                    $array = Excel::toArray(new ImportUsers, $request->file('sample_file'));
                }
                echo "<pre>";print_r($array);exit;
                $i = 0;
                date_default_timezone_set('Asia/Kolkata');
                $arr= array();
                if(count($array)>0){
//                    In Built Format
                    foreach ($array[0] as $key => $value) {
                        if($i > 12 && !empty($value[0]) && !empty($value[1])){
                            if(!in_array($value[1], $arr)){
                                $arr[] = $value[1];
                                $insert[] = [
                                'detail_no' => $value[1],
                                'item_desc' => $value[2],
                                'item_type' => $value[3],
                                'material_type' => $value[4],
                                'finish_size' => $value[5],
                                'qty' => $value[6],
                                'design_id' => $id,
                                'order_id' => $order_id,
                                'design_detail_date'=>date('Y-m-d'),
                                'design_detail_time'=>date("H:i:s")
                                ];
                            }
                        }
                        $i++;
                    }
//                    echo "<pre>";print_r($insert);exit;
                    if(!empty($insert)){
                        $insertData = DB::table('tbl_design_detail')->insert($insert);
                        if ($insertData) {
                            Session::flash('alert-success','Created Successfully.');
                        }else {                        
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }
        }
        \App\Order::where('order_id','=',$order_id)->update(['design_status'=>'1']);
//        \App\DesignManager::where('order_id','=',$order_id)->update(['release_status'=>'1']);
        return redirect('design_dept');
    }
    
    public function saveditDesign($id,Request $request){
        $requestData = $request->all();
//        echo $id;
//        echo "<pre>";print_r($requestData);exit;
        $design_detail = \App\Design::findorfail($id);
        date_default_timezone_set('Asia/Kolkata');
        $requestData['update_date_time'] = date('Y-m-d H:i:s');
        $design_detail->update($requestData);
        Session::flash('alert-success','Updated Successfully.');
        return redirect('edit_design_dept');
    }

}