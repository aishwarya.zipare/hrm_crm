<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;
use File;
use Session;
use Excel;
use Samples;
use App\Rating;
use App\Imports\ImportUsers;
use Illuminate\Support\Facades\Auth;
use Phpmailer;
class AudioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user_role = json_decode(Auth::user()->user_role,true);
              return $next($request);
        });
    }
    
    public function view_form()
    {
        return view('upload.upload_audio');
    }
    public function save_data(Request $request)
    {
        $requestData = $request->all();
        echo "<pre/>";print_r($requestData);exit;
       $requestData['user_id'] = $this->user_role;
       $requestData['activity_date'] = date('Y-m-d', strtotime($requestData['activity_date']));
       \App\Activity::create($requestData);
    }
    public function update_activity(Request $request)
    {
        $requestData = $request->all();
       $requestData['user_id'] = $this->user_role;
        $activity_detail = \App\Activity::select('*')->where(['id'=>$requestData['id']])->first();
       $activity_detail->update($requestData);
    }
    public function check_calender()
    {
         $date = date('Y-m-d');
         $activity_data = DB::table('tbl_activity_calender')
                              ->select(['tbl_activity_calender.*','users.*','tbl_activity_calender.id as activity_id'])
                               ->leftjoin('users','users.id','=','tbl_activity_calender.owner')
                               ->where(['users.flag'=> 0,'tbl_activity_calender.status'=>'Planned'])
                                ->where('tbl_activity_calender.activity_date','<=',$date)
                               ->get();
//         echo "<pre/>";print_r($activity_data);exit;
         foreach($activity_data as $data)
         {
             $this->sendMissedActivityMail($data->activity_id,$data->reporting_manager_mail);
             \App\Activity::where(['id'=>$data->activity_id])->update(['status'=>'Missed','mail_flag'=>1]);
         }
         
    }
    public function sendMissedActivityMail($id,$reciept)
    {
        $activity_detail = \App\Activity::select('*')->where(['id'=>$id])->first();
        require_once('phpmailer/PHPMailerAutoload.php');
            $flag1=false;
            $mail = new PHPMailer;
            $mail->isSMTP();   
			//$mail->isMail();
			// Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com'; //'smtp.gmail.com';             // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                     // Enable SMTP authentication
            $mail->Username = 'ha.hindsil@gmail.com';          // SMTP username
            $mail->Password = 'hind@1234'; // SMTP password
            $mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                          // TCP port to connect to            
//            $mail->Port = 465;                          // TCP port to connect to            
            $mail->From = "ha.hindsil@gmail.com";
            $mail->FromName = "MIBS Quality";
           // $mail->addAddress('aishwarya.zipare@iping.in');
            $mail->addAddress($reciept);
            $mail->isHTML(true); 
            $mail->Body = "Hello You have Missed Activity ".$activity_detail->activity_name;
            $mail->Subject = "Activity Details";
            $mail->AltBody = "This is the plain text version of the email content";
            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
               // return redirect('error');
            } else {
           echo "Sent";
                
            }
    }
}