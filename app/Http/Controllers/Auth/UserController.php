<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\User;
use Auth;
use App\Department;
use Session;
use DB;

class UserController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = Auth::user()->role;
           return $next($request);
        });
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dept_id = Auth::user()->dept_code;
//        if($this->role == 1)
//        {
//            $employee_list = DB::table('users')
//                        ->select(['users.*','tbl_department.name as dept_name'])
//                        ->leftjoin('tbl_department', 'tbl_department.dept_id', '=', 'users.dept_code')
//                        ->orderBy('users.id','desc')
//                        ->where('is_left','=','Yes')
//                        ->get();
//            //$employee_list = User::get();
//        }
//        else if($this->role == 2)
//        {
//            $employee_list = DB::table('users')
//                        ->select(['users.*','tbl_department.name as dept_name'])
//                        ->leftjoin('tbl_department', 'tbl_department.dept_id', '=', 'users.dept_code')
//                        ->where(['users.dept_code'=>$dept_id])
//                        ->where('is_left','=','Yes')
//                        ->orderBy('users.id','desc')
//                        ->get();
////            $employee_list = User::where(['dept_code'=>$dept_id])->get();
//        }
//        else{
//            return redirect('error');
//        }
        $employee_list = User::get();
       // echo "<pre>";print_r($employee_list);exit;
        return view('employee.employee',['employee_list'=> $employee_list]);
    }
    
    public function addEmployee()
    {
        $dept_id = Auth::user()->dept_code;
        if($this->role == 2)
        {
            $department = Department::where(['dept_id'=>$dept_id])->get();   
        }else{
            $department = Department::get(); 
        }
        return view('employee.add_employee',['department' => $department]);
    }
    
    public function create(Request $request)
    {
        $requestData = $request->all();
//        echo "<pre>";print_r($requestData);exit;
        $requestData['password'] = bcrypt($requestData['password']);
        User::create($requestData);
        Session::flash('alert-success','Employee Created Suceessfully. ');
        return redirect('employee');
    }

    public function edit()
    {
        if($this->role == 3){
            $id = Auth::user()->id;
            $employee_data = User::find($id);
        }else{
            $id = $_GET['id'];
            $employee_data = User::find($id);
        }
        
        $dept_id = Auth::user()->dept_code;
        if($this->role == 2)
        {
            $department = Department::where(['dept_id'=>$dept_id])->orderBy('dept_id', 'asc')->pluck('name', 'dept_id'); 
        }else{
            $department = Department::orderBy('dept_id', 'asc')->pluck('name', 'dept_id');
        }
        return view('employee.edit_employee',['employee_data'=>$employee_data,'department'=>$department]);
    }
    
    public function update($id, Request $request)
    {
        $requestData = $request->except('password');
        if ($request->password)
            $requestData['password'] = bcrypt($request->password);
        $employee_data = User::find($id);
        $employee_data->update($requestData);
        Session::flash('alert-success','Employee Updated Suceessfully.');
        return redirect('employee');
    }

    public function delete($id)
    {
        User::destroy($id);
        Session::flash('alert-success','Employee Deleted Suceessfully.');
        return redirect('employee');
    }
}
