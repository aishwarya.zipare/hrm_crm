<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    
    protected function authenticated()
    {
        /* Your db inserts */
        date_default_timezone_set("Asia/kolkata");
        $logData['user'] = Auth::user()->id;
        $logData['date'] = date('Y-m-d');
        $logData['login_time'] = date('H:m:s');
        \App\LoginInfo::create($logData);
        return redirect()->intended($this->redirectTo);
    }
    
    
    public function logout(Request $request)
    {
        date_default_timezone_set("Asia/kolkata");
        $id = Auth::user()->id;
        $log['logout_time'] = date('H:m:s');
        $user = \App\LoginInfo::where(['user'=>$id])->orderBy('id','desc')->first();
        $user->update($log);
        
        $this->guard()->logout();

        $request->session()->invalidate();
        
        return redirect('/');
    }
  
}
