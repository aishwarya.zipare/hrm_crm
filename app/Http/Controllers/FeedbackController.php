<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;
use File;
use Session;
use Excel;
use Samples;
use App\Feedback;
use App\Rating;
use App\Allrating;
use App\Imports\ImportUsers;
use Illuminate\Support\Facades\Auth;

class FeedbackController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = json_decode(Auth::user()->role,true);
//            echo "<pre>";print_r(Auth::user()->dept_id);exit;
            $user = Auth::user()->dept_id;
            $this->department = explode(",",$user);
           return $next($request);
        });
    }
    
    public function getFeedbackForm()
    {
        $call_ids=array();
//        echo "<pre>";
        $call_details=DB::table('tbl_feedback_form')
                            ->select('phone_number')
                            ->distinct('phone_number')
                            ->get();
        foreach($call_details as $r)
        {
            if (in_array($r->phone_number, $call_ids))
            {
            }
            else
            {
                $call_ids[]=$r->phone_number;
            }
            
        }
//        print_r($call_details);
        $call_details = DB::table('tbl_call_details')
                            ->select('id','phone_number')
                            ->whereNotIn('id', $call_ids)
                            ->get();
//        echo "<pre>";
//        print_r($call_details);
//        exit;
        
        $process_details = DB::table('tbl_process')
                            ->select('*')
                            ->get();
        return view('feedback.feedback_form',['call_details'=>$call_details,'process_details'=>$process_details]);
       
    }
    
    public function get_design(){
        if($this->role == 1 || $this->role == 13 ||$this->role == 14 || (in_array(3,$this->department) && ($this->role == 2 || $this->role == 3 || $this->role == 4)))
        {
        $rating_tickets=array();
        $rating_details=DB::table('tbl_rating_form')
                            ->select('ticket_no')
                            ->get();
        foreach($rating_details as $r)
        {
            if (in_array($r->ticket_no, $rating_tickets))
            {
            }
            else
            {
                $rating_tickets[]=$r->ticket_no;
            }
            
        }
        $ticket_details = DB::table('tbl_tickets')
                            ->select('ticket_no')
                            ->whereNotIn('ticket_no', $rating_tickets)
                            ->get();
        
        $process_details = DB::table('tbl_process')
                            ->select('*')
                            ->get();
        return view('rating_form',['ticket_details'=>$ticket_details,'process_details'=>$process_details]);
        }
        else{
            return view('home');
        }
    }
    public function get_call_info()
    {
        $call_details_id = $_GET['$call_details_id'];
        $call_details = DB::table('tbl_call_details')
                            ->select('*')
                            ->where('id','=',$call_details_id)
                            ->first();
        $report="";
        $report.="<div class='form-group row'>
                                    <label for='name' class='col-sm-2'>Agent Name</label>
                                        <div class='col-sm-4'>
                                            <input type='text' name='agent_name' id='agent_name' value='".$call_details->agent_name."' class='form-control' />
                                        </div>
                                    <label for='name' class='col-sm-1'></label>
                                    <label for='name' class='col-sm-1'>TL</label>
                                        <div class='col-sm-4'>
                                            <input type='text' name='process_tl' id='process_tl' value='".$call_details->process_tl."' class='form-control' />
                                        </div>
                                </div>
                                <div class='form-group row'>
                                    <label for='name' class='col-sm-2'>Call Auditor</label>
                                        <div class='col-sm-4'>
                                            <input type='text' name='call_auditor' id='call_auditor' value='".$call_details->call_auditor."' class='form-control' />
                                        </div>
                                </div>";
        echo json_encode($report);
    }
    public function get_feedback_parameter(){
        $process_id = $_GET['process_id'];
        $parameter_details = DB::table('tbl_parameters')
                            ->select('*')
                            ->where('process_id','=',$process_id)
                            ->get();
        $report = "";
        $report .= "<div class='card-body'>
                        <div class='table-responsive'>
                            <table class='table table-bordered' border='0' id='info'>
                                <thead>
                                    <th style='width:60px;text-align:center;'><b>Sr No.</b></th>
                                    <th colspan='2' style='width:500px;'><b>Parameter Name</b></th>
                                    <th style='width:10px;text-align:center;'><b>Target Rating</b></th>
                                    <th style='width:10px;text-align:center;'><b>Achieved Rating</b></th>
                                </thead>
                                <tbody id='h_lost'>";
        $a=1; 
        $i=0;
        foreach($parameter_details as $pd)
        {
            $subparameter_details = DB::table('tbl_subparameter')
                            ->select('*')
                            ->where('parameter_id','=',$pd->id)
                            ->get();
            if($pd->rating=="")
            {
                $pd->rating=0;
            }
            //rating on parameter and sub-parameter level
//            $report.="<tr class='input_fields_wrap'>
//                                                <td style='text-align:center;'>".$a."</td>
//                                                <td colspan='2'><input type='text' name='parameter_details[".$i."][0]' value='".$pd->parameter_name."' style='border:none;font-weight:bold;width:100%' readonly></td>
//                                                <td style='text-align:center;'><input type='text' name='parameter_details[".$i."][1]' id='parameter_details_".$i."_1' value='".$pd->rating."' style='border:none;text-align:center;font-weight:bold;' readonly></td> 
//                                                <td style='text-align:center;'><input type='text' name='parameter_details[".$i."][2]' id='parameter_details_".$i."_2' class='form-control' value='' style='width: 100px;text-align:center;font-weight:bold;' autocomplete='off' onkeyup='check_rating(".$i.")' ></td>                                                            
//                                            </tr>";
            //rating on sub-parameter level
            $report.="<tr class='input_fields_wrap'>
                                                <td style='text-align:center;'>".$a."</td>
                                                <td colspan='2'><input type='hidden' name='parameter_details[".$i."][0]' value='".$pd->id."' style='border:none;font-weight:bold;width:100%' readonly><input type='text' value='".$pd->parameter_name."' style='border:none;font-weight:bold;width:100%' readonly></td>
                                                <td style='text-align:center;'></td> 
                                                <td style='text-align:center;'></td>                                                            
                                            </tr>";
            $j=1;
            foreach($subparameter_details as $sd)
            {
                 $report.="<tr class='subprocess_row'>
                                                <td></td>
                                                <td style='width:50px;'></td>
                                                <td><input type='hidden'  name='parameter_details[".$i."][".$j."]' value='".$sd->id."' style='border:none;width:100%' readonly><input type='text'  value='".$sd->subparameter_name."' style='border:none;width:100%' readonly></td>
                                                <td style='text-align:center;'><input type='text' name='parameter_details[".$i."][".$j."1]' id='parameter_details_".$i."_".$j."1' value='".$sd->rating."' style='border:none;text-align:center;font-weight:bold;' readonly></td> 
                                                <td style='text-align:center;'><input type='text' name='parameter_details[".$i."][".$j."2]' id='parameter_details_".$i."_".$j."2' class='form-control' value='' style='width: 100px;text-align:center;font-weight:bold;' autocomplete='off' onkeyup='check_sub_rating(".$i.",".$j.")' ></td>                                                            
                                            </tr>";
                 $j++;
            }
            $i++;
            $a++;
        }
        $report.="<tr>"
                . "<td>Remarks<td>"
                . "<td colspan='3'><textarea name='remarks' id='remarks' rows='2' cols='95'></textarea></td>"
                . "</tr>";
        $report.="</tbody>
                            </table>
                        </div>
                    </div>";
            echo json_encode($report);
    }
    
     public function saveFeedbackForm(Request $request)
    {
        $requestData = $request->all();
//        echo "<pre>";
//        print_r("Form data");
//        print_r($requestData);
//        exit;
        $feedback= new Feedback;
        $feedback->date = $requestData['date'];
        $feedback->phone_number =$requestData['phone_number'];
        $feedback->process_id = $requestData['process_id'];
        $ratings=$requestData['parameter_details'];
        $feedback->parameter_details=json_encode($requestData['parameter_details']);
        $feedback->remarks=$requestData['remarks'];       
        $feedback->save();
        
        $res=DB::table('tbl_all_rating')
                            ->select('id')
                            ->where(['phone_number'=>$requestData['phone_number'],'process_id'=>$requestData['process_id']])
                            ->first();
        
       if(!empty($res))
        {
            $allrating = Allrating::find($res->id);
            $allrating->feedback_form=json_encode($requestData['parameter_details']);
            $allrating->save();
        }
        else
        {
            $allrating= new Allrating;
            $allrating->phone_number =$requestData['phone_number'];
            $allrating->process_id = $requestData['process_id'];
            $allrating->feedback_form=json_encode($requestData['parameter_details']);
            $allrating->save();
        }
        
        Session::flash('alert', 'Saved Successfully');
        return redirect('feedback-form');
    }
}