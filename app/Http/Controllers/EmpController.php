<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Session;
use File;
use App\Imports\ImportUsers;
use Excel;
use App\User;
use App\EmpDetails;

class EmpController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function getEmpData() 
    {
        $emp_data = DB::table('tbl_emp_details')->where(['is_active' =>1])->orderBy('emp_id', 'asc')->get();
        return view('emp.emp-details',['emp_data'=>$emp_data]);
    }
    public function getEmp() 
    {
//        echo "add";
        return view('emp.add-emp-details');
    }
    public function addEmp(Request $request) 
    {
        $requestData=$request->all();
//        echo "<pre>";
//        print_r($requestData['emp_name']);
//        exit;
        EmpDetails::create($requestData);
        
        $user=new User;
        $user->name=$requestData['emp_name'];
        $user->email = $requestData['email'];
        $password = bcrypt('123456');
        $user->password = $password;
        $user->role = "employee";
        $user->emp_id=$requestData['emp_id'];
        $user->save();    
        
        Session::flash('alert-success','Added Successfully.');
        return redirect('emp-details');
    }
    
     public function editEmp(){
        $id = $_GET['id'];
        $emp = EmpDetails::where(['id'=>$id,'is_active' => 1])->first();
        if(!empty($emp))
        {
            if($emp->other=="")
            {
                $emp->other=0;
            }
            if($emp->incentive=="")
            {
                $emp->incentive=0;
            }
            if($emp->pf=="")
            {
                $emp->pf=0;
            }
            if($emp->esi=="")
            {
                $emp->esi=0;
            }
        }
//        echo "<pre>";
//        print_r($emp);
//        exit;
        return view('emp.edit-emp-details',['emp'=>$emp]);
    }
    
    public function updateEmp(Request $request){
        $requestData = $request->all();
        $id = $requestData['id'];
        $emp = EmpDetails::where(['id'=>$id,'is_active' => 1])->first();
        $emp->update($requestData);
        Session::flash('alert-success', 'Updated Successfully.');
        return redirect('emp-details');
    }
    public function deleteEmp($id)
    {
        $status = 0;
        $query = EmpDetails::where('id', $id)->update(['is_active' => $status]);
        return redirect('emp-details');
    }
    
    

}
