<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Session;
use File;
use App\Imports\ImportUsers;
use Excel;
use App\AttDetails;

class AttendanceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getUploadForm()
    {
        return view('emp.upload_att_details');
    }
    
    public function saveUploadForm(request $request)
    {
        echo "<pre>";
        $requestData=$request->all();
//        echo "<pre>";
//        print_r($requestData);
//        exit;
        print_r($requestData['date']);
        if ($request->hasFile('call_file')) {
            $extension = File::extension($request->call_file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                $path = $request->call_file->getRealPath();
                $data = Excel::toArray(new ImportUsers, $request->file('call_file'));
                echo $path;
                if (!empty($data)) 
                {
                    $flag=0;
                    
                    for($i=4;$i<count($data[0]);$i+=2)
                    {
                        $month_row=$data[0][1];
                        $month_str=$month_row[0];
                        $m_words=explode(" ",$month_str);
                        $month=$m_words[5];
                        $year=$m_words[7];
                        $row=$data[0][$i];                        
                        $str=substr($row[0], 0, 4);
                        $emp_id=ltrim($str, '0');
                        $emp_data= \App\EmpDetails::select('id','emp_name')->where(['emp_id'=>$emp_id])->first();
                        $insert=array();
                        if(!empty($emp_data))
                        {
                            $insert['date']=$requestData['date'];
                            $insert['month']=$month;
                            $insert['year']=$year;
                            $insert['emp_id']=$emp_data['id'];
                            $insert['emp_name']=$emp_data['emp_name'];
                            $insert['total_days']=$row[41];
                            $insert['full_days']=$row[32];
                            $insert['half_days']=$row[33];
                            $insert['holidays']=$row[34];
                            $insert['wkk_off']=$row[35];
                            $insert['exception']=$row[36];
                            $insert['absent']=$row[37];
                            $insert['special']=$row[40];
                        }
                        
                        if(!empty($insert))
                        {
                            $empdata=DB::table('tbl_att_details')->where(['emp_id'=>$emp_data['id'],'month'=>$month,'year'=>$year])->first();
                            if(empty($empdata))
                            {
                                $insertData = DB::table('tbl_att_details')->insert($insert);
                            }
                            else
                            {
                                $updateData = DB::table('tbl_att_details')
                                        ->where('id','=',$empdata->id)
                                        ->update($insert);
                            }
                            $flag=$flag+1;
                        }   
                    }
                    if($flag>0)
                    {
                        Session::flash('alert-success', 'File Successfully uploaded');
                        $msg = "Yes";
                        return redirect('upload-att-details');
                    }
            } else {
                Session::flash('error', 'Unsuccessfull upload');
                $msg = "No";
                  return redirect('upload-att-details');
            }
        } else {
            Session::flash('error', 'Unsuccessfull upload');
            $msg = "No";
             return redirect('upload-att-details');
        }
        } else {
            Session::flash('error', 'Unsuccessfull upload');
            $msg = "No";
             return redirect('upload-att-details');
        }
    } 

}
