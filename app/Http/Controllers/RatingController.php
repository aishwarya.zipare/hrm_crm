<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;
use File;
use Session;
use Excel;
use Samples;
use App\Rating;
use App\Imports\ImportUsers;
use Illuminate\Support\Facades\Auth;

class RatingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = json_decode(Auth::user()->role,true);
//            echo "<pre>";print_r(Auth::user()->dept_id);exit;
            $user = Auth::user()->dept_id;
            $this->department = explode(",",$user);
           return $next($request);
        });
    }
    
    public function get_design(){
//        echo "rating";
//        exit;
        $rating_tickets=array();
        $rating_details=DB::table('tbl_rating_form')
                            ->select('ticket_no')
                            ->get();
        foreach($rating_details as $r)
        {
            if (in_array($r->ticket_no, $rating_tickets))
            {
            }
            else
            {
                $rating_tickets[]=$r->ticket_no;
            }
            
        }
        $ticket_details = DB::table('tbl_tickets')
                            ->select('ticket_no')
                            ->whereNotIn('ticket_no', $rating_tickets)
                            ->get();
        
//        echo "<pre>";
//        print_r($ticket_details);       
//        exit;
        $process_details = DB::table('tbl_process')
                            ->select('*')
                            ->get();
        return view('rating_form',['ticket_details'=>$ticket_details,'process_details'=>$process_details]);
        
    }
    public function get_ticket_info()
    {
        $ticket_no = $_GET['ticket_no'];
        $ticket_details = DB::table('tbl_tickets')
                            ->select('*')
                            ->where('ticket_no','=',$ticket_no)
                            ->first();
        $report="";
        $report.="<div class='form-group row'>
                                    <label for='name' class='col-sm-2'>Name</label>
                                        <div class='col-sm-4'>
                                            <input type='text' name='t_name' id='t_name' value='".$ticket_details->name."' class='form-control' />
                                        </div>
                                    <label for='name' class='col-sm-1'></label>
                                    <label for='name' class='col-sm-1'>Mobile No</label>
                                        <div class='col-sm-4'>
                                            <input type='text' name='t_mobile_no' id='t_mobile_no' value='".$ticket_details->mobile_no."' class='form-control' />
                                        </div>
                                </div>
                                <div class='form-group row'>
                                    <label for='name' class='col-sm-2'>Location</label>
                                        <div class='col-sm-4'>
                                            <input type='text' name='t_location' id='t_location' value='".$ticket_details->location."' class='form-control' />
                                        </div>
                                </div>";
        echo json_encode($report);
    }
    public function get_parameter(){
        $process_id = $_GET['process_id'];
        $parameter_details = DB::table('tbl_parameters')
                            ->select('*')
                            ->where('process_id','=',$process_id)
                            ->get();
        $report = "";
        $report .= "<div class='card-body'>
                        <div class='table-responsive'>
                            <table class='table table-bordered' border='0' id='info'>
                                <thead>
                                    <th style='width:60px;text-align:center;'><b>Sr No.</b></th>
                                    <th colspan='2' style='width:500px;'><b>Parameter Name</b></th>
                                    <th style='width:10px;text-align:center;'><b>Target Rating</b></th>
                                    <th style='width:10px;text-align:center;'><b>Achieved Rating</b></th>
                                </thead>
                                <tbody id='h_lost'>";
        $a=1; 
        $i=0;
        foreach($parameter_details as $pd)
        {
            $subparameter_details = DB::table('tbl_subparameter')
                            ->select('*')
                            ->where('parameter_id','=',$pd->id)
                            ->get();
            if($pd->rating=="")
            {
                $pd->rating=0;
            }
            $report.="<tr class='input_fields_wrap'>
                                                <td style='text-align:center;'>".$a."</td>
                                                <td colspan='2'><input type='text' name='para_details[".$i."][0]' value='".$pd->parameter_name."' style='border:none;font-weight:bold;width:100%' readonly></td>
                                                <td style='text-align:center;'><input type='text' name='para_details[".$i."][1]' id='para_details_".$i."_1' value='".$pd->rating."' style='border:none;text-align:center;font-weight:bold;' readonly></td> 
                                                <td style='text-align:center;'><input type='text' name='para_details[".$i."][2]' id='para_details_".$i."_2' class='form-control' value='' style='width: 100px;text-align:center;font-weight:bold;' autocomplete='off' onkeyup='check_rating(".$i.")' ></td>                                                            
                                            </tr>";
            $j=3;
            foreach($subparameter_details as $sd)
            {
                 $report.="<tr class='subprocess_row'>
                                                <td></td>
                                                <td style='width:50px;'></td>
                                                <td><input type='text'  name='para_details[".$i."][".$j."]' value='".$sd->subparameter_name."' style='border:none;width:100%' readonly></td>
                                                <td></td>
                                                <td></td>
                                            </tr>";
                 $j++;
            }
            $i++;
            $a++;
        }
        $report.="</tbody>
                            </table>
                        </div>
                    </div>";
//        exit;
            echo json_encode($report);
    }
    
     public function saveRating(Request $request)
    {
        $requestData = $request->all();
//        echo "<pre>";
//        print_r("Form data");
//        print_r($requestData);
        $rating= new Rating;
        $rating->date = $requestData['date'];
        $rating->ticket_no =$requestData['ticket_no'];
        $rating->process = $requestData['process'];
        $rating->t_name = $requestData['t_name'];
        $rating->t_mobile_no=$requestData['t_mobile_no'];
        $rating->t_location = $requestData['t_location'];
        $rating->para_details=json_encode($requestData['para_details']);
        $rating->save();
        Session::flash('alert', 'Saved Successfully');
        return redirect('rating_form');
//        exit;
    }
}