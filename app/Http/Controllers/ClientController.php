<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;
use File;
use Session;
use Excel;
use Samples;
use App\Rating;
use Phpmailer;
use App\Imports\ImportUsers;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user_role = json_decode(Auth::user()->user_role,true);
              return $next($request);
        });
    }
    
   public function approval_client()
   {
       if(isset($_GET["id"]))
       {
        $nofify_id=$_GET["id"];
       
        
         $approval_notify_detail = \App\NotifyApproval::findorfail($nofify_id);
            $requestData['read_time'] = date("H:i:s");
            $requestData['read_date'] = date('Y-m-d');
            $requestData['read_status'] = 1;
            $approval_notify_detail->update($requestData);
        
        $id = $approval_notify_detail->process_id;
       }
       if(isset($_GET["process_id"]))
       {
           $id=$_GET["process_id"];
       }
       $process_detail = \App\Process::select('*')->where('tbl_process.id','=',$id)->first();
        $parameter_detail = \App\Parameter::select('*')->where('process_id','=',$process_detail->id)->get();
        $sub_parameter_detail = \App\SubParameter::select('*')->where('process_id','=',$process_detail->id)->get();
        return view('master_data.client_approval',['process_detail'=>$process_detail,'parameter_detail'=>$parameter_detail,'subparameter_detail'=>$sub_parameter_detail]);
   }
   public function approval_process(Request $request)
   {
       $requestData = $request->all();
       $requestData['user'] = $this->user_role;
       \App\ClientApproval::create($requestData);
       if(isset($requestData['status']) && $requestData['status'] == 1)
       \App\Process::where(['id'=>$requestData['process_id']])->update(['approval_flag'=>1]);
       
        $process_data = \App\Process::select('*')->where(['id'=>$requestData['process_id']])->first();
        $approval_notify['process_id'] = $process_data->id;
            $approval_notify['user_id'] = $process_data->clients;
            $approval_notify['notify_date'] = date("y-m-d");
            $approval_notify['notify_from'] = "C";
            $approval_notify_data = \App\NotifyApproval::create($approval_notify);
        $this->sendEmail($approval_notify_data->id);
         Session::flash('alert-success','Created Successfully.');
        return redirect('home');
   }
    public function sendEmail($approval_id)
    {
        
            $notification_data = \App\NotifyApproval::where(['id'=>$approval_id])->first();
            $id = $notification_data->process_id;
            $approval_data = \App\Process::where(['id'=>$id])->first();
            if(!empty($approval_data))
            {
            $client_data = \App\User::select('*')->where(['id'=>$approval_data->quality_manager])->first();
//            echo "<pre/>";print_r($client_data);exit;
            require_once('phpmailer/PHPMailerAutoload.php');
            $flag1=false;
            $mail = new PHPMailer;
            $mail->isSMTP();   
			//$mail->isMail();
			// Set mailer to use SMTP
            $mail->Host = 'smtp.gmail.com'; //'smtp.gmail.com';             // Specify main and backup SMTP servers
            $mail->SMTPAuth = true;                     // Enable SMTP authentication
            $mail->Username = 'ha.hindsil@gmail.com';          // SMTP username
            $mail->Password = 'hind@1234'; // SMTP password
            $mail->SMTPSecure = 'tls';                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port = 587;                          // TCP port to connect to            
//            $mail->Port = 465;                          // TCP port to connect to            
            $mail->From = "ha.hindsil@gmail.com";
            $mail->FromName = "MIBS Quality";
           // $mail->addAddress('aishwarya.zipare@iping.in');
            $mail->addAddress($client_data->email);
            $mail->isHTML(true); 
            $mail->Body = "Hello Please find link for process approval  http://192.168.1.54/MIBS_CRM/public/approval_manager?id=".$approval_id;
            $mail->Subject = "Process Approval";
            $mail->AltBody = "This is the plain text version of the email content";
            if (!$mail->send()) {
                echo 'Mailer Error: ' . $mail->ErrorInfo;
               // return redirect('error');
            } else {
           echo "Sent";
                
            }
            }
    }
   public function approval_form()
   {
        $process_detail = \App\Process::select('*')->get();
        return view('master_data.process_approval',['process_detail'=>$process_detail]);
   }
    public function approval_manager()
   {
       if(isset($_GET["id"]))
       {
        $nofify_id=$_GET["id"];
       
        
         $approval_notify_detail = \App\NotifyApproval::findorfail($nofify_id);
            $requestData['read_time'] = date("H:i:s");
            $requestData['read_date'] = date('Y-m-d');
            $requestData['read_status'] = 1;
            $approval_notify_detail->update($requestData);
        
        $id = $approval_notify_detail->process_id;
       }
       if(isset($_GET["process_id"]))
       {
           $id=$_GET["process_id"];
       }
       $client_detail = DB::table('users')
                              ->select(['users.*'])
                               ->leftjoin('tbl_role','tbl_role.id','=','users.user_role')
                               ->where('users.flag','=','0')
                                ->where('tbl_role.role_name','like','%Client%')
                               ->get();
        $qm_detail = DB::table('users')
                              ->select(['users.*'])
                               ->leftjoin('tbl_role','tbl_role.id','=','users.user_role')
                               ->where('users.flag','=','0')
                                ->where('tbl_role.role_name','like','%Quality Manager%')
                               ->get();
        $qa_detail = DB::table('users')
                              ->select(['users.*'])
                               ->leftjoin('tbl_role','tbl_role.id','=','users.user_role')
                               ->where('users.flag','=','0')
                                ->where('tbl_role.role_name','like','%Quality Auditor%')
                               ->get();
        $tl_detail = DB::table('users')
                              ->select(['users.*'])
                               ->leftjoin('tbl_role','tbl_role.id','=','users.user_role')
                               ->where('users.flag','=','0')
                                ->where('tbl_role.role_name','like','%Team Leader%')
                               ->get();
        $process_detail = \App\Process::select('*')->where('tbl_process.id','=',$id)->first();
        $approval_detail = \App\ClientApproval::select('*')->where('process_id','=',$id)->first();
        return view('master_data.manager_approval',['process_detail'=>$process_detail,'client_detail'=>$client_detail,'qm_detail'=>$qm_detail,'qa_detail'=>$qa_detail,'tl_detail'=>$tl_detail,'approval_detail'=>$approval_detail]);
//       
//        $parameter_detail = \App\Parameter::select('*')->where('process_id','=',$process_detail->id)->get();
//        $sub_parameter_detail = \App\SubParameter::select('*')->where('process_id','=',$process_detail->id)->get();
//        return view('master_data.manager_approval',['process_detail'=>$process_detail,'parameter_detail'=>$parameter_detail,'subparameter_detail'=>$sub_parameter_detail]);
   }
}