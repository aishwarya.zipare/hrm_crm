<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Operator;
use App\SubDepartment;
use Session;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user_role = Auth::user()->user_role;
           return $next($request);
        });
    }
    
    public function validateEmail($id) {
        $id = trim($id);
        if (\App\User::where('email', $id)->exists()) {
            echo "Email Already exists!";
        }
    }
    
    
    public function userList()
    {
        $users = DB::table('users')
                ->select('users.*','tbl_role.role_name')
//                ->leftjoin('tbl_department','tbl_department.dept_id','users.dept_id')
                ->leftjoin('tbl_role','tbl_role.id','users.user_role')
                ->where('users.flag','=',0)
                ->get();
//        echo "<pre>";print_r($users);exit;
        if($this->user_role == 1){
            return view('auth.user_list',['users'=>$users]);
        }else{
            return redirect('home');
        }
        
    }
    
    public function addUser()
    {
        if($this->user_role == 1){
            $roles = \App\Role::where('flag','=',0)->get();
//            echo "<pre>";print_r($department);exit;
            return view('auth.register',['roles'=>$roles]);
        }else{
            return redirect('home');
        }
    }
    
    public function saveUser(Request $request)
    {
        $requestData = $request->all();
//        echo "<pre>";print_r($requestData);exit;
        $requestData['password'] = bcrypt($requestData['password']);
        $user_data=\App\User::create($requestData);
        if($requestData['user_role']==3)
        {
                  $array=$requestData['agent_detail'];
                foreach($array as $d){
                $agent_data['user_id']=$user_data->id;
                $agent_data['agent_id']=$d[0];
                $agent_data['agent_name']=$d[1];
                $agent_data['agent_email']=$d[2];
                $p_data= \App\Agent::create($agent_data);
            }  
        }

        Session::flash('alert-success', 'Created Successfully.');
        return redirect('user_list');
    }
    
    public function ediUser()
    {
        $id = $_GET['id'];
        
        if($this->user_role == 1){
            $users = \App\User::findorfail($id);
//            echo "<pre>";print_r($users);exit;
             $agent_data = \App\Agent::where(['user_id'=>$id])->get();
            $roles = \App\Role::get();
            return view('auth.edit_user',['users'=>$users,'roles'=>$roles,'agent_data'=>$agent_data]);
        }else{
            return redirect('home');
        }
    }
    
    public function updateUser($id,Request $request)
    {
        $requestData = $request->except('password');
//        echo "<pre>";print_r($requestData);exit;
        if ($request->password)
            $requestData['password'] = bcrypt($request->password);
        $users = \App\User::findorfail($id);
        $users->update($requestData);
        if($requestData['user_role']==3)
        {
         $array=$requestData['agent_detail'];
        foreach($array as $d){
                $id=$d[3];
                $agent_data['agent_id']=$d[0];
                $agent_data['agent_name']=$d[1];
                $agent_data['agent_email']=$d[2];
                \App\Agent::where(['id'=>$id])->update($agent_data);
        }
        }
        Session::flash('alert-success', 'Updated Successfully.');
        return redirect('user_list');
    }
    
    public function deletUser($id)
    {
        $query= \App\User::where('id', $id)->update(['flag' => 1]);
        $agent= \App\Agent::where('user_id', $id)->update(['flag' => 1]);
        Session::flash('alert-success', 'Deleted Successfully.');
        return redirect('user_list');
    }
    
    public function loginInfo()
    {
        return view('auth.login_info');
    }
    public function roleList()
    {
        $roles = \App\Role::select('*')->where(['flag'=>0])->get();
//        echo "<pre>";print_r($users);exit;
        if($this->user_role == 1){
            return view('auth.role_list',['role_list'=>$roles]);
        }else{
            return redirect('home');
        }
        
    }
    public function addRole()
    {
        if($this->user_role == 1){
            return view('auth.add_role');
        }else{
            return redirect('home');
        }
    }
    
    public function saveRole(Request $request)
    {
        $requestData = $request->all();
//        echo "<pre>";print_r($requestData);exit;
        $role_data=\App\Role::create($requestData);

        Session::flash('alert-success', 'Created Successfully.');
        return redirect('role_list');
    }
    public function editRole()
    {
        $id = $_GET['id'];
        
        if($this->user_role == 1){
            $roles = \App\Role::findorfail($id);
            return view('auth.edit_role',['roles'=>$roles]);
        }else{
            return redirect('home');
        }
    }
    
    public function updateRole($id,Request $request)
    {
        $requestData = $request->except('password');
//        echo "<pre>";print_r($requestData);exit;
        $roles = \App\Role::findorfail($id);
        $roles->update($requestData);
        Session::flash('alert-success', 'Updated Successfully.');
        return redirect('role_list');
    }
    public function deleteRole($id)
    {
        $query= \App\Role::where('id', $id)->update(['flag' => 1]);
        Session::flash('alert-success', 'Deleted Successfully.');
        return redirect('role_list');
    }
}
