<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Session;
use File;
use App\Imports\ImportUsers;
use Excel;
use App\User;
use App\Leave;
use App\EmpDetails;

class LeaveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function emp_leave_app()
    {
        $emp_id=Auth::user()->emp_id;
        $leave_list=Leave::where('emp_id',$emp_id)->orderBy('id','desc')->get();
//        echo "<pre>";
//        print_r($leave_list);
//        exit;
        return view("leave.emp_leave_application",['leave_list'=>$leave_list]); 
    }
    
    public function apply_leave(Request $request)
    {
        $requestData=$request->all();
        $applied_date = date('Y-m-d');
        $date1 = strtotime($applied_date);  
        $date2 = strtotime($requestData['from_date']);  
        $diff = abs($date1 - $date2)/60/60/24;
        if($diff<7)
        {
            $short_notice_leave=1;
        }
        else
        {
            $short_notice_leave=0;
        }
        $emp_id=Auth::user()->emp_id;
        $emp_name=Auth::user()->name;
        $leave=new Leave;
        $leave->emp_id=$emp_id;
        $leave->emp_name=$emp_name;
        $leave->from_date=$requestData['from_date'];
        $leave->to_date=$requestData['to_date'];
        $leave->reason=$requestData['reason'];
        $leave->total_days=$requestData['total_days'];
        $leave->status="Pending";
        $leave->short_notice_leave=$short_notice_leave;
        $leave->save();
        Session::flash('alert-success', 'Leave application submitted successfully......');
        return redirect('emp_leave_app');
    }
    
    //admin page
    public function leave_app()
    {
        $leave_list=Leave::where('status','=', "Pending")->orderBy('id','desc')->get();
        $leave_list1=Leave::where('status','=', "Approved")->get();
        $leave_list2=Leave::where('status','=', "Declined")->get();
//        echo "<pre>";
//        print_r($leave_list);
//        exit;
        return view("leave.leave_approval",['leave_list'=>$leave_list]); 
    }
    
    public function approve()
    {
        $id = $_GET['id'];
        $emp = Leave::find($id);
        $emp->status="Approved";
        $emp->save();
        Session::flash('alert-success', 'Approved Successfully...');
        return redirect('leave_app');
    }
    public function decline()
    {
        $id = $_GET['id'];
        $emp = Leave::find($id);
        $emp->status="Declined";
        $emp->save();
        Session::flash('alert-success', 'Leave request has been declined....');
        return redirect('leave_app');
    }
    
    //admin
    public function show_leaves()
    {
        $emp_details= EmpDetails::select('emp_id','emp_name')->orderBy('emp_name')->get();
        return view("leave.show_leaves",['emp_details'=>$emp_details]);
    }
    
    public function get_emp_leaves(Request $request)
    {
        $requestData=$request->all();
        $from_date=$requestData['from_date'];
        $to_date=$requestData['to_date'];
        $emp_id=$requestData['emp_id'];
        $res=Leave::where(['emp_id'=>$emp_id])
                ->where('status','=','Approved')
                ->whereBetween('from_date', [$from_date, $to_date])
                ->get();
        $report="";
        if(count($res)>0)
        {
            $total_leaves=Leave::where(['emp_id'=>$emp_id])
                ->where('status','=','Approved')
                ->whereBetween('from_date', [$from_date, $to_date])
                ->sum('total_days');
            
            $short_notice_leaves=Leave::where(['emp_id'=>$emp_id])
                ->where('status','=','Approved')
                ->where('short_notice_leave','=','1')
                ->whereBetween('from_date', [$from_date, $to_date])
                ->sum('total_days');
            
//            print_r($total_leaves);
//            print_r($short_notice_leaves);
//            exit;
            $report.="<div class='card-body'><table width=100% style='border: 0px solid #d2cdcd;'><tr style='border: 0px;'><td style='border:0px;'>Total Leaves : ".$total_leaves." </td><td style='border:0px;'>Short Notice Leaves : ".$short_notice_leaves." </td><td style='border:10px;'></td></tr></table></div>";
            $report.="<div class='card-body'><h5 class='card-title'>LEAVE DETAILS</h5><div class='table-responsive'><table id='zero_config' class='table table-striped table-bordered'>
                            <thead>
                                <tr>
                                    <th style='font-weight: bolad'>Sr.No.</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Reason</th>
                                    <th>Total Days</th>
                                    <th>Short Notice Leave</th>
                                </tr>
                            </thead>
                            <tbody id='call_det'>";
            $i=1;
            foreach($res as $row)
            {
                $report.="<tr>"
                . "<td style='width:50px;'>".$i."</td>
                    <td style='width:100px;'>".$row->from_date."</td>
                    <td style='width:100px;'>".$row->to_date."</td>
                    <td style='width:200px;'>".$row->reason."</td>
                    <td style='width:100px;'>".$row->total_days."</td>";
                    if($row->short_notice_leave==1)
                    {
                    $report.="<td style='width:100px;'>Yes</td>
                    </tr>";
                    }
                    else
                    {
                        $report.="<td style='width:100px;'>No</td>
                    </tr>";
                    }
                $i++;
            }
                
            $report.="</tbody>"
                    . "</table>"
                    . "</div></div>";
            echo json_encode($report);
//            echo "<pre>";
//            print_r($res);
//            exit;
        }
        else
        {
            echo json_encode("");
        }
        
    }
    

}
