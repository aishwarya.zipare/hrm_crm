<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use DateTime;
use File;
use Session;
use Excel;
use Samples;
use App\Rating;
use App\Process;
use App\Imports\ImportUsers;
use Illuminate\Support\Facades\Auth;

class RatingReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->role = json_decode(Auth::user()->role,true);
//            echo "<pre>";print_r(Auth::user()->dept_id);exit;
            $user = Auth::user()->dept_id;
            $this->department = explode(",",$user);
           return $next($request);
        });
    }    
    public function get_design(){
        if($this->role == 1 || $this->role == 13 ||$this->role == 14 || (in_array(3,$this->department) && ($this->role == 2 || $this->role == 3 || $this->role == 4))) {
        $process_details = DB::table('tbl_process')
                            ->select('*')
                            ->get();
        return view('rating_report_form',['process_details'=>$process_details]);
        }
        else{
            return view('home');
        }
    }
    public function get_tickets(){
        $from_date = $_GET['from_date'];
        $to_date = $_GET['to_date'];
        $process_id = $_GET['process_id'];
        
        if($from_date=="")
        {
            $from_date=$to_date;
        }
        if($to_date=="")
        {
            $to_date=$from_date;
        }
        $ticket_details = DB::table('tbl_rating_form')
                            ->leftjoin('tbl_tickets','tbl_tickets.ticket_no','=','tbl_rating_form.ticket_no')
                            ->select('tbl_tickets.ticket_no');
                            
        if($from_date!="" && $to_date!="")
        {
            $ticket_details->WhereBetween('tbl_rating_form.date', [$from_date, $to_date]);
        }
        if($process_id!="")
        {
           $ticket_details->where('tbl_rating_form.process','=',$process_id);
        }
        
        $ticket_details1=$ticket_details->distinct()->get();
//        echo "<pre>";
//        print_r($ticket_details1);
//        exit;
        echo json_encode($ticket_details1);
    }
    public function get_processes(){
        $from_date = $_GET['from_date'];
        $to_date = $_GET['to_date'];
        if($from_date=="")
        {
            $from_date=$to_date;
        }
        if($to_date=="")
        {
            $to_date=$from_date;
        }
        $process_details = DB::table('tbl_rating_form')
                            ->leftjoin('tbl_process','tbl_process.id','=','tbl_rating_form.process')
                            ->select('tbl_process.process_name','tbl_process.id');
                            
        if($from_date!="" && $to_date!="")
        {
            $process_details->WhereBetween('tbl_rating_form.date', [$from_date, $to_date]);
        }
        
        $process_details1=$process_details->distinct()->get();
        echo json_encode($process_details1);
    }
    public function get_parameter_list(){
        $from_date = $_GET['from_date'];
        $to_date = $_GET['to_date'];
        $process = $_GET['process'];
        $ticket_no = $_GET['ticket_no'];
        $report="";
        if($from_date=="")
        {
            $from_date=$to_date;
        }
        if($to_date=="")
        {
            $to_date=$from_date;
        }
                
        if($from_date=="" && $to_date=="" && $process=="" && $ticket_no=="")
        {
            echo json_encode("");
        }else
        {
//            echo "in else";
            $rating_details = DB::table('tbl_rating_form')
                                ->leftjoin('tbl_process','tbl_process.id','=','tbl_rating_form.process')
                                ->select('tbl_rating_form.*','tbl_process.process_name');
            if($from_date!="" && $to_date!="")
            {
                $rating_details->WhereBetween('tbl_rating_form.date', [$from_date, $to_date]);
            }
            if($process!="")
            {
                $rating_details->where('tbl_rating_form.process','=',$process);
            }
            if($ticket_no!="")
            {
                $rating_details->where('tbl_rating_form.ticket_no','=',$ticket_no);
            }
            $rating_details1=$rating_details->get();
//            echo "<pre>";
//            print_r($rating_details1);
//            exit;
            $report="";
            $report.="<div class='card-body'>
                        <div class='table-responsive'>
                            <table class='table table-bordered'>
                                <thead>
                                    <tr>
                                        <th style='width:50px;text-align:center;'><b>Sr. No.</b></th>
                                        <th style='width:115px;text-align:center;'><b>Date</b></th>
                                        <th><b>Process</b></th>
                                        <th style='width:250px;text-align:center;'><b>Ticket No.</b></th>
                                        <th style='width:110px;text-align:center;'><b>Actions</b></th>
                                    </tr>
                                </thead>
                                <tbody id='h_lost'>";
            $i=1;
            foreach($rating_details1 as $rd)
            {
                $report.="<tr class='input_fields_wrap'>
                                        <td>".$i."</td>
                                        <td>".$rd->date."</td>
                                        <td>".$rd->process_name."</td>
                                        <td>".$rd->ticket_no."</td>
                                        <td><h3><a href='view_rating?data=$rd->ticket_no,$rd->process,$rd->date' class='m-r-10 mdi mdi-table-edit view_button'></a><a href='get_rating_report?data=$rd->ticket_no,$rd->process,$rd->date' class='m-r-10 mdi mdi-download download_button'></a></h3></td>
                                    </tr>";
                $i++;
            }
            $report.="          </tbody>
                            </table>
                        </div>";
            echo json_encode($report);
        } 
        
    }    
    public function get_rating_report()
    {
        $request = explode(',', $_GET['data']);        
        $ticket_no =$request[0];
        $process_id = $request[1];
        $date = $request[2];
         return view('download_rating_report',['ticket_no'=>$ticket_no,'process_id'=>$process_id,'date'=>$date]);
    }    
    public function view_rating_form()
    {
        $request = explode(',', $_GET['data']);        
        $ticket_no =$request[0];
        $process_id = $request[1];
        $date = $request[2];
        $rating_details= Rating::select('*')
                        ->where('date','=',$date)
                        ->where('process','=',$process_id)
                        ->where('ticket_no','=',$ticket_no)
                        ->first();
        $process=Process::select('process_name')
                         ->where('id','=',$process_id)
                         ->first();
        if(!empty($process))
        {
            $process_name=$process->process_name;
        }
        else
        {
            $process_name="";
        }
//        echo "<pre>";
//        print_r($process_name);
//        exit;
         return view('view_rating_form',['rating_details'=>$rating_details,'process_name'=>$process_name]);
        
    }
}