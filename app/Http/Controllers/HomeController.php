<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
       public function fetch_notify($id)
   {
        $user_id = Auth::user()->id;
        $date = date('Y-m-d');
        $notify_arr = array();
    $approval_notification = DB::table('tbl_approval_notification')
                        ->where('read_status' ,'=', 0)
                        ->where('user_id','=',$user_id)
                        ->get();
      $cnt = (count($approval_notification));
//       return view('layouts.common.header');
//      $cnt['count'] = $cnt1;
//      $cnt['message'] = 'New Notification';
//      $cnt['remaining'] = 'You have'.$cnt1.'unread messages.';
//      print_r($cnt);
//      print json_encode($cnt1, JSON_NUMERIC_CHECK);
        if($cnt > $id){
            $data['alert'] = $cnt;
            $data['complition_flag'] = 0;
            $data['user_id'] = $user_id;
        }else{
            $data['complition_flag'] = 1;
        }
        
        print json_encode($data, JSON_NUMERIC_CHECK);
   }
       public function getHeader(){
        return view('layouts.common.header');
    }
    
}
