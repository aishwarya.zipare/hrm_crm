<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesignDetail extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_design_detail";
    
    protected $fillable = [
        'detail_no','item_desc','item_type','material_type','finish_size','qty','design_id','order_id','upload_file','ppc_status',
        'design_detail_date','design_detail_time','material_finish_status','operator_finish_status','quality_finish_status','assembly_finish_status',
        'design_file_date','design_file_time'
    ];
}
