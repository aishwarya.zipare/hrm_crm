<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotifyApproval extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_approval_notification";
    
    protected $fillable = [
        'user_id','notify_date','process_id','read_time','read_status','read_date','notify_from'
    ];
}
