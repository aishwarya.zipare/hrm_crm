<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parameter extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_parameters";
    public $timestamps = false;
    protected $fillable = [
        'process_id','parameter_name','rating'
    ];
}
