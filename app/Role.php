<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_role";
    
    protected $fillable = [
        'role_name'
    ];
}
