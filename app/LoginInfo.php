<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginInfo extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_login_info";
    
    protected $fillable = [
        'date','user','login_time','logout_time'
    ];
}
