<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttDetails extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_att_details";
    public $timestamps=true;
    protected $fillable = [
        'emp_id','emp_name','total_days','full_days','half_days','holidays','wkk_off',
        'exception','month','year','date',
        'special','absent'
    ];
}
