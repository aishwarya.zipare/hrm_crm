<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientApproval extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_client_approval";
    public $timestamps = true;
    protected $fillable = [
        'process_id','status','remark','user'
    ];
}
