<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_feedback_form";
    public $timestamps=true;
    protected $fillable = [
        'date','phone_number','call_details_id','process_id','para_details'
    ];
}
