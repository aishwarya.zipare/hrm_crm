<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allrating extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_all_rating";
    public $timestamps=true;
    protected $fillable = [
        'phone_number','process_id','feedback_form','call_evaluation','call_calibration'
    ];
}
