<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmpDetails extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_emp_details";
    public $timestamps=true;
    protected $fillable = [
        'emp_id','emp_name','emp_mobile','email','dob','gender','gross_sal','incentive',
        'pf','esi','other','basic_sal','hra','conveyance','pf_no','esi_no','bank_acc_no','net_pay',
        'designation','emp_type'
    ];
}
