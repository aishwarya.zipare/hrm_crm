<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallDetail extends Model
{
    protected $primaryKey = "id";
    public $table = "tbl_call_details";
    public $timestamps = true;
    protected $fillable = [
        'call_id','contact_no','tele_caller_name','team_leader','process_name','customer_mobile_no'
    ];
}
