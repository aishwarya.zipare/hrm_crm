<?php
//ini_set('memory_limit', '-1');
set_time_limit(0);
ini_set('memory_limit', '900000M');
if (count($production) > 0 || count($machine_data) > 0) {
$date_from = strtotime($from_date); // Convert date to a UNIX timestamp
$date_to = strtotime($to_date); // Convert date to a UNIX timestamp 
for ($i = $date_from; $i <= $date_to; $i += 86400) {
    $date_arr[] = date("Y-m-d", $i);
}
$capacity=$prod_per=0;
?>
<table>
    <tr style="background-color: #27a9e3;"><td colspan="11" style="text-align:center;"><h2>MACHINE SUMMARY REPORT</h2></td></tr>  
</table>
<table border='1'>
    <tr><td colspan="11" style="text-align:center;"><b>Daily Machine Wise Down Time Report</b></td></tr>
    <tr>

        <th style="text-align:center;">Date</th>
        <!--<th style="text-align:center;">Supervisor</th>-->
        <th colspan="2" style="text-align:center;">Operator</th>
        <th colspan="2" style="text-align:center;">Helper</th>
        <th colspan="2" style="text-align:center;">Total</th>
        <th colspan="2" style="text-align:center;">Todate</th>
        <th colspan="2" style="text-align:center;">Per Day Worker</th>
      
    </tr>
    <tr>
        <?php
        $tssup = $twsup = $tsop = $twop = $tshelp = $twhelp = $tot_wire_operator = $tot_wire_helper = array();
        $str_supervisior = $str_operator = $str_helper = $wire_supervisior = $wire_operator = $wire_helper = $tot_wire_supervisior = array();
        $ssup = $sop = $shelp = $wsup = $wop = $whelp = array();
        $btotal_hrs1=$mtotal_hrs1=$etotal_hrs1="00:00";
        foreach ($date_arr as $date) {
            //For Stranding job card operators
            $basic = App\Basic::where(['log_date' => $date])->get();
            foreach ($basic as $data) {
                $str_supervisior[] = explode(',', $data->supevisior);
                $str_operator[] = explode(',', $data->operator);
                $str_helper[] = explode(',', $data->helper);


                foreach ($str_supervisior as $sup) {
                    foreach ($sup as $s) {
                        $ssup[] = $s;
                    }
                }
                foreach ($str_operator as $sup) {
                    foreach ($sup as $s) {
                        $sop[] = $s;
                    }
                }
                foreach ($str_helper as $sup) {
                    foreach ($sup as $s) {
                        $shelp[] = $s;
                    }
                }
               // echo "<pre/>"; print_r(array_unique($shelp));
            }
            //For Wire Drawing job card operators
            $coil_basic = \App\CoilDetails::where(['log_date' => $date])->get();
            foreach ($coil_basic as $data) {
                $wire_supervisior[] = explode(',', $data->supevisior);
                $wire_operator[] = explode(',', $data->operator);
                $wire_helper[] = explode(',', $data->helper);


                foreach ($wire_supervisior as $sup) {
                    foreach ($sup as $s) {
                        $wsup[] = $s;
                    }
                }
                foreach ($wire_operator as $sup) {
                    foreach ($sup as $s) {
                        $wop[] = $s;
                    }
                }
                foreach ($wire_helper as $sup) {
                    foreach ($sup as $s) {
                        $whelp[] = $s;
                    }
                }
             
            }
            
    //Todate Workers
    $data1 = explode('-', $date);
    $start_date= $data1[0]."-".$data1[1]."-"."01";
     $todate_basic = App\Basic::where('log_date', '>=', $start_date)->where('log_date', '<=',$date)->get();
      foreach ($todate_basic as $data) {
                $tot_str_supervisior[] = explode(',', $data->supevisior);
                $tot_str_operator[] = explode(',', $data->operator);
                $tot_str_helper[] = explode(',', $data->helper);


                foreach ($tot_str_supervisior as $sup) {
                    foreach ($sup as $s) {
                        $tssup[] = $s;
                    }
                }
                foreach ($tot_str_operator as $sup) {
                    foreach ($sup as $s) {
                        $tsop[] = $s;
                    }
                }
                foreach ($tot_str_helper as $sup) {
                    foreach ($sup as $s) {
                        $tshelp[] = $s;
                    }
                }
            }
            //Todate Wire Drawing job card operators
            $todate_coil_basic = \App\CoilDetails::where('log_date', '>=', $start_date)->where('log_date', '<=',$date)->get();
            foreach ($todate_coil_basic as $data) {
                $tot_wire_supervisior[] = explode(',', $data->supevisior);
                $tot_wire_operator[] = explode(',', $data->operator);
                $tot_wire_helper[] = explode(',', $data->helper);


                foreach ($tot_wire_supervisior as $sup) {
                    foreach ($sup as $s) {
                        $twsup[] = $s;
                    }
                }
                foreach ($tot_wire_operator as $sup) {
                    foreach ($sup as $s) {
                        $twop[] = $s;
                    }
                }
                foreach ($tot_wire_helper as $sup) {
                    foreach ($sup as $s) {
                        $twhelp[] = $s;
                    }
                }
//                echo "<pre/>"; print_r(array_unique($twhelp));
//                echo "<pre/>"; print_r(array_unique($tshelp));
            }
          //  exit;
            if (count($todate_basic) > 0 || count($todate_coil_basic) > 0) {
                // echo $date;
               // print_r($tssup);
                 $totdate_supervisior = count(array_filter(array_unique(array_merge($tssup, $twsup))));
                 ini_set('memory_limit', '-1');
                 $todate_operator = count(array_filter(array_unique(array_merge($tsop, $twop))));
                 ini_set('memory_limit', '-1');
                 $todate_helper = count(array_filter(array_unique(array_merge($tshelp, $twhelp))));
                 $todate = $totdate_supervisior + $todate_operator + $todate_helper;
                 $data1[2];
                 $per_day_worker = $todate/$data1[2];
            }
          //  exit;
            if (count($basic) > 0 || count($coil_basic) > 0) {
                // echo $date;
                 $total_supervisior = count(array_filter(array_unique(array_merge($ssup, $wsup))));
                 $total_operator = count(array_filter(array_unique(array_merge($sop, $wop))));
                 $total_helper = count(array_filter(array_unique(array_merge($shelp, $whelp))));
                $total = $total_supervisior + $total_helper + $total_operator;
                ?>
                <td style="text-align:center;">{{$date}}</td>
                <!--<td style="text-align:center;">{{$total_supervisior}}</td>-->
                <td colspan="2" style="text-align:center;">{{$total_operator}}</td>
                <td colspan="2" style="text-align:center;">{{$total_helper}}</td>
                <td colspan="2" style="text-align:center;">{{$total}}</td>
                <td colspan="2" style="text-align:center;">{{$todate}}</td>
                <td colspan="2" style="text-align:center;">{{round($per_day_worker,2)}}</td>
                
            </tr>
            <?php
            $ssup = $wsup = $sop = $wop = $shelp = $whelp = [];
            $tssup = $twsup = $tsop = $twop = $tshelp = $twhelp = $tot_wire_operator = $tot_wire_helper = [];
            $str_supervisior = $str_operator = $str_helper = $wire_supervisior = $wire_operator = $wire_helper = $tot_wire_supervisior = [];
        }
        
    }
    
     //echo "<pre/>";print_r($todate_basic);exit;
    ?>
<!--            <tr>
                 <td style="text-align:center;">Todate</td>
                 <td colspan="2" style="text-align:center;">{{$todate}}</td>
                 <td colspan="2" style="text-align:center;">Per Day Worker</td>
                  <td colspan="2" style="text-align:center;">{{round($per_day_worker,2)}}</td>
            </tr>-->
</table>
<br/>
<br/>
<?php // exit;?>
<table border='1'>
    <tr style="background-color: #5bb75b;">
        <th rowspan="3" style="text-align:center;">Date</th>
        <th rowspan="3"style="text-align:center;">Machine Name</th>
        <th rowspan="3" style="text-align:center;">Conductor Name</th>
        <th rowspan="3" style="text-align:center;vertical-align:center;">Machine<br/>Capacity<br/>(KM/Mt)</th>
        <th  rowspan="3" style="text-align: center;">Today's<br/>Production<br/>Achieved(Kg)</th>
        <th rowspan="3" style="text-align:center;">Today's<br/>Production<br/>Achieved(MT)</th>
        <th colspan="30"  style="text-align:center;">Stoppages</th>
<!--        <th style="text-align:center;">Nature Of E BreakDown</th>
        <th style="text-align:center;">Nature Of M BreakDown</th>
        <th style="text-align:center;">Nature Of O BreakDown</th>-->
        <!--<th rowspan="3" style="text-align:center;">Time</th>-->
<!--        <th style="text-align:center;">Total's Hours Lost</th>-->
    </tr>
    <tr style="background-color: #5bb75b;">
        <th colspan="4">Nature Of Process BreakDown</th>
        <th colspan="4">Nature Of E</th>
        <th colspan="4">Nature Of M</th>
        <th colspan="3">Shortage of raw material</th>
        <th colspan="3">Shortage of  empty bobbins</th>
        <th colspan="3">Shortage of feeding bobbins</th>
        <th colspan="3">Shortage of manpower</th>
        <th colspan="3">Wire entanglement</th>
        <th colspan="3">Wire breakages</th>
    </tr>
    <tr style="background-color: #5bb75b;">
        <th>Total Hrs</th>
        <th>Reason</th>
        <th>Todate</th>
        <th>%</th>
        <th>Total Hrs</th>
        <th>Reason</th>
        <th>Todate</th>
        <th>%</th>
        <th>Total Hrs</th>
        <th>Reason</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
    </tr>
<?php
$total_prod = $total_prod1=$weight= 0;
        
        $machine_array=array();
        $btotal_hrs=$mtotal_hrs=$etotal_hrs="00:00";
        $total_hrs=$total_hrs1=$final_hrs="00:00";
        $bdtotal_hrs=$edtotal_hrs=$mdtotal_hrs="00:00";
        $bdtotal_hrs1=$edtotal_hrs1=$mdtotal_hrs1="00:00";
        $raw_hrs1=$bobbin_hrs1=$feed_hrs1=$manp_hrs1=$ent_hrs1=$wb_hrs1="00:00";
        $bdraw_hrs1=$bdbobbin_hrs1=$bdfeed_hrs1=$bdmanp_hrs1=$bdent_hrs1=$bdwb_hrs1="00:00";
foreach ($date_arr as $date) {
    $data1 = explode('-', $date);
     $start_date= $data1[0]."-".$data1[1]."-"."01";
    $coil_data = DB::table('tbl_coil')
            ->select('tbl_coil.machine_name', 'tbl_coil.code')
            //->selectRaw('sum(weight) as weight')
            ->selectRaw('sum(production_wt) as weight')
            ->selectRaw('max(boobin_wt) as max')
            ->groupBy('tbl_coil.machine_name')
            ->groupBy('tbl_coil.code')
            ->where(['tbl_coil.log_date' => $date])
            ->get();
//    echo "<pre>";print_r($coil_data);exit;
    foreach ($coil_data as $coil) {
        $down=$bdown=$edown=$mdown='';
        $down=$bdown1=$edown1=$mdown1='';
                $btotal_hrs=$mtotal_hrs=$etotal_hrs="00:00";
                $bdtotal_hrs=$edtotal_hrs=$mdtotal_hrs="00:00";
                $bdraw_hrs=$edtotal_hrs=$mdtotal_hrs="00:00";
                $bdbobbin_hrs=$bdfeed_hrs=$bdmanp_hrs=$bdent_hrs=$bdwb_hrs="00:00";
                $raw_hrs=$bobbin_hrs=$feed_hrs=$manp_hrs=$ent_hrs=$wb_hrs="00:00";
         $wt = $coil->weight;
        $mt = $coil->max;
        $kg=$wt*$mt;
        $mt=($wt)/1000;
        $machine_data = App\Machine::where(['machine_name' => $coil->machine_name])->first();
        $id_data = App\Coil::select('id')->where(['machine_name' => $coil->machine_name, 'code' => $coil->code, 'log_date' => $date])->get();
        if (count($id_data) > 0) {
            foreach($id_data as $data)
            {
           $id = $data->id;
        }
        } else {
            $id = 0;
        }
        //echo $id;
        if(count($machine_data)>0)
        {
         $capacity=(int)$machine_data->capacity;
        }
         $total_prod = $total_prod + $kg;
        $total_prod1 = $total_prod1 + $mt;
        if($capacity>0)
        {
            $prod_per = $coil->weight / $capacity * 100;
        }
        
        $dross_out = App\CoilStoppage::where(['coil_id' => $id])->get();
        // echo "<pre>";print_r($id);exit;
        $count1 = count($dross_out);
        $master_product= App\MasterProduct::where(['id' => $coil->code])->first();
        $conductor_name=App\Conductor::where(['id' => $master_product->master_conductor_id])->first();
        $machine_data= App\Machine::where(['id' => $coil->machine_name])->first();
        ?>
         <tr>
            <td rowspan="2" style="text-align:center;">{{$date}}</td>
            <td rowspan="2" style="text-align:center;">{{$machine_data->machine_name}}</td>
            <td rowspan="2" style="text-align:center;">{{$conductor_name->name}}</td>
            <td rowspan="2" style="text-align:center;">{{$machine_data->capacity}}</td>
             <td rowspan="2" style="text-align:center;">{{$wt}}</td>
            <td rowspan="2" style="text-align:center;">{{$mt}}</td>
             <?php  
             foreach($id_data as $data)
                     {
                          $id=$data->id;
                          $dross_out = App\CoilStoppage::where(['coil_id' => $id])->get();  
//                          echo "<pre/>";print_r($dross_out);exit;
                            $count1 = count($dross_out);
                             if(count($dross_out)>0){
                
                            foreach($dross_out as $dross) { 
                
                      $j = 1;
            //   echo "<pre>";print_r($dross);
            $datetime1 = new DateTime("24:00");
            $datetime2 = new DateTime($dross->total_hrs);
            $interval = $datetime1->diff($datetime2);
            $lost_hr = $interval->format('%h:%i');
             if($dross->total_hrs==null)
                    {
                    $dross->total_hrs="0:00";
             }
             if($dross->nature_of_breakdown!='')
             {
                    $bdown.=$dross->nature_of_breakdown.",";
                    if($dross->nature_of_breakdown=="Shortage_of_raw_material")
                    {
                         $raw_hrs=sum_the_time($raw_hrs.":00",$dross->total_hrs.":00");
                         $raw_hrs1=sum_the_time($raw_hrs1.":00",$dross->total_hrs.":00");
                    }
                    if($dross->nature_of_breakdown=="Shortage_of_empty_bobbins")
                    {
                         $bobbin_hrs=sum_the_time($bobbin_hrs.":00",$dross->total_hrs.":00");
                         $bobbin_hrs1=sum_the_time($bobbin_hrs1.":00",$dross->total_hrs.":00");          
                    }
                    if($dross->nature_of_breakdown=="Shortage_of_feeding_bobbins")
                    {
                         $feed_hrs=sum_the_time($feed_hrs.":00",$dross->total_hrs.":00");
                         $feed_hrs1=sum_the_time($feed_hrs1.":00",$dross->total_hrs.":00"); 
                    }
                    if($dross->nature_of_breakdown=="Shortage_of_manpower")
                    {
                        $manp_hrs=sum_the_time($manp_hrs.":00",$dross->total_hrs.":00");
                        $manp_hrs1=sum_the_time($manp_hrs1.":00",$dross->total_hrs.":00"); 
                    }
                     if($dross->nature_of_breakdown=="Wire_entanglement")
                    {
                        $ent_hrs=sum_the_time($ent_hrs.":00",$dross->total_hrs.":00");
                        $ent_hrs1=sum_the_time($ent_hrs1.":00",$dross->total_hrs.":00"); 
                    }
                     if($dross->nature_of_breakdown=="Wire Breakages")
                     {
                        $wb_hrs=sum_the_time($wb_hrs.":00",$dross->total_hrs.":00");
                        $wb_hrs1=sum_the_time($wb_hrs1.":00",$dross->total_hrs.":00"); 
                     }
                    if($dross->total_hrs!="")
                    {
                         $btotal_hrs=sum_the_time($btotal_hrs.":00",$dross->total_hrs.":00");
                         $btotal_hrs1=sum_the_time($btotal_hrs1.":00",$dross->total_hrs.":00");
                    }
                   
                   
             }
             
            if($dross->nature_of_e!='')
            {
                $edown.=$dross->nature_of_e.",";
                if($bdown=="")
                {
                    if($dross->total_hrs!="")
                    {
                    $etotal_hrs=sum_the_time($etotal_hrs.":00",$dross->total_hrs.":00");
                    $etotal_hrs1=sum_the_time($etotal_hrs1.":00",$dross->total_hrs.":00");
                    }
                }
            }
             if($dross->nature_of_m!='')
            {
                $mdown.=$dross->nature_of_m.",";
                if($bdown==""||$edown=="")
                {
                    if($dross->total_hrs!="")
                    {
                    $mtotal_hrs=sum_the_time($mtotal_hrs.":00",$dross->total_hrs.":00");
                    $mtotal_hrs1=sum_the_time($mtotal_hrs1.":00",$dross->total_hrs.":00");
                    }
                }
            }
            $total_hrs1=sum_the_time($total_hrs1.":00",$dross->total_hrs.":00");
                
                 $j++; $down='';} } 
                 }
             $todate_data = App\Coil::select('id')->where(['machine_name' => $coil->machine_name, 'code' => $coil->code])->where('log_date', '>=', $start_date)->where('log_date', '<=', $date)->get();
             foreach($todate_data as $data){
                  $id=$data->id;
                  $dross_out = App\CoilStoppage::where(['coil_id' => $id])->get(); 
                   if(count($dross_out)>0){
                       foreach($dross_out as $dross){
                           if($dross->nature_of_breakdown!='')
                            {
                               $bdown1.=$dross->nature_of_breakdown.",";
                                if($dross->nature_of_breakdown=="Shortage_of_raw_material")
                                {
                                    $bdraw_hrs=sum_the_time($bdraw_hrs.":00",$dross->total_hrs.":00");
                                    $bdraw_hrs1=sum_the_time($bdraw_hrs1.":00",$dross->total_hrs.":00");
                               }
                               if($dross->nature_of_breakdown=="Shortage_of_empty_bobbins")
                                {
                                    $bdbobbin_hrs=sum_the_time($bdbobbin_hrs.":00",$dross->total_hrs.":00");
                                    $bdbobbin_hrs1=sum_the_time($bdbobbin_hrs1.":00",$dross->total_hrs.":00");
                               }
                               if($dross->nature_of_breakdown=="Shortage_of_feeding_bobbins")
                                {
                                    $bdfeed_hrs=sum_the_time($bdfeed_hrs.":00",$dross->total_hrs.":00");
                                    $bdfeed_hrs1=sum_the_time($bdfeed_hrs1.":00",$dross->total_hrs.":00");
                               }
                               if($dross->nature_of_breakdown=="Shortage_of_manpower")
                                {
                                    $bdmanp_hrs=sum_the_time($bdmanp_hrs.":00",$dross->total_hrs.":00");
                                    $bdmanp_hrs1=sum_the_time($bdmanp_hrs1.":00",$dross->total_hrs.":00");
                               }
                               if($dross->nature_of_breakdown=="Wire_entanglement")
                                {
                                    $bdent_hrs=sum_the_time($bdent_hrs.":00",$dross->total_hrs.":00");
                                    $bdent_hrs1=sum_the_time($bdent_hrs1.":00",$dross->total_hrs.":00");
                               }
                                if($dross->nature_of_breakdown=="Wire Breakages")
                                {
                                    $bdwb_hrs=sum_the_time($bdwb_hrs.":00",$dross->total_hrs.":00");
                                    $bdwb_hrs1=sum_the_time($bdwb_hrs1.":00",$dross->total_hrs.":00");
                               }
                            if($dross->total_hrs!="")
                            {
                                 $bdtotal_hrs=sum_the_time($bdtotal_hrs.":00",$dross->total_hrs.":00");
                                 $bdtotal_hrs1=sum_the_time($bdtotal_hrs1.":00",$dross->total_hrs.":00");
                            }
                   
                            }
                             if($dross->nature_of_e!='')
            {
                $edown1.=$dross->nature_of_e.",";
//                if($bdown1=="")
//                {
                    if($dross->total_hrs!="")
                    {
                    $edtotal_hrs=sum_the_time($edtotal_hrs.":00",$dross->total_hrs.":00");
                    $edtotal_hrs1=sum_the_time($edtotal_hrs1.":00",$dross->total_hrs.":00");
                    }
                //}
            }
             if($dross->nature_of_m!='')
            {
                $mdown1.=$dross->nature_of_m.",";
//                if($bdown1==""||$edown1=="")
//                {
                    if($dross->total_hrs!="")
                    {
                    $mdtotal_hrs=sum_the_time($mdtotal_hrs.":00",$dross->total_hrs.":00");
                    $mdtotal_hrs1=sum_the_time($mdtotal_hrs1.":00",$dross->total_hrs.":00");
                    }
//                }
            }
                       }
                   }
             }
           $btill_hrs=str_replace(":",".",$btotal_hrs);
           $bdtill_hrs=str_replace(":",".",$bdtotal_hrs);
           if($btill_hrs>0)
           $btill_hrs=round(($btill_hrs/24)*100,2);
           if($bdtill_hrs>0)
           $bdtill_hrs=round(($bdtill_hrs/24)*100,2);
           //E Till Date
           $etill_hrs=str_replace(":",".",$etotal_hrs);
           $edtill_hrs=str_replace(":",".",$edtotal_hrs);
           if($etill_hrs>0)
           $etill_hrs=round(($etill_hrs/24)*100,2);
           if($edtill_hrs>0)
           $edtill_hrs=round(($edtill_hrs/24)*100,2);
           //M Till Date
            $mtill_hrs=str_replace(":",".",$mtotal_hrs);
           $mdtill_hrs=str_replace(":",".",$mdtotal_hrs);
           if($mtill_hrs>0)
           $mtill_hrs=round(($mtill_hrs/24)*100,2);
           if($mdtill_hrs>0)
           $mdtill_hrs=round(($mdtill_hrs/24)*100,2);
           
           //Process category
           //Raw material
            $rawtill_hrs=str_replace(":",".",$raw_hrs);
           $rawdtill_hrs=str_replace(":",".",$bdraw_hrs);
           if($rawtill_hrs>0)
           $rawtill_hrs=round(($rawtill_hrs/24)*100,2);
           if($rawdtill_hrs>0)
           $rawdtill_hrs=round(($rawdtill_hrs/24)*100,2);
           //Boobins empty
             $bobtill_hrs=str_replace(":",".",$bobbin_hrs);
           $bobdtill_hrs=str_replace(":",".",$bdbobbin_hrs);
           if($bobtill_hrs>0)
           $bobtill_hrs=round(($bobtill_hrs/24)*100,2);
           if($bobdtill_hrs>0)
           $bobdtill_hrs=round(($bobdtill_hrs/24)*100,2);
           //feeding bobbins
           $fedtill_hrs=str_replace(":",".",$feed_hrs);
           $fed1till_hrs=str_replace(":",".",$bdfeed_hrs);
           if($fedtill_hrs>0)
           $fedtill_hrs=round(($fedtill_hrs/24)*100,2);
           if($fed1till_hrs>0)
           $fed1till_hrs=round(($fed1till_hrs/24)*100,2);
           //Manpower
           $manptill_hrs=str_replace(":",".",$manp_hrs);
           $manpdtill_hrs=str_replace(":",".",$bdmanp_hrs);
           if($manptill_hrs>0)
           $manptill_hrs=round(($manptill_hrs/24)*100,2);
           if($manpdtill_hrs>0)
           $manpdtill_hrs=round(($manpdtill_hrs/24)*100,2);
           //entanglement
           $entill_hrs=str_replace(":",".",$ent_hrs);
           $ent1ill_hrs=str_replace(":",".",$bdent_hrs);
           if($entill_hrs>0)
           $entill_hrs=round(($entill_hrs/24)*100,2);
           if($ent1ill_hrs>0)
           $ent1ill_hrs=round(($ent1ill_hrs/24)*100,2);
           //wire breakages
            $wbtill_hrs=str_replace(":",".",$wb_hrs);
           $wbdtill_hrs=str_replace(":",".",$bdwb_hrs);
           if($wbtill_hrs>0)
           $wbtill_hrs=round(($wbtill_hrs/24)*100,2);
           if($wbdtill_hrs>0)
           $wbdtill_hrs=round(($wbdtill_hrs/24)*100,2);
             ?>
            <td rowspan="2" style="text-align:center;">{{$btotal_hrs}}</td>
            <td rowspan="2" style="text-align:center;">{{$bdown}}</td>
            <td rowspan="2" style="text-align:center;">{{$bdtotal_hrs}}</td>
            <td style="text-align:center;vailgn:middle;">{{$btill_hrs}}</td>
            <td  rowspan="2" style="text-align:center;">{{$etotal_hrs}}</td>
            <td rowspan="2" style="text-align:center;">{{$edown}}</td>
            <td rowspan="2" style="text-align:center;">{{$edtotal_hrs}}</td>
            <td style="text-align:center;vailgn:middle;">{{$etill_hrs}}</td>
            <td rowspan="2" style="text-align:center;">{{$mtotal_hrs}}</td>
            <td rowspan="2" style="text-align:center;">{{$mdown}}</td>
            <td rowspan="2" style="text-align:center;">{{$mdtotal_hrs}}</td>
            <td style="text-align:center;vailgn:middle;">{{$mtill_hrs}}</td>
             <td rowspan="2" style="text-align:center;">{{$raw_hrs}}</td>
             <td rowspan="2" style="text-align:center;">{{$bdraw_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$rawtill_hrs}}</td>
               <td rowspan="2" style="text-align:center;">{{$bobbin_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdbobbin_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$bobtill_hrs}}</td>
             
               <td rowspan="2" style="text-align:center;">{{$feed_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdfeed_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$fedtill_hrs}}</td>
             
               <td rowspan="2" style="text-align:center;">{{$manp_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdmanp_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$manptill_hrs}}</td>
             
               <td rowspan="2" style="text-align:center;">{{$ent_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdent_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$entill_hrs}}</td>
             
                <td rowspan="2" style="text-align:center;">{{$wb_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdwb_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$wbtill_hrs}}</td>
         </tr>
         <tr>
             <td style="text-align:center;">{{$bdtill_hrs}}</td>
             <td style="text-align:center;">{{$edtill_hrs}}</td>
             <td style="text-align:center;">{{$mdtill_hrs}}</td>
             <td style="text-align:center;">{{$rawdtill_hrs}}</td>
             <td style="text-align:center;">{{$bobdtill_hrs}}</td>
             <td style="text-align:center;">{{$fed1till_hrs}}</td>
             <td style="text-align:center;">{{$manpdtill_hrs}}</td>
             <td style="text-align:center;">{{$ent1ill_hrs}}</td>
             <td style="text-align:center;">{{$wbdtill_hrs}}</td>
         </tr>
            <?php 
            
              }
}
            ?>
              <tr style="background-color: #da4f49;">
                    <td style="text-align:center;">Wire Drawing<br/>Total</td>
                    <td colspan="3"></td>
                    <td style="text-align:center;">{{$total_prod}}</td>
                    <td style="text-align:center;">{{$total_prod1}}</td>
                     <td style="text-align:center;">{{$btotal_hrs1}}</td>
                    <td></td>
                    <td  style="text-align:center;">{{$bdtotal_hrs1}}</td>
                    <td></td>
                    <td style="text-align:center;">{{$etotal_hrs1}}</td>
                    <td></td>
                    <td  style="text-align:center;">{{$edtotal_hrs1}}</td>
                    <td></td>
                    <td style="text-align:center;">{{$mtotal_hrs1}}</td>
                    <td></td>
                     <td  style="text-align:center;">{{$mdtotal_hrs1}}</td>
                    <td  style="text-align:center;"><?php echo "Total Hrs Lost: ".$total_hrs1;?></td>
                    <td style="text-align:center">{{$raw_hrs1}}</td>
                    <td>{{$bdraw_hrs1}}</td>
                    <td></td>
                    <td style="text-align:center">{{$bobbin_hrs1}}</td>
                    <td style="text-align:center">{{$bobbin_hrs1}}</td>
                    <td></td>
                    
                    <td style="text-align:center">{{$feed_hrs1}}</td>
                    <td style="text-align:center">{{$bdfeed_hrs1}}</td>
                    <td></td>
                    
                    <td style="text-align:center">{{$manp_hrs1}}</td>
                    <td style="text-align:center">{{$bdmanp_hrs1}}</td>
                    <td></td>
                    
                    <td style="text-align:center">{{$ent_hrs1}}</td>
                    <td style="text-align:center">{{$bdent_hrs1}}</td>
                    <td></td>
                    
                    <td style="text-align:center">{{$wb_hrs1}}</td>
                    <td style="text-align:center">{{$bdwb_hrs1}}</td>
                    <td></td>
                    
                </tr>
</table>
<br/>
<br/>
<?php // exit;?>
<table border="1">
    <tr style="background-color: #5bb75b;">
        <th rowspan="3" style="text-align:center;">Date</th>
        <th rowspan="3"style="text-align:center;">Machine Name</th>
        <th rowspan="3" style="text-align:center;width:100px;">Conductor Name</th>
        <th rowspan="3" style="text-align:center;">Machine<br/>Capacity<br/>(KM/Mt)</th>
        <th  rowspan="3" style="text-align: center;width:100px;">Today's<br/>Production<br/>Achieved(KM)</th>
        <th rowspan="3" style="text-align:center;width:100px;">Today's<br/>Production<br/>Achieved(MT)</th>
        <th colspan="30"  style="text-align:center;">Stoppages</th>
<!--        <th style="text-align:center;">Nature Of E BreakDown</th>
        <th style="text-align:center;">Nature Of M BreakDown</th>
        <th style="text-align:center;">Nature Of O BreakDown</th>-->
        <!--<th rowspan="3" style="text-align:center;">Time</th>-->
<!--        <th style="text-align:center;">Total's Hours Lost</th>-->
    </tr>
    <tr style="background-color: #5bb75b;">
        <th colspan="4">Nature Of Process BreakDown</th>
        <th colspan="4">Nature Of E</th>
        <th colspan="4">Nature Of M</th>
        <th colspan="3">Shortage of raw material</th>
        <th colspan="3">Shortage of  empty bobbins</th>
        <th colspan="3">Shortage of feeding bobbins</th>
        <th colspan="3">Shortage of manpower</th>
        <th colspan="3">Wire entanglement</th>
        <th colspan="3">Wire breakages</th>
    </tr>
    <tr style="background-color: #5bb75b;">
        <th>Total Hrs</th>
        <th>Reason</th>
        <th>Todate</th>
        <th>%</th>
        <th>Total Hrs</th>
        <th>Reason</th>
        <th>Todate</th>
        <th>%</th>
        <th>Total Hrs</th>
        <th>Reason</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
        <th>Today</th>
        <th>Todate</th>
        <th>%</th>
    </tr>
                    <?php
                    $wt_mt=$wt_km=0;
                     $btotal_hrs=$mtotal_hrs=$etotal_hrs="00:00";
        $total_hrs=$total_hrs1=$final_hrs="00:00";
        $bdtotal_hrs=$edtotal_hrs=$mdtotal_hrs="00:00";
        $bdtotal_hrs1=$edtotal_hrs1=$mdtotal_hrs1="00:00";
        $raw_hrs1=$bobbin_hrs1=$feed_hrs1=$manp_hrs1=$ent_hrs1=$wb_hrs1="00:00";
        $bdraw_hrs1=$bdbobbin_hrs1=$bdfeed_hrs1=$bdmanp_hrs1=$bdent_hrs1=$bdwb_hrs1="00:00";
                    $btotal_hrs1=$mtotal_hrs1=$etotal_hrs1="00:00";
                    foreach ($date_arr as $date) {
                        $data1 = explode('-', $date);
                        $start_date= $data1[0]."-".$data1[1]."-"."01";
                      //  echo $date;
                         $drum_data = DB::table('tbl_drum')
                             ->select('tbl_drum.machine_name','tbl_drum.conductor_code')
                            ->selectRaw('sum(net_wt) as net_wt')
                            ->selectRaw('sum(length) as len')
                             ->groupBy('tbl_drum.machine_name')
                             ->groupBy('tbl_drum.conductor_code')
                             ->where(['tbl_drum.log_date'=>$date])
                            ->get();
                         
//                         echo "<pre/>";print_r($drum_data);exit;
                         $cond_range=0;
             foreach ($drum_data as $coil) {
                  $down=$bdown=$edown=$mdown='';
        $down=$bdown1=$edown1=$mdown1='';
                $btotal_hrs=$mtotal_hrs=$etotal_hrs="00:00";
                $bdtotal_hrs=$edtotal_hrs=$mdtotal_hrs="00:00";
                $bdraw_hrs=$edtotal_hrs=$mdtotal_hrs="00:00";
                $bdbobbin_hrs=$bdfeed_hrs=$bdmanp_hrs=$bdent_hrs=$bdwb_hrs="00:00";
                $raw_hrs=$bobbin_hrs=$feed_hrs=$manp_hrs=$ent_hrs=$wb_hrs="00:00";
                 $machine_data = App\Machine::where(['id' => $coil->machine_name])->first();
                $id_data = App\Drum::select('*')->where(['machine_name' => $coil->machine_name,'conductor_code' => $coil->conductor_code,'log_date' => $date])->get();
                $range_data = App\Production::select('cond_range')->where(['machine_name' => $coil->machine_name,'conductor_code' => $coil->conductor_code,'log_date' => $date])->first();
//                echo $range_data->cond_range; 
                if(count($range_data)>0)
                {
                    $cond_range=$range_data->cond_range;
                }
                
                if(count($id_data)>0)
          {
                     foreach($id_data as $data)
                     {
                           $id=$data->id;
                     }
             
          }
 else {
     $id=0;
 }
               
                $wt=$coil->len;
                $wt1=($coil->len*$cond_range)/1000;
                $wt_km=$wt_km+$wt;
                $wt_mt=$wt_mt+$wt1;
                if($capacity>0)
                {
                    $prod_per = $wt / $capacity * 100;
                }
                  $production_data = DB::table('tbl_master_product')
                              ->select(['tbl_conductor.*','tbl_master_product.*'])
                               ->leftjoin('tbl_conductor','tbl_conductor.id','=','tbl_master_product.master_conductor_id')
                              ->where('tbl_master_product.id', $coil->conductor_code) 
                               ->first();
                $machine_data= App\Machine::where(['id' => $coil->machine_name])->first();
                ?>
                     <tr>
            <td  rowspan="2" style="text-align:center;">{{$date}}</td>
            <td  rowspan="2" style="text-align:center;">{{$machine_data->machine_name}}</td>
            <td  rowspan="2" style="text-align:center;">{{$production_data->name}}</td>
            <td  rowspan="2" style="text-align:center;">{{$machine_data->capacity}}</td>
            <td  rowspan="2" style="text-align:center;mso-number-format:'#,##0.000'">{{number_format($wt,3)}}</td>
            <td  rowspan="2" style="text-align:center;">{{$wt1}}</td>
                    <?php
                     foreach($id_data as $data)
                     {
                          $id=$data->id;
                          $dros_out = App\DrossOut::where(['coil_id' => $id])->get();  
                            $count = count($dros_out);
                     
                     if(count($dros_out)>0){
            foreach($dros_out as $dross) { 
                $j = 1;
                $down='';
//               echo "<pre>";print_r($dross);
            $datetime1 = new DateTime("24:00");
            $datetime2 = new DateTime($dross->total_hrs);
            $total_hrs=sum_the_time($total_hrs.":00",$dross->total_hrs.":00");
            $interval = $datetime1->diff($datetime2);
            $lost_hr = $interval->format('%h:%i');
               if($dross->nature_of_breakdown!='')
             {
                    if($dross->nature_of_breakdown=="Shortage_of_raw_material")
                    {
                         $raw_hrs=sum_the_time($raw_hrs.":00",$dross->total_hrs.":00");
                         $raw_hrs1=sum_the_time($raw_hrs1.":00",$dross->total_hrs.":00");
                    }
                    if($dross->nature_of_breakdown=="Shortage_of_empty_bobbins")
                    {
                         $bobbin_hrs=sum_the_time($bobbin_hrs.":00",$dross->total_hrs.":00");
                         $bobbin_hrs1=sum_the_time($bobbin_hrs1.":00",$dross->total_hrs.":00");          
                    }
                    if($dross->nature_of_breakdown=="Shortage_of_feeding_bobbins")
                    {
                         $feed_hrs=sum_the_time($feed_hrs.":00",$dross->total_hrs.":00");
                         $feed_hrs1=sum_the_time($feed_hrs1.":00",$dross->total_hrs.":00"); 
                    }
                    if($dross->nature_of_breakdown=="Shortage_of_manpower")
                    {
                        $manp_hrs=sum_the_time($manp_hrs.":00",$dross->total_hrs.":00");
                        $manp_hrs1=sum_the_time($manp_hrs1.":00",$dross->total_hrs.":00"); 
                    }
                     if($dross->nature_of_breakdown=="Wire_entanglement")
                    {
                        $ent_hrs=sum_the_time($ent_hrs.":00",$dross->total_hrs.":00");
                        $ent_hrs1=sum_the_time($ent_hrs1.":00",$dross->total_hrs.":00"); 
                    }
                     if($dross->nature_of_breakdown=="Wire Breakages")
                     {
                        $wb_hrs=sum_the_time($wb_hrs.":00",$dross->total_hrs.":00");
                        $wb_hrs1=sum_the_time($wb_hrs1.":00",$dross->total_hrs.":00"); 
                     }
                    $bdown.=$dross->nature_of_breakdown.",";
                    $btotal_hrs=sum_the_time($btotal_hrs.":00",$dross->total_hrs.":00");
                    $btotal_hrs1=sum_the_time($btotal_hrs1.":00",$dross->total_hrs.":00");
             }
             
            if($dross->nature_of_e!='')
            {
                $edown.=$dross->nature_of_e.",";
                if($bdown=="")
                {
                    $etotal_hrs=sum_the_time($etotal_hrs.":00",$dross->total_hrs.":00");
                    $etotal_hrs1=sum_the_time($etotal_hrs1.":00",$dross->total_hrs.":00");
                }
            }
             if($dross->nature_of_m!='')
            {
                $mdown.=$dross->nature_of_m.",";
                if($bdown==""||$edown=="")
                {
                    $mtotal_hrs=sum_the_time($mtotal_hrs.":00",$dross->total_hrs.":00");
                    $mtotal_hrs1=sum_the_time($mtotal_hrs1.":00",$dross->total_hrs.":00");
                }
            }
            $final_hrs=sum_the_time($final_hrs.":00",$dross->total_hrs.":00");
            
                 $j++; $down='';} }  
             }
            // echo $coil->machine_name."&".$coil->conductor_code;
              $todate_data = App\Drum::select('id')->where(['machine_name' => $coil->machine_name, 'conductor_code' => $coil->conductor_code])->where('log_date', '>=', $start_date)->where('log_date', '<=', $date)->get();
            // echo "<pre>";print_r($todate_data);
              foreach($todate_data as $data){
                  $id=$data->id;
                  $dross_out = App\DrossOut::where(['coil_id' => $id])->get(); 
                   if(count($dross_out)>0){
                       foreach($dross_out as $dross){
                           if($dross->nature_of_breakdown!='')
                            {
                               $bdown1.=$dross->nature_of_breakdown.",";
                                if($dross->nature_of_breakdown=="Shortage_of_raw_material")
                                {
                                    $bdraw_hrs=sum_the_time($bdraw_hrs.":00",$dross->total_hrs.":00");
                                    $bdraw_hrs1=sum_the_time($bdraw_hrs1.":00",$dross->total_hrs.":00");
                               }
                               if($dross->nature_of_breakdown=="Shortage_of_empty_bobbins")
                                {
                                    $bdbobbin_hrs=sum_the_time($bdbobbin_hrs.":00",$dross->total_hrs.":00");
                                    $bdbobbin_hrs1=sum_the_time($bdbobbin_hrs1.":00",$dross->total_hrs.":00");
                               }
                               if($dross->nature_of_breakdown=="Shortage_of_feeding_bobbins")
                                {
                                    $bdfeed_hrs=sum_the_time($bdfeed_hrs.":00",$dross->total_hrs.":00");
                                    $bdfeed_hrs1=sum_the_time($bdfeed_hrs1.":00",$dross->total_hrs.":00");
                               }
                               if($dross->nature_of_breakdown=="Shortage_of_manpower")
                                {
                                    $bdmanp_hrs=sum_the_time($bdmanp_hrs.":00",$dross->total_hrs.":00");
                                    $bdmanp_hrs1=sum_the_time($bdmanp_hrs1.":00",$dross->total_hrs.":00");
                               }
                               if($dross->nature_of_breakdown=="Wire_entanglement")
                                {
                                    $bdent_hrs=sum_the_time($bdent_hrs.":00",$dross->total_hrs.":00");
                                    $bdent_hrs1=sum_the_time($bdent_hrs1.":00",$dross->total_hrs.":00");
                               }
                                if($dross->nature_of_breakdown=="Wire Breakages")
                                {
                                    $bdwb_hrs=sum_the_time($bdwb_hrs.":00",$dross->total_hrs.":00");
                                    $bdwb_hrs1=sum_the_time($bdwb_hrs1.":00",$dross->total_hrs.":00");
                               }
                            if($dross->total_hrs!="")
                            {
                                 $bdtotal_hrs=sum_the_time($bdtotal_hrs.":00",$dross->total_hrs.":00");
                                 $bdtotal_hrs1=sum_the_time($bdtotal_hrs1.":00",$dross->total_hrs.":00");
                            }
                   
                            }
                             if($dross->nature_of_e!='')
            {
                $edown1.=$dross->nature_of_e.",";
//                if($bdown1=="")
//                {
                    if($dross->total_hrs!="")
                    {
                    $edtotal_hrs=sum_the_time($edtotal_hrs.":00",$dross->total_hrs.":00");
                    $edtotal_hrs1=sum_the_time($edtotal_hrs1.":00",$dross->total_hrs.":00");
                    }
                //}
            }
             if($dross->nature_of_m!='')
            {
                $mdown1.=$dross->nature_of_m.",";
//                if($bdown1==""||$edown1=="")
//                {
                    if($dross->total_hrs!="")
                    {
                    $mdtotal_hrs=sum_the_time($mdtotal_hrs.":00",$dross->total_hrs.":00");
                    $mdtotal_hrs1=sum_the_time($mdtotal_hrs1.":00",$dross->total_hrs.":00");
                    }
//                }
            }
                       }
                   }
             }
           $btill_hrs=str_replace(":",".",$btotal_hrs);
           $bdtill_hrs=str_replace(":",".",$bdtotal_hrs);
           if($btill_hrs>0)
           $btill_hrs=round(($btill_hrs/24)*100,2);
           if($bdtill_hrs>0)
           $bdtill_hrs=round(($bdtill_hrs/24)*100,2);
           //E Till Date
           $etill_hrs=str_replace(":",".",$etotal_hrs);
           $edtill_hrs=str_replace(":",".",$edtotal_hrs);
           if($etill_hrs>0)
           $etill_hrs=round(($etill_hrs/24)*100,2);
           if($edtill_hrs>0)
           $edtill_hrs=round(($edtill_hrs/24)*100,2);
           //M Till Date
            $mtill_hrs=str_replace(":",".",$mtotal_hrs);
           $mdtill_hrs=str_replace(":",".",$mdtotal_hrs);
           if($mtill_hrs>0)
           $mtill_hrs=round(($mtill_hrs/24)*100,2);
           if($mdtill_hrs>0)
           $mdtill_hrs=round(($mdtill_hrs/24)*100,2);
           
           //Process category
           //Raw material
            $rawtill_hrs=str_replace(":",".",$raw_hrs);
           $rawdtill_hrs=str_replace(":",".",$bdraw_hrs);
           if($rawtill_hrs>0)
           $rawtill_hrs=round(($rawtill_hrs/24)*100,2);
           if($rawdtill_hrs>0)
           $rawdtill_hrs=round(($rawdtill_hrs/24)*100,2);
           //Boobins empty
             $bobtill_hrs=str_replace(":",".",$bobbin_hrs);
           $bobdtill_hrs=str_replace(":",".",$bdbobbin_hrs);
           if($bobtill_hrs>0)
           $bobtill_hrs=round(($bobtill_hrs/24)*100,2);
           if($bobdtill_hrs>0)
           $bobdtill_hrs=round(($bobdtill_hrs/24)*100,2);
           //feeding bobbins
           $fedtill_hrs=str_replace(":",".",$feed_hrs);
           $fed1till_hrs=str_replace(":",".",$bdfeed_hrs);
           if($fedtill_hrs>0)
           $fedtill_hrs=round(($fedtill_hrs/24)*100,2);
           if($fed1till_hrs>0)
           $fed1till_hrs=round(($fed1till_hrs/24)*100,2);
           //Manpower
           $manptill_hrs=str_replace(":",".",$manp_hrs);
           $manpdtill_hrs=str_replace(":",".",$bdmanp_hrs);
           if($manptill_hrs>0)
           $manptill_hrs=round(($manptill_hrs/24)*100,2);
           if($manpdtill_hrs>0)
           $manpdtill_hrs=round(($manpdtill_hrs/24)*100,2);
           //entanglement
           $entill_hrs=str_replace(":",".",$ent_hrs);
           $ent1ill_hrs=str_replace(":",".",$bdent_hrs);
           if($entill_hrs>0)
           $entill_hrs=round(($entill_hrs/24)*100,2);
           if($ent1ill_hrs>0)
           $ent1ill_hrs=round(($ent1ill_hrs/24)*100,2);
           //wire breakages
            $wbtill_hrs=str_replace(":",".",$wb_hrs);
           $wbdtill_hrs=str_replace(":",".",$bdwb_hrs);
           if($wbtill_hrs>0)
           $wbtill_hrs=round(($wbtill_hrs/24)*100,2);
           if($wbdtill_hrs>0)
           $wbdtill_hrs=round(($wbdtill_hrs/24)*100,2);
           ?>
             <td rowspan="2" style="text-align:center;">{{$btotal_hrs}}</td>
            <td rowspan="2" style="text-align:center;">{{$bdown}}</td>
            <td rowspan="2" style="text-align:center;">{{$bdtotal_hrs}}</td>
            <td style="text-align:center;vailgn:middle;">{{$btill_hrs}}</td>
            <td  rowspan="2" style="text-align:center;">{{$etotal_hrs}}</td>
            <td rowspan="2" style="text-align:center;">{{$edown}}</td>
            <td rowspan="2" style="text-align:center;">{{$edtotal_hrs}}</td>
            <td style="text-align:center;vailgn:middle;">{{$etill_hrs}}</td>
            <td rowspan="2" style="text-align:center;">{{$mtotal_hrs}}</td>
            <td rowspan="2" style="text-align:center;">{{$mdown}}</td>
            <td rowspan="2" style="text-align:center;">{{$mdtotal_hrs}}</td>
            <td style="text-align:center;vailgn:middle;">{{$mtill_hrs}}</td>
             <td rowspan="2" style="text-align:center;">{{$raw_hrs}}</td>
             <td rowspan="2" style="text-align:center;">{{$bdraw_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$rawtill_hrs}}</td>
               <td rowspan="2" style="text-align:center;">{{$bobbin_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdbobbin_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$bobtill_hrs}}</td>
             
               <td rowspan="2" style="text-align:center;">{{$feed_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdfeed_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$fedtill_hrs}}</td>
             
               <td rowspan="2" style="text-align:center;">{{$manp_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdmanp_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$manptill_hrs}}</td>
             
               <td rowspan="2" style="text-align:center;">{{$ent_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdent_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$entill_hrs}}</td>
             
                <td rowspan="2" style="text-align:center;">{{$wb_hrs}}</td>
              <td rowspan="2" style="text-align:center;">{{$bdwb_hrs}}</td>
             <td style="text-align:center;vailgn:middle;">{{$wbtill_hrs}}</td>
         </tr>
         <tr>
             <td style="text-align:center;">{{$bdtill_hrs}}</td>
             <td style="text-align:center;">{{$edtill_hrs}}</td>
             <td style="text-align:center;">{{$mdtill_hrs}}</td>
             <td style="text-align:center;">{{$rawdtill_hrs}}</td>
             <td style="text-align:center;">{{$bobdtill_hrs}}</td>
             <td style="text-align:center;">{{$fed1till_hrs}}</td>
             <td style="text-align:center;">{{$manpdtill_hrs}}</td>
             <td style="text-align:center;">{{$ent1ill_hrs}}</td>
             <td style="text-align:center;">{{$wbdtill_hrs}}</td>
         </tr>
                    <?php } } ?>
<!--                     <tr style="background-color: #5bb75b;">
                    <td style="text-align:center;">Stranding Total</td>
                    <td colspan="3"></td>
                    <td style="text-align:center;mso-number-format:'#,##0.000'">{{number_format($wt_km,3)}}</td>
                    <td style="text-align:center;mso-number-format:'#,##0.000'">{{number_format($wt_mt,3)}}</td>
                    <td style="text-align:center;">{{$btotal_hrs1}}</td>
                    <td></td>
                    <td style="text-align:center;">{{$etotal_hrs1}}</td>
                    <td></td>
                    <td style="text-align:center;">{{$mtotal_hrs1}}</td>
                    <td  style="text-align:center;"><?php echo "Total Hrs Lost: ".$final_hrs;?></td>
                    <td></td>
                </tr>-->
                 <tr style="background-color: #5bb75b;">
                    <td style="text-align:center;">Stranding Total</td>
                    <td colspan="3"></td>
                    <td style="text-align:center;mso-number-format:'#,##0.000'">{{number_format($wt_km,3)}}</td>
                    <td style="text-align:center;mso-number-format:'#,##0.000'">{{number_format($wt_mt,3)}}</td>
                     <td style="text-align:center;">{{$btotal_hrs1}}</td>
                    <td></td>
                    <td  style="text-align:center;">{{$bdtotal_hrs1}}</td>
                    <td></td>
                    <td style="text-align:center;">{{$etotal_hrs1}}</td>
                    <td></td>
                    <td  style="text-align:center;">{{$edtotal_hrs1}}</td>
                    <td></td>
                    <td style="text-align:center;">{{$mtotal_hrs1}}</td>
                    <td></td>
                     <td  style="text-align:center;">{{$mdtotal_hrs1}}</td>
                    <td  style="text-align:center;"><?php echo "Total Hrs Lost: ".$final_hrs;?></td>
                    <td style="text-align:center">{{$raw_hrs1}}</td>
                    <td>{{$bdraw_hrs1}}</td>
                    <td></td>
                    <td style="text-align:center">{{$bobbin_hrs1}}</td>
                    <td style="text-align:center">{{$bobbin_hrs1}}</td>
                    <td></td>
                    
                    <td style="text-align:center">{{$feed_hrs1}}</td>
                    <td style="text-align:center">{{$bdfeed_hrs1}}</td>
                    <td></td>
                    
                    <td style="text-align:center">{{$manp_hrs1}}</td>
                    <td style="text-align:center">{{$bdmanp_hrs1}}</td>
                    <td></td>
                    
                    <td style="text-align:center">{{$ent_hrs1}}</td>
                    <td style="text-align:center">{{$bdent_hrs1}}</td>
                    <td></td>
                    
                    <td style="text-align:center">{{$wb_hrs1}}</td>
                    <td style="text-align:center">{{$bdwb_hrs1}}</td>
                    <td></td>
                    
                </tr>
               
             
                

</table>
<?php // exit;?>
<table>
     <tr></tr>
                <tr></tr>
                <?php 
			 $coil_wt_data = DB::table('tbl_coil')
            ->select('tbl_coil.code')
            //->selectRaw('sum(weight) as weight')
            ->selectRaw('sum(production_wt) as weight')
            ->selectRaw('max(boobin_wt) as max')
            ->groupBy('tbl_coil.code')
            ->whereBetween('log_date', array($from_date, $to_date))
            ->get();
                $drum_wt_data = DB::table('tbl_drum')
            ->select('tbl_drum.conductor_code')
//            ->selectRaw('sum(net_wt) as net_weight')
            ->selectRaw('sum(length) as net_weight')
            ->groupBy('tbl_drum.conductor_code')
            ->whereBetween('log_date', array($from_date, $to_date))
             ->where('tbl_drum.stage','=','Finish Goods')
            ->get();
//                echo "<pre>";print_r($drum_wt_data);exit;
                ?>
                 <tr>
                    <td style="border: none;"></td>
                    <td style="border: none;"></td>
                    <td colspan="2" style="text-align:center;border: 1px solid;">Summary Report</td>
                    
                </tr>
                <tr>
                    <td style=" border: none;"></td>
                    <td style=" border: none;"></td>
                    <td>
                     <table align="center" border="1">
                    <tr>
                    <th colspan="4">Drawing in MT</th>
                    </tr>
                   
                        <?php foreach($coil_wt_data as $data)
                        {
							$wt = $data->weight;
							$mt = $data->max;
							$kg=$wt*$mt;
							$mt=($wt)/1000;
                            $conductor_name= App\MasterProduct::where(['id' => $data->code])->first();
                            
                            ?>
                     <tr>
                         <td colspan="2" style="text-align:center;">{{$conductor_name->drum_code}}</td>
                         <td colspan="2" style="text-align:center;">{{$mt}}</td>
                        </tr>
                        <?php
                        }
                        ?>
                    
                </table>   
                    </td>
                    <td>
                     <table align="center" border="1">
                    <tr>
                    <th colspan="4">Stranding in KM</th>
                    </tr>
                   
                        <?php foreach($drum_wt_data as $ddata)
                        {
                             $conductor_name= App\MasterProduct::where(['id' => $ddata->conductor_code])->first();
                            
                            ?>
                     <tr>
                         <td colspan="2" style="text-align:center;">{{$conductor_name->drum_code}}</td>
                         <td colspan="2" style="text-align:center;mso-number-format:'#,##0.000'">{{number_format($ddata->net_weight,3)}}</td>
                        </tr>
                        <?php
                        }
                        ?>
                    
                </table>   
                    </td>
                   
                </tr>
                 <tr></tr>
                <tr></tr>
</table>

<table align="center" border="1" style="text-align:center;border: 1px solid;">
                <?php 
                        $down_data = DB::table('tbl_down')
                                     ->select('*')
                                     ->whereBetween('date', array($from_date, $to_date))
                                     ->orderBy('date')
                                     ->orderBy('machine_name')
                                     ->get();
//                echo "<pre>";print_r($down_data);exit;
                  $date_arr = array();
                  $total_stopage = "0:0";
                ?>
                 <tr>
                     <td colspan="10" style="text-align:center;border: 1px solid;"><h2>Down Time Report</h2></td>
                 </tr>
                 <tr>
                    <td style="text-align:center;">Date</td>
                    <td style="text-align:center;">Machine Name</td>
                    <td style="text-align:center;">Total Hrs</td>
                    <td style="text-align:center;">Nature of E</td>
                    <td style="text-align:center;">Nature of M</td>
                    <td style="text-align:center;">Process Breakdown</td>
                    <td style="text-align:center;">Other</td>
                    <td style="text-align:center;">Reason</td>
                    <td style="text-align:center;">Action Taken</td>
                    <td style="text-align:center;">Remarks</td>
                 </tr>
                 @foreach($down_data as $down)
                 <?php 
//                 echo "<pre>";print_r($date_arr);
                 if(!in_array($down->date,$date_arr)){
                 if($down->total_hrs == "23:59" && $down->machine_name == "All")
                 {
                     $date_arr[] = $down->date;
                 }
                ?>
                 <tr>
                     <td style="text-align:center;">{{$down->date}}</td>
                     <td style="text-align:center;">{{$down->machine_name}}</td>
                     <?php if($down->total_hrs == "23:59") $down->total_hrs = "24:00"; ?>
                     <td style="text-align:center;">{{$down->total_hrs}}</td>
                     <td style="text-align:center;">{{$down->nature_of_e}}</td>
                     <td style="text-align:center;">{{$down->nature_of_m}}</td>
                     <td style="text-align:center;">{{$down->process_breakdown}}</td>
                     <td style="text-align:center;">{{$down->nature_of_breakdown}}</td>
                     <td style="text-align:center;">{{$down->reason}}</td>
                     <td style="text-align:center;">{{$down->action_taken}}</td>
                     <td style="text-align:center;">{{$down->remarks}}</td>
                 </tr>
                 <?php  
                 if($down->total_hrs != ""){
                    $times = array($down->total_hrs, $total_stopage);
                    $seconds = 0;
                    foreach ($times as $time)
                    {
                      list($hour,$minute) = explode(':', $time);
                      $seconds += $hour*3600;
                      $seconds += $minute*60;
                    }
                    $hours = floor($seconds/3600);
                    $seconds -= $hours*3600;
                    $minutes  = floor($seconds/60);
                    $total_stopage = sprintf('%02d:%02d', $hours, $minutes);
                }
                 
                 }
                 ?>
                 @endforeach
                 <tr style="background-color: #5bb75b;">
                     <td style="text-align:center;"></td>
                     <td style="text-align:center;">Total</td>
                     <td style="text-align:center;">{{$total_stopage}}</td>
                     <td style="text-align:center;"></td>
                     <td style="text-align:center;"></td>
                     <td style="text-align:center;"></td>
                     <td style="text-align:center;"></td>
                     <td style="text-align:center;"></td>
                     <td style="text-align:center;"></td>
                     <td style="text-align:center;"></td>
                 </tr>
</table>

<?php
 
//exit;;
$the_data = 'this is test text for downloading the contents.';
$report_name = "Machine Summary Report";
header("Content-Type: application/xls");
header("Content-type: image/Upload");
header("Content-Type: text/csv; charset=utf-8");
header("Content-Disposition: attachment; filename=.$report_name.xls");
header('Expires: 0');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Content-Transfer-Encoding: binary');
}else {
    $flg = 1;
echo '<a href="javascript:void(0)" onclick="goToURL(); return false;"></a>';
?>
<link href="css/sweetalert.css" rel="stylesheet" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script>
//    alert();
$(document).ready(function () {
    swal({title: "Error", text: "No Report Available For This Date", type: "error",confirmButtonText: "Back"},
        function(){ 
            location.href = 'log_summary_report';
        }
     );
//    swal({ type: "success", title: "Good Job!", confirmButtonColor: "#292929", text: "Form Sumbmitted Successfully for line A", confirmButtonText: "Ok" });
    
});
</script>
                    
                    
                    
<?php }
                    function sum_the_time($time1, $time2) {
      $times = array($time1, $time2);
      $seconds = 0;
      foreach ($times as $time)
      {
        list($hour,$minute,$second) = explode(':', $time);
        $seconds += $hour*3600;
        $seconds += $minute*60;
        $seconds += $second;
      }
      $hours = floor($seconds/3600);
      $seconds -= $hours*3600;
      $minutes  = floor($seconds/60);
      $seconds -= $minutes*60;
      if($seconds < 9)
      {
      $seconds = "0".$seconds;
      }
      if($minutes < 9)
      {
      $minutes = "0".$minutes;
      }
        if($hours < 9)
      {
      $hours = "0".$hours;
      }
      return "{$hours}:{$minutes}";
    }
                    ?>